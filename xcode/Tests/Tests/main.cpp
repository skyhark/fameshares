//
//  main.cpp
//  Tests
//
//  Created by Sacha Vandamme on 20/06/16.
//  Copyright © 2016 Sacha Vandamme. All rights reserved.
//

#include "bitcoin_main.h"

int main(int argc, char* argv[])
{
    return main_daemon(argc, argv);
}
