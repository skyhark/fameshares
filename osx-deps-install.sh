#!/bin/bash


#install brew
/usr/bin/ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)"

#install dependencies
brew install autoconf automake berkeley-db libtool boost miniupnpc openssl pkg-config protobuf qt5 libevent
brew link --force openssl


#install original bitcoin
git clone https://github.com/bitcoin/bitcoin.git
cd bitcoin;
./autogen.sh
./configure --with-incompatible-bdb
make
make install


#compile and install univalue
cd src/univalue/
/autogen.sh && ./configure
make
make install


#compile and install secp
cd ../secp256k1
/autogen.sh && ./configure
make
make install


#compile and install leveldb
cd ../leveldb/
make
cp out-static/libleveldb.a /usr/local/lib/
cp out-static/libmemenv.a /usr/local/lib/
cp out-shared/libleveldb.dylib /usr/local/lib/
cp out-shared/libleveldb.dylib.1 /usr/local/lib/
cp out-shared/libleveldb.dylib.1.19 /usr/local/lib/