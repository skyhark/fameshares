FROM debian:jessie
MAINTAINER Famebroker
LABEL version="1.0"
LABEL description="Fameshares Container"

#Install dependencies
RUN apt-get update && \
    apt-get dist-upgrade -y && \
    DEBIAN_FRONTEND="noninteractive" apt-get -f -q upgrade -y -o Dpkg::Options::="--force-confnew" --no-install-recommends

RUN DEBIAN_FRONTEND="noninteractive" apt-get -f -q install -y -o Dpkg::Options::="--force-confnew" \
    git build-essential autoconf libevent-pthreads-2.0-5 libpgm-5.1 libsodium-dev libzmq-dev libdb++-dev libevent-dev  \
    && DEBIAN_FRONTEND="noninteractive" apt-get -f -q install -y -o Dpkg::Options::="--force-confnew" \
    libboost-all-dev libssl-dev libprotobuf-dev protobuf-compiler libqt4-dev libqrencode-dev libtool pkg-config \
    && DEBIAN_FRONTEND="noninteractive" apt-get -f -q install -y -o Dpkg::Options::="--force-confnew" bsdmainutils libcurl3-dev

ENV DATA_DIR /root/.FameShares
ENV RPCUSER fameshares
ENV RPCPASSWORD devops

RUN mkdir $DATA_DIR && chmod 777 $DATA_DIR;

RUN cd /var && git clone -b 0.12 https://github.com/bitcoin/bitcoin.git
RUN cd /var/bitcoin && ./autogen.sh && ./configure --with-incompatible-bdb -without-gui
RUN cd /var/bitcoin && make && make install

RUN cd /var && mkdir /var/fameshares
COPY . /var/fameshares
RUN chmod +x /var/fameshares/docker-entrypoint.sh
RUN cd /var/fameshares && ./autogen.sh && ./configure --with-incompatible-bdb
RUN cd /var/fameshares && make; exit 0

# Copy and link secp256k1 from bitcoin
RUN rm -r /var/fameshares/src/secp256k1 && cp -avr /var/bitcoin/src/secp256k1 /var/fameshares/src/
RUN cd /var/fameshares/src/secp256k1 \
    && cp --preserve=links include/* /usr/local/lib \
    && cp -avr include /usr/local/include/secp256k1

# Copy and link leveldb from bitcoin
RUN rm -r /var/fameshares/src/leveldb && cp -avr /var/bitcoin/src/leveldb /var/fameshares/src/
RUN cd /var/bitcoin/src/leveldb \
    && cp --preserve=links libleveldb.* /usr/local/lib \
    && cp -r include/leveldb /usr/local/include/ \
    && ln -s /usr/include/leveldb /var/fameshares/src/leveldb

# Copy and link univalue
# RUN rm -r /var/fameshares/src/univalue && cp -avr /var/bitcoin/src/univalue /var/fameshares/src/
RUN cd /var/fameshares/src/univalue \
    && cp -avr include /usr/local/include/univalue \
    && cp -avr /var/fameshares/src/univalue/include/* /usr/local/include/univalue

# Comilation and copy configuration
RUN cd /var/fameshares && make \
    && ln -s /var/fameshares/src/bitcoind /usr/bin/famesharesd \
    && cp -av /var/fameshares/shares_config/actions.json $DATA_DIR/actions.json \
    && cp -av /var/fameshares/shares_config/fameshares.conf $DATA_DIR/fameshares.conf

# Delete bitcoin repository
# RUN rm -r /var/bitcoin/

EXPOSE 1451
EXPOSE 1450

VOLUME ["/root/.FameShares"]
ENTRYPOINT ["/var/fameshares/docker-entrypoint.sh"]