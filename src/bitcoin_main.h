//
//  bitcoin_main.h
//  Lib-FameCoin
//
//  Created by Sacha Vandamme on 23/03/16.
//
//

#ifndef bitcoin_main_h
#define bitcoin_main_h

#define EXPORT __attribute__((visibility("default")))

EXPORT int main_daemon(int argc, char* argv[]);
EXPORT int main_tx(int argc, char* argv[]);
EXPORT int main_cli(int argc, char* argv[]);

/*#define main_allias(n) int main(int argc, char* argv[]) { return n(argc, argv); }

#ifdef COMPILE_TX
main_allias(main_tx);
#endif

#ifdef COMPILE_CLI
main_allias(main_cli);
#endif

#ifdef COMPILE_DAEMON
main_allias(main_daemon);
#endif*/


#endif /* bitcoin_main_h */
