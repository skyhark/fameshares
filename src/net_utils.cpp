#include "utils.h"
#include "alert.h"
#include "net.h"
#include "util.h"
#include "primitives/block.h"
#include "fameshares.h"
#include "timedata.h"
#include "merkleblock.h"
#include "consensus/validation.h"
#include "version.h"
#include "addrman.h"
#include "protocol.h"
#include "hash.h"

using namespace std;

map<NodeId, CNodeState> mapNodeState;
map<uint256, pair<NodeId, list<QueuedBlock>::iterator> > mapBlocksInFlight;
map<std::string, unsigned int> actionSyncStarted;
CMainCleanup instance_of_cmaincleanup;

/** Number of blocks in flight with validated headers. */
int nQueuedValidatedHeaders = 0;

bool fLargeWorkForkFound = false;
bool fLargeWorkInvalidChainFound = false;

CNodeState *State(NodeId pnode) {
    map<NodeId, CNodeState>::iterator it = mapNodeState.find(pnode);
    if (it == mapNodeState.end())
        return NULL;
    
    return &it->second;
}
/*
bool GetNodeStateStats(NodeId nodeid, CNodeStateStats &stats) {
    LOCK(cs_main);
    CNodeState *state = State(nodeid);
    if (state == NULL)
        return false;
    stats.nMisbehavior = state->nMisbehavior;
    stats.nSyncHeight = state->pindexBestKnownBlock ? state->pindexBestKnownBlock->nHeight : -1;
    stats.nCommonHeight = state->pindexLastCommonBlock ? state->pindexLastCommonBlock->nHeight : -1;
    BOOST_FOREACH(const QueuedBlock& queue, state->vBlocksInFlight) {
        if (queue.pindex)
            stats.vHeightInFlight.push_back(queue.pindex->nHeight);
    }
    return true;
}*/


CNodeState::CNodeState() {
    fCurrentlyConnected = false;
    nMisbehavior = 0;
    fShouldBan = false;
    nStallingSince = 0;
    nBlocksInFlight = 0;
    nBlocksInFlightValidHeaders = 0;
    fPreferredDownload = false;
    fPreferHeaders = false;
    
    BOOST_FOREACH(const ActionsMap::value_type& v, shares()->get_actions() )
    {
        action_states[v.first] = new CNodeActionState(v.first);
    }
}

CNodeState::~CNodeState()
{
    /*BOOST_FOREACH(const NodeActionStateMap::value_type& v, action_states )
    {
        delete v.second;
    }*/
    
    action_states.clear();
}

CNodeActionState *CNodeState::state(std::string action_identifier)
{
    if (!action_states.count(action_identifier))
    {
        action_states[action_identifier] = new CNodeActionState(action_identifier);
    }

    return action_states[action_identifier];
}


//////////////////////////////////////////////////////////////////////////////
//
// Registration of network node signals.
//

// Requires cs_main.
// Returns a bool indicating whether we requested this block.
bool MarkBlockAsReceived(const uint256& hash) {
    map<uint256, pair<NodeId, list<QueuedBlock>::iterator> >::iterator itInFlight = mapBlocksInFlight.find(hash);
    if (itInFlight != mapBlocksInFlight.end()) {
        CNodeState *state = State(itInFlight->second.first);
        nQueuedValidatedHeaders -= itInFlight->second.second->fValidatedHeaders;
        state->nBlocksInFlightValidHeaders -= itInFlight->second.second->fValidatedHeaders;
        state->vBlocksInFlight.erase(itInFlight->second.second);
        state->nBlocksInFlight--;
        state->nStallingSince = 0;
        mapBlocksInFlight.erase(itInFlight);
        return true;
    }
    return false;
}

namespace {
    
    /** Map maintaining per-node state. Requires cs_main. */

    int GetHeight(std::string action_identifier)
    {
        LOCK(cs_main);
        return chain(action_identifier)->Height();
    }
    
    void UpdatePreferredDownload(CNode* node, CNodeState* state)
    {
        nPreferredDownload -= state->fPreferredDownload;
        
        // Whether this node should be marked as a preferred download node.
        state->fPreferredDownload = (!node->fInbound || node->fWhitelisted) && !node->fOneShot && !node->fClient;
        
        nPreferredDownload += state->fPreferredDownload;
    }
    
    // Returns time at which to timeout block request (nTime in microseconds)
    int64_t GetBlockTimeout(int64_t nTime, int nValidatedQueuedBefore, const Consensus::Params &consensusParams)
    {
        return nTime + 500000 * consensusParams.nPowTargetSpacing * (4 + nValidatedQueuedBefore);
    }
    
    void InitializeNode(NodeId nodeid, const CNode *pnode) {
        LOCK(cs_main);
        CNodeState &state = mapNodeState.insert(std::make_pair(nodeid, CNodeState())).first->second;
        state.name = pnode->addrName;
        state.address = pnode->addr;
    }
    
    void FinalizeNode(NodeId nodeid) {
        LOCK(cs_main);
        CNodeState *state = State(nodeid);
        
        BOOST_FOREACH(const NodeActionStateMap::value_type& v, state->action_states)
        {
            if (v.second->fSyncStarted)
                actionSyncStarted[v.first]--;
        }
        
        if (state->nMisbehavior == 0 && state->fCurrentlyConnected) {
            AddressCurrentlyConnected(state->address);
        }
        
        BOOST_FOREACH(const QueuedBlock& entry, state->vBlocksInFlight) {
            nQueuedValidatedHeaders -= entry.fValidatedHeaders;
            mapBlocksInFlight.erase(entry.hash);
        }
        EraseOrphansFor(nodeid);
        nPreferredDownload -= state->fPreferredDownload;
        
        mapNodeState.erase(nodeid);
    }
    
    
    // Requires cs_main.
    void MarkBlockAsInFlight(std::string action_identifier, NodeId nodeid, const uint256& hash, const Consensus::Params& consensusParams, CBlockIndex *pindex = NULL) {
        CNodeState *state = State(nodeid);
        assert(state != NULL);
        
        // Make sure it's not listed somewhere already.
        MarkBlockAsReceived(hash);
        
        int64_t nNow = GetTimeMicros();
        QueuedBlock newentry = {hash, pindex, nNow, pindex != NULL, GetBlockTimeout(nNow, nQueuedValidatedHeaders, consensusParams), action_identifier};
        nQueuedValidatedHeaders += newentry.fValidatedHeaders;
        list<QueuedBlock>::iterator it = state->vBlocksInFlight.insert(state->vBlocksInFlight.end(), newentry);
        state->nBlocksInFlight++;
        state->nBlocksInFlightValidHeaders += newentry.fValidatedHeaders;
        mapBlocksInFlight[hash] = std::make_pair(nodeid, it);
    }
    
    /** Check whether the last unknown block a peer advertised is not yet known. */
    void ProcessBlockAvailability(std::string action_identifier, NodeId nodeid) {
        CNodeState *statex = State(nodeid);
        assert(statex != NULL);
        
        CNodeActionState *state = statex->state(action_identifier);
        assert(state != NULL);
        
        FAction *action;
        try
        {
            action = getAction(action_identifier);
        }
        catch(...)
        {
            statex->action_states.erase(action_identifier);
            LogPrintf("%s: seems to be removed, deleting state of node \n", action_identifier);
            return;
        }
        
        if (!state->hashLastUnknownBlock.IsNull()) {
            BlockMap::iterator itOld = action->blocks.find(state->hashLastUnknownBlock);
            if (itOld != action->blocks.end() && itOld->second->nChainWork > 0) {
                if (state->pindexBestKnownBlock == NULL || itOld->second->nChainWork >= state->pindexBestKnownBlock->nChainWork)
                    state->pindexBestKnownBlock = itOld->second;
                state->hashLastUnknownBlock.SetNull();
            }
        }
    }
    
    void ProcessBlockAvailability(NodeId nodeid)
    {
        CNodeState *statex = State(nodeid);
        assert(statex != NULL);
        
        BOOST_FOREACH(const NodeActionStateMap::value_type& v, statex->action_states )
        {
            ProcessBlockAvailability(v.first, nodeid);
        }
    }
    
    /** Update tracking information about which blocks a peer is assumed to have. */
    void UpdateBlockAvailability(std::string action_identifier, NodeId nodeid, const uint256 &hash) {
        CNodeState *statex = State(nodeid);
        assert(statex != NULL);
        
        CNodeActionState *state = statex->state(action_identifier);
        assert(state != NULL);
        
        ProcessBlockAvailability(action_identifier, nodeid);
        FAction *action = getAction(action_identifier);
        
        BlockMap::iterator it = action->blocks.find(hash);
        if (it != action->blocks.end() && it->second->nChainWork > 0) {
            // An actually better block was announced.
            if (state->pindexBestKnownBlock == NULL || it->second->nChainWork >= state->pindexBestKnownBlock->nChainWork)
                state->pindexBestKnownBlock = it->second;
        } else {
            // An unknown block was announced; just assume that the latest one is the best one.
            state->hashLastUnknownBlock = hash;
        }
    }
    
    // Requires cs_main
    bool CanDirectFetch(std::string action_identifier, const Consensus::Params &consensusParams)
    {
        return chain(action_identifier)->Tip()->GetBlockTime() > GetAdjustedTime() - consensusParams.nPowTargetSpacing * 20;
    }
    
    // Requires cs_main
    bool PeerHasHeader(CNodeState *state, CBlockIndex *pindex)
    {
        CNodeActionState *state2 = state->state(pindex->action_identifier);
        assert(state2 != NULL);
        
        if (state2->pindexBestKnownBlock && pindex == state2->pindexBestKnownBlock->GetAncestor(pindex->nHeight))
            return true;
        if (state2->pindexBestHeaderSent && pindex == state2->pindexBestHeaderSent->GetAncestor(pindex->nHeight))
            return true;
        return false;
    }
    
    /** Find the last common ancestor two blocks have.
     *  Both pa and pb must be non-NULL. */
    CBlockIndex* LastCommonAncestor(CBlockIndex* pa, CBlockIndex* pb) {
        if (pa->nHeight > pb->nHeight) {
            pa = pa->GetAncestor(pb->nHeight);
        } else if (pb->nHeight > pa->nHeight) {
            pb = pb->GetAncestor(pa->nHeight);
        }
        
        while (pa != pb && pa && pb) {
            pa = pa->pprev;
            pb = pb->pprev;
        }
        
        // Eventually all chain branches meet at the genesis block.
        assert(pa == pb);
        return pa;
    }
    
    /** Update pindexLastCommonBlock and add not-in-flight missing successors to vBlocks, until it has
     *  at most count entries. */
    void FindNextBlocksToDownload(std::string action_identifier, NodeId nodeid, unsigned int count, std::vector<CBlockIndex*>& vBlocks, NodeId& nodeStaller) {
        if (count == 0)
            return;
        
        vBlocks.reserve(vBlocks.size() + count);
        CNodeState *state = State(nodeid);
        assert(state != NULL);
        
        // Make sure pindexBestKnownBlock is up to date, we'll need it.
        ProcessBlockAvailability(nodeid);
        FAction *action = getAction(action_identifier);
        
        CNodeActionState *state2 = state->state(action_identifier);
        assert(state2 != NULL);
        
        if (state2->pindexBestKnownBlock == NULL || state2->pindexBestKnownBlock->nChainWork < action->chain->Tip()->nChainWork) {
            // This peer has nothing interesting.
            return;
        }
        
        CChain chainActive = *(action->chain);
        
        if (state2->pindexLastCommonBlock == NULL) {
            // Bootstrap quickly by guessing a parent of our best tip is the forking point.
            // Guessing wrong in either direction is not a problem.
            state2->pindexLastCommonBlock = chainActive[std::min(state2->pindexBestKnownBlock->nHeight, chainActive.Height())];
        }
        
        // If the peer reorganized, our previous pindexLastCommonBlock may not be an ancestor
        // of its current tip anymore. Go back enough to fix that.
        state2->pindexLastCommonBlock = LastCommonAncestor(state2->pindexLastCommonBlock, state2->pindexBestKnownBlock);
        if (state2->pindexLastCommonBlock == state2->pindexBestKnownBlock)
            return;
        
        std::vector<CBlockIndex*> vToFetch;
        CBlockIndex *pindexWalk = state2->pindexLastCommonBlock;
        // Never fetch further than the best block we know the peer has, or more than BLOCK_DOWNLOAD_WINDOW + 1 beyond the last
        // linked block we have in common with this peer. The +1 is so we can detect stalling, namely if we would be able to
        // download that next block if the window were 1 larger.
        int nWindowEnd = state2->pindexLastCommonBlock->nHeight + BLOCK_DOWNLOAD_WINDOW;
        int nMaxHeight = std::min<int>(state2->pindexBestKnownBlock->nHeight, nWindowEnd + 1);
        NodeId waitingfor = -1;
        while (pindexWalk->nHeight < nMaxHeight) {
            // Read up to 128 (or more, if more blocks than that are needed) successors of pindexWalk (towards
            // pindexBestKnownBlock) into vToFetch. We fetch 128, because CBlockIndex::GetAncestor may be as expensive
            // as iterating over ~100 CBlockIndex* entries anyway.
            int nToFetch = std::min(nMaxHeight - pindexWalk->nHeight, std::max<int>(count - vBlocks.size(), 128));
            vToFetch.resize(nToFetch);
            pindexWalk = state2->pindexBestKnownBlock->GetAncestor(pindexWalk->nHeight + nToFetch);
            vToFetch[nToFetch - 1] = pindexWalk;
            for (unsigned int i = nToFetch - 1; i > 0; i--) {
                vToFetch[i - 1] = vToFetch[i]->pprev;
            }
            
            // Iterate over those blocks in vToFetch (in forward direction), adding the ones that
            // are not yet downloaded and not in flight to vBlocks. In the mean time, update
            // pindexLastCommonBlock as long as all ancestors are already downloaded, or if it's
            // already part of our chain (and therefore don't need it even if pruned).
            BOOST_FOREACH(CBlockIndex* pindex, vToFetch) {
                if (!pindex->IsValid(BLOCK_VALID_TREE)) {
                    // We consider the chain that this peer is on invalid.
                    return;
                }
                if (pindex->nStatus & BLOCK_HAVE_DATA || chainActive.Contains(pindex)) {
                    if (pindex->nChainTx)
                        state2->pindexLastCommonBlock = pindex;
                } else if (mapBlocksInFlight.count(pindex->GetBlockHash()) == 0) {
                    // The block is not already downloaded, and not yet in flight.
                    if (pindex->nHeight > nWindowEnd) {
                        // We reached the end of the window.
                        if (vBlocks.size() == 0 && waitingfor != nodeid) {
                            // We aren't able to fetch anything, but we would be if the download window was one larger.
                            nodeStaller = waitingfor;
                        }
                        return;
                    }
                    vBlocks.push_back(pindex);
                    if (vBlocks.size() == count) {
                        return;
                    }
                } else if (waitingfor == -1) {
                    // This is the first already-in-flight block.
                    waitingfor = mapBlocksInFlight[pindex->GetBlockHash()].first;
                }
            }
        }
    }
    
} // anon namespace

bool GetNodeStateStats(std::string action_identifier, NodeId nodeid, CNodeStateStats &stats) {
    LOCK(cs_main);
    CNodeState *state = State(nodeid);
    if (state == NULL)
        return false;
    stats.nMisbehavior = state->nMisbehavior;
    stats.nSyncHeight = state->state(action_identifier)->pindexBestKnownBlock ? state->state(action_identifier)->pindexBestKnownBlock->nHeight : -1;
    stats.nCommonHeight = state->state(action_identifier)->pindexLastCommonBlock ? state->state(action_identifier)->pindexLastCommonBlock->nHeight : -1;
    BOOST_FOREACH(const QueuedBlock& queue, state->vBlocksInFlight) {
        if (queue.pindex)
            stats.vHeightInFlight.push_back(queue.pindex->nHeight);
    }
    return true;
}

void RegisterNodeSignals(CNodeSignals& nodeSignals)
{
    nodeSignals.GetHeight.connect(&GetHeight);
    nodeSignals.ProcessMessages.connect(&ProcessMessages);
    nodeSignals.SendMessages.connect(&SendMessages);
    nodeSignals.InitializeNode.connect(&InitializeNode);
    nodeSignals.FinalizeNode.connect(&FinalizeNode);
}

void UnregisterNodeSignals(CNodeSignals& nodeSignals)
{
    nodeSignals.GetHeight.disconnect(&GetHeight);
    nodeSignals.ProcessMessages.disconnect(&ProcessMessages);
    nodeSignals.SendMessages.disconnect(&SendMessages);
    nodeSignals.InitializeNode.disconnect(&InitializeNode);
    nodeSignals.FinalizeNode.disconnect(&FinalizeNode);
}

CBlockIndex* FindForkInGlobalIndex(const CChain& chain, const CBlockLocator& locator)
{
    // Find the first block the caller has in the main chain
    BlockMap mapBlockIndex = shares()->action(chain.action_identifier)->blocks;
    
    BOOST_FOREACH(const uint256& hash, locator.vHave) {
        BlockMap::iterator mi = mapBlockIndex.find(hash);
        if (mi != mapBlockIndex.end())
        {
            CBlockIndex* pindex = (*mi).second;
            if (chain.Contains(pindex))
                return pindex;
        }
    }
    return chain.Genesis();
}

std::string GetWarnings(const std::string& strFor)
{
    int nPriority = 0;
    string strStatusBar;
    string strRPC;
    string strGUI;

    if (!CLIENT_VERSION_IS_RELEASE) {
        strStatusBar = "This is a pre-release test build - use at your own risk - do not use for mining or merchant applications";
        strGUI = _("This is a pre-release test build - use at your own risk - do not use for mining or merchant applications");
    }

    if (GetBoolArg("-testsafemode", false))
        strStatusBar = strRPC = strGUI = "testsafemode enabled";

    // Misc warnings like out of disk space and clock is wrong
    if (strMiscWarning != "")
    {
        nPriority = 1000;
        strStatusBar = strGUI = strMiscWarning;
    }

    if (fLargeWorkForkFound)
    {
        nPriority = 2000;
        strStatusBar = strRPC = "Warning: The network does not appear to fully agree! Some miners appear to be experiencing issues.";
        strGUI = _("Warning: The network does not appear to fully agree! Some miners appear to be experiencing issues.");
    }
    else if (fLargeWorkInvalidChainFound)
    {
        nPriority = 2000;
        strStatusBar = strRPC = "Warning: We do not appear to fully agree with our peers! You may need to upgrade, or other nodes may need to upgrade.";
        strGUI = _("Warning: We do not appear to fully agree with our peers! You may need to upgrade, or other nodes may need to upgrade.");
    }

    // Alerts
    {
        LOCK(cs_mapAlerts);
        BOOST_FOREACH(PAIRTYPE(const uint256, CAlert)& item, mapAlerts)
        {
            const CAlert& alert = item.second;
            if (alert.AppliesToMe() && alert.nPriority > nPriority)
            {
                nPriority = alert.nPriority;
                strStatusBar = strGUI = alert.strStatusBar;
            }
        }
    }

    if (strFor == "gui")
        return strGUI;
    else if (strFor == "statusbar")
        return strStatusBar;
    else if (strFor == "rpc")
        return strRPC;
    assert(!"GetWarnings(): invalid parameter");
    return "error";
}

CMainCleanup::~CMainCleanup() {
    // block headers
    
    FameShares *_shares = shares();
    if(_shares != NULL)
    {
        ActionsMap actions = _shares->get_actions();
        
        BOOST_FOREACH(const ActionsMap::value_type& v, actions )
        {
            BlockMap::iterator it1 = v.second->blocks.begin();
            for (; it1 != v.second->blocks.end(); it1++)
                delete (*it1).second;
            v.second->blocks.clear();
            
            // orphan transactions
            v.second->mapOrphanTransactions.clear();
            v.second->mapOrphanTransactionsByPrev.clear();
        }
    }
}


//////////////////////////////////////////////////////////////////////////////
//
// Messages
//


bool AlreadyHave(const CInv& inv) EXCLUSIVE_LOCKS_REQUIRED(cs_main)
{
    FAction *action = getAction(inv.action_identifier);
    
    switch (inv.type)
    {
        case MSG_TX:
        {
            assert(action->recentRejects);
            if (action->chain->Tip()->GetBlockHash() != action->hashRecentRejectsChainTip)
            {
                // If the chain tip has changed previously rejected transactions
                // might be now valid, e.g. due to a nLockTime'd tx becoming valid,
                // or a double-spend. Reset the rejects filter and give those
                // txs a second chance.
                action->hashRecentRejectsChainTip = action->chain->Tip()->GetBlockHash();
                action->recentRejects->reset();
            }
            
            return action->recentRejects->contains(inv.hash) ||
                    action->mempool.exists(inv.hash) ||
                    action->mapOrphanTransactions.count(inv.hash) ||
                    action->coins->HaveCoins(inv.hash);
        }
        case MSG_BLOCK:
            return action->blocks.count(inv.hash);
    }
    // Don't know what it is, just say we already got one
    return true;
}

typedef boost::unordered_map<std::string, vector<CInv> > ActionsNotFoundMap;

void ProcessGetData(CNode* pfrom)
{
    std::deque<CInv>::iterator it = pfrom->vRecvGetData.begin();
    
    ActionsNotFoundMap vNotFound;
    
    LOCK(cs_main);
    
    while (it != pfrom->vRecvGetData.end()) {
        // Don't bother if send buffer is too full to respond anyway
        if (pfrom->nSendSize >= SendBufferSize())
            break;
        
        const CInv &inv = *it;
        {
            boost::this_thread::interruption_point();
            it++;
            
            if (inv.type == MSG_BLOCK || inv.type == MSG_FILTERED_BLOCK)
            {
                FAction *action = getAction(inv.action_identifier);
                CNodeActionInfo *nodeAction = pfrom->action(inv.action_identifier);
                const Consensus::Params& consensusParams = action->params->GetConsensus();
                
                bool send = false;
                BlockMap::iterator mi = action->blocks.find(inv.hash);
                if (mi != action->blocks.end())
                {
                    if (action->chain->Contains(mi->second)) {
                        send = true;
                    } else {
                        static const int nOneMonth = 30 * 24 * 60 * 60;
                        // To prevent fingerprinting attacks, only send blocks outside of the active
                        // chain if they are valid, and no more than a month older (both in time, and in
                        // best equivalent proof of work) than the best header chain we know about.
                        send = mi->second->IsValid(BLOCK_VALID_SCRIPTS) && (action->pindexBestHeader != NULL) &&
                        (action->pindexBestHeader->GetBlockTime() - mi->second->GetBlockTime() < nOneMonth) &&
                        (GetBlockProofEquivalentTime(*action->pindexBestHeader, *mi->second, *action->pindexBestHeader, consensusParams) < nOneMonth);
                        if (!send) {
                            LogPrintf("%s: ignoring request from peer=%i identifier=%s for old block that isn't in the main chain\n", __func__, pfrom->GetId(), inv.action_identifier);
                        }
                    }
                }
                // disconnect node in case we have reached the outbound limit for serving historical blocks
                // never disconnect whitelisted nodes
                static const int nOneWeek = 7 * 24 * 60 * 60; // assume > 1 week = historical
                if (send && CNode::OutboundTargetReached(true) && ( ((action->pindexBestHeader != NULL) && (action->pindexBestHeader->GetBlockTime() - mi->second->GetBlockTime() > nOneWeek)) || inv.type == MSG_FILTERED_BLOCK) && !pfrom->fWhitelisted)
                {
                    LogPrint("net", "historical block serving limit reached, disconnect peer=%d identifier=%s\n", pfrom->GetId(), inv.action_identifier);
                    
                    //disconnect node
                    pfrom->fDisconnect = true;
                    send = false;
                }
                // Pruned nodes may have deleted the block, so check whether
                // it's available before trying to send.
                if (send && (mi->second->nStatus & BLOCK_HAVE_DATA))
                {
                    // Send block from disk
                    CBlock block;
                    block.action_identifier = inv.action_identifier;
                    if (!ReadBlockFromDisk(block, (*mi).second, consensusParams))
                        assert(!"cannot load block from disk");
                    if (inv.type == MSG_BLOCK)
                        pfrom->PushMessage(inv.action_identifier ,NetMsgType::BLOCK, block);
                    else // MSG_FILTERED_BLOCK)
                    {
                        LOCK(pfrom->cs_filter);
                        if (pfrom->pfilter)
                        {
                            CMerkleBlock merkleBlock(block, *pfrom->pfilter);
                            pfrom->PushMessage(inv.action_identifier ,NetMsgType::MERKLEBLOCK, merkleBlock);
                            // CMerkleBlock just contains hashes, so also push any transactions in the block the client did not see
                            // This avoids hurting performance by pointlessly requiring a round-trip
                            // Note that there is currently no way for a node to request any single transactions we didn't send here -
                            // they must either disconnect and retry or request the full block.
                            // Thus, the protocol spec specified allows for us to provide duplicate txn here,
                            // however we MUST always provide at least what the remote peer needs
                            typedef std::pair<unsigned int, uint256> PairType;
                            BOOST_FOREACH(PairType& pair, merkleBlock.vMatchedTxn)
                            pfrom->PushMessage(inv.action_identifier , NetMsgType::TX, block.vtx[pair.first]);
                        }
                        // else
                        // no response
                    }
                    
                    // Trigger the peer node to send a getblocks request for the next batch of inventory
                    if (inv.hash == nodeAction->hashContinue)
                    {
                        FAction *action = getAction(inv.action_identifier);
                        // Bypass PushInventory, this must send even if redundant,
                        // and we want it right after the last block so they don't
                        // wait for other stuff first.
                        vector<CInv> vInv;
                        vInv.push_back(CInv(inv.action_identifier, MSG_BLOCK, action->chain->Tip()->GetBlockHash()));
                        pfrom->PushMessage(inv.action_identifier ,NetMsgType::INV, vInv);
                        nodeAction->hashContinue.SetNull();
                    }
                }
            }
            else if (inv.IsKnownType())
            {
                // Send stream from relay memory
                bool pushed = false;
                {
                    LOCK(cs_mapRelay);
                    map<CInv, CDataStream>::iterator mi = mapRelay.find(inv);
                    if (mi != mapRelay.end()) {
                        pfrom->PushMessage(inv.action_identifier,inv.GetCommand(), (*mi).second);
                        pushed = true;
                    }
                }
                if (!pushed && inv.type == MSG_TX) {
                    CTransaction tx;
                    FAction *action = getAction(inv.action_identifier);
                    if (action->mempool.lookup(inv.hash, tx)) {
                        CDataStream ss(SER_NETWORK, PROTOCOL_VERSION);
                        ss.reserve(1000);
                        ss << tx;
                        pfrom->PushMessage(inv.action_identifier,NetMsgType::TX, ss);
                        pushed = true;
                    }
                }
                if (!pushed) {
                    if(!vNotFound.count(inv.action_identifier))
                    {
                        vNotFound[inv.action_identifier] = vector<CInv>();
                    }

                    vNotFound[inv.action_identifier].push_back(inv);
                }
            }
            
            // Track requests for our stuff.
            GetMainSignals().Inventory(inv.hash);
            
            if (inv.type == MSG_BLOCK || inv.type == MSG_FILTERED_BLOCK)
                break;
        }
    }
    
    pfrom->vRecvGetData.erase(pfrom->vRecvGetData.begin(), it);
    
    if (!vNotFound.empty()) {
        // Let the peer know that we didn't find what it asked for, so it doesn't
        // have to wait around forever. Currently only SPV clients actually care
        // about this message: it's needed when they are recursively walking the
        // dependencies of relevant unconfirmed transactions. SPV clients want to
        // do that because they want to know about (and store and rebroadcast and
        // risk analyze) the dependencies of transactions relevant to them, without
        // having to download the entire memory pool.
        
        BOOST_FOREACH(const ActionsNotFoundMap::value_type& v, vNotFound)
        {
            pfrom->PushMessage(v.first, NetMsgType::NOTFOUND, v.second);
        }
    }
}

bool ProcessMessage(CNode* pfrom, std::string action_identifier, string strCommand, CDataStream& vRecv, int64_t nTimeReceived)
{
    FAction *action;

    try
    {
        action = getAction(action_identifier);
    }
    catch(...)
    {
        return false;
    }

    FChainParams *chainparams = action->params;
    CNodeActionInfo *nodeAction = pfrom->action(action_identifier);

    RandAddSeedPerfmon();
    LogPrint("net", "received: %s (%u bytes) peer=%d identifier=%s\n", SanitizeString(strCommand), vRecv.size(), pfrom->id, action_identifier);
    if (mapArgs.count("-dropmessagestest") && GetRand(atoi(mapArgs["-dropmessagestest"])) == 0)
    {
        LogPrintf("dropmessagestest DROPPING RECV MESSAGE\n");
        return true;
    }
    
    
    if (!(nLocalServices & NODE_BLOOM) &&
        (strCommand == NetMsgType::FILTERLOAD ||
         strCommand == NetMsgType::FILTERADD ||
         strCommand == NetMsgType::FILTERCLEAR))
    {
        if (pfrom->nVersion >= NO_BLOOM_VERSION) {
            Misbehaving(pfrom->GetId(), 100);
            return false;
        } else if (GetBoolArg("-enforcenodebloom", DEFAULT_ENFORCENODEBLOOM)) {
            pfrom->fDisconnect = true;
            return false;
        }
    }
    
    
    if (strCommand == NetMsgType::VERSION)
    {
        bool full_hanlde = (pfrom->nVersion == 0);

        // Each connection can only send one version message
        if (nodeAction->versionReceived)
        {
            pfrom->PushMessage(action_identifier, NetMsgType::REJECT, strCommand, REJECT_DUPLICATE, string("Duplicate version message for "+action_identifier));
            Misbehaving(pfrom->GetId(), 1);
            return false;
        }
        
        nodeAction->versionReceived = true;
        
        int64_t nTime;
        CAddress addrMe;
        CAddress addrFrom;
        uint64_t nNonce = 1;
        vRecv >> pfrom->nVersion >> pfrom->nServices >> nTime >> addrMe;
        if (pfrom->nVersion < MIN_PEER_PROTO_VERSION)
        {
            // disconnect from peers older than this proto version
            LogPrintf("peer=%d using obsolete version %i; disconnecting\n", pfrom->id, pfrom->nVersion);
            pfrom->PushMessage(action_identifier,NetMsgType::REJECT, strCommand, REJECT_OBSOLETE,
                               strprintf("Version must be %d or greater", MIN_PEER_PROTO_VERSION));
            pfrom->fDisconnect = true;
            return false;
        }
        
        if (pfrom->nVersion == 10300)
            pfrom->nVersion = 300;
        if (!vRecv.empty())
            vRecv >> addrFrom >> nNonce;

        if (!vRecv.empty()) {
            vRecv >> LIMITED_STRING(pfrom->strSubVer, MAX_SUBVERSION_LENGTH);
            pfrom->cleanSubVer = SanitizeString(pfrom->strSubVer);
        }
    
        if (!vRecv.empty())
            vRecv >> nodeAction->nStartingHeight;
        if (!vRecv.empty())
            vRecv >> pfrom->fRelayTxes; // set to true after we get the first filter* message
        else
            pfrom->fRelayTxes = true;
        
        // Disconnect if we connected to ourself
        if (nNonce == nLocalHostNonce && nNonce > 1)
        {
            LogPrintf("connected to self at %s, disconnecting\n", pfrom->addr.ToString());
            pfrom->fDisconnect = true;
            return true;
        }
        
        pfrom->addrLocal = addrMe;
        if (pfrom->fInbound && addrMe.IsRoutable() && full_hanlde)
        {
            SeenLocal(addrMe);
        }
        
        // Be shy and don't send version until we hear
        if (pfrom->fInbound)
            pfrom->PushVersion(action_identifier);
        
        if(full_hanlde)
        {
            pfrom->fClient = !(pfrom->nServices & NODE_NETWORK);
            
            // Potentially mark this peer as a preferred download peer.
            UpdatePreferredDownload(pfrom, State(pfrom->GetId()));
            
            // Change version
            pfrom->PushMessage(action_identifier ,NetMsgType::VERACK);
            pfrom->ssSend.SetVersion(min(pfrom->nVersion, PROTOCOL_VERSION));
        
            if (!pfrom->fInbound)
            {
                // Advertise our address
                if (fListen && !IsInitialBlockDownload(action))
                {
                    CAddress addr = GetLocalAddress(&pfrom->addr);
                    if (addr.IsRoutable())
                    {
                        LogPrintf("ProcessMessages: advertising address %s\n", addr.ToString());
                        pfrom->PushAddress(addr);
                    } else if (IsPeerAddrLocalGood(pfrom)) {
                        addr.SetIP(pfrom->addrLocal);
                        LogPrintf("ProcessMessages: advertising address %s\n", addr.ToString());
                        pfrom->PushAddress(addr);
                    }
                }
            
                // Get recent addresses
                if (pfrom->fOneShot || pfrom->nVersion >= CADDR_TIME_VERSION || addrman.size() < 1000)
                {
                    pfrom->PushMessage(action_identifier ,NetMsgType::GETADDR);
                    pfrom->fGetAddr = true;
                }
                addrman.Good(pfrom->addr);
            } else {
                if (((CNetAddr)pfrom->addr) == (CNetAddr)addrFrom)
                {
                    addrman.Add(addrFrom, addrFrom);
                    addrman.Good(addrFrom);
                }
            }
        }
        
        
        // Relay alerts
        {
            LOCK(cs_mapAlerts);
            BOOST_FOREACH(PAIRTYPE(const uint256, CAlert)& item, mapAlerts)
            item.second.RelayTo(action_identifier, pfrom);
        }
        
        pfrom->fSuccessfullyConnected = true;
        
        string remoteAddr;
        if (fLogIPs)
            remoteAddr = ", peeraddr=" + pfrom->addr.ToString();
        
        LogPrintf("receive version message: %s: version %d, blocks=%d, us=%s, peer=%d%s identifier=%s\n",
                  pfrom->cleanSubVer, pfrom->nVersion,
                  nodeAction->nStartingHeight, addrMe.ToString(), pfrom->id,
                  remoteAddr, action_identifier);
        
        if(full_hanlde)
        {
            int64_t nTimeOffset = nTime - GetTime();
            pfrom->nTimeOffset = nTimeOffset;
            AddTimeData(pfrom->addr, nTimeOffset);
        }
    }
    
    
    else if (pfrom->nVersion == 0)
    {
        // Must have a version message before anything else
        Misbehaving(pfrom->GetId(), 1);
        return false;
    }
    
    
    else if (strCommand == NetMsgType::VERACK)
    {
        pfrom->SetRecvVersion(min(pfrom->nVersion, PROTOCOL_VERSION));
        
        // Mark this node as currently connected, so we update its timestamp later.
        if (pfrom->fNetworkNode) {
            LOCK(cs_main);
            State(pfrom->GetId())->fCurrentlyConnected = true;
        }
        
        if (pfrom->nVersion >= SENDHEADERS_VERSION) {
            // Tell our peer we prefer to receive headers rather than inv's
            // We send this to non-NODE NETWORK peers as well, because even
            // non-NODE NETWORK peers can announce blocks (such as pruning
            // nodes)
            pfrom->PushMessage(action_identifier,NetMsgType::SENDHEADERS);
        }
        
        //pfrom->PushMessage(action_identifier,NetMsgType::ALLOWEDACTIONS);
    }
    
    
    else if (strCommand == NetMsgType::ADDR)
    {
        vector<CAddress> vAddr;
        vRecv >> vAddr;
        
        // Don't want addr from older versions unless seeding
        if (pfrom->nVersion < CADDR_TIME_VERSION && addrman.size() > 1000)
            return true;
        if (vAddr.size() > 1000)
        {
            Misbehaving(pfrom->GetId(), 20);
            return error("message addr size() = %u", vAddr.size());
        }
        
        // Store the new addresses
        vector<CAddress> vAddrOk;
        int64_t nNow = GetAdjustedTime();
        int64_t nSince = nNow - 10 * 60;
        BOOST_FOREACH(CAddress& addr, vAddr)
        {
            boost::this_thread::interruption_point();
            
            if (addr.nTime <= 100000000 || addr.nTime > nNow + 10 * 60)
                addr.nTime = nNow - 5 * 24 * 60 * 60;
            pfrom->AddAddressKnown(addr);
            bool fReachable = IsReachable(addr);
            if (addr.nTime > nSince && !pfrom->fGetAddr && vAddr.size() <= 10 && addr.IsRoutable())
            {
                // Relay to a limited number of other nodes
                {
                    LOCK(cs_vNodes);
                    // Use deterministic randomness to send to the same nodes for 24 hours
                    // at a time so the addrKnowns of the chosen nodes prevent repeats
                    static uint256 hashSalt;
                    if (hashSalt.IsNull())
                        hashSalt = GetRandHash();
                    uint64_t hashAddr = addr.GetHash();
                    uint256 hashRand = ArithToUint256(UintToArith256(hashSalt) ^ (hashAddr<<32) ^ ((GetTime()+hashAddr)/(24*60*60)));
                    hashRand = Hash(BEGIN(hashRand), END(hashRand));
                    multimap<uint256, CNode*> mapMix;
                    BOOST_FOREACH(CNode* pnode, vNodes)
                    {
                        if (pnode->nVersion < CADDR_TIME_VERSION)
                            continue;
                        unsigned int nPointer;
                        memcpy(&nPointer, &pnode, sizeof(nPointer));
                        uint256 hashKey = ArithToUint256(UintToArith256(hashRand) ^ nPointer);
                        hashKey = Hash(BEGIN(hashKey), END(hashKey));
                        mapMix.insert(make_pair(hashKey, pnode));
                    }
                    int nRelayNodes = fReachable ? 2 : 1; // limited relaying of addresses outside our network(s)
                    for (multimap<uint256, CNode*>::iterator mi = mapMix.begin(); mi != mapMix.end() && nRelayNodes-- > 0; ++mi)
                        ((*mi).second)->PushAddress(addr);
                }
            }
            // Do not store addresses outside our network
            if (fReachable)
                vAddrOk.push_back(addr);
        }
        addrman.Add(vAddrOk, pfrom->addr, 2 * 60 * 60);
        if (vAddr.size() < 1000)
            pfrom->fGetAddr = false;
        if (pfrom->fOneShot)
            pfrom->fDisconnect = true;
    }
    
    else if (strCommand == NetMsgType::SENDHEADERS)
    {
        LOCK(cs_main);
        State(pfrom->GetId())->fPreferHeaders = true;
    }
    
    
    else if (strCommand == NetMsgType::INV)
    {
        vector<CInv> vInv;
        vRecv >> vInv;
        if (vInv.size() > MAX_INV_SZ)
        {
            Misbehaving(pfrom->GetId(), 20);
            return error("message inv size() = %u", vInv.size());
        }
        
        bool fBlocksOnly = GetBoolArg("-blocksonly", DEFAULT_BLOCKSONLY);
        
        // Allow whitelisted peers to send data other than blocks in blocks only mode if whitelistrelay is true
        if (pfrom->fWhitelisted && GetBoolArg("-whitelistrelay", DEFAULT_WHITELISTRELAY))
            fBlocksOnly = false;
        
        LOCK(cs_main);
        
        std::vector<CInv> vToFetch;
        
        for (unsigned int nInv = 0; nInv < vInv.size(); nInv++)
        {
            const CInv &inv = vInv[nInv];
            
            boost::this_thread::interruption_point();
            pfrom->AddInventoryKnown(inv);
            
            bool fAlreadyHave = AlreadyHave(inv);
            LogPrint("net", "got inv: %s  %s peer=%d\n", inv.ToString(), fAlreadyHave ? "have" : "new", pfrom->id);
            
            if (inv.type == MSG_BLOCK) {
                UpdateBlockAvailability(action_identifier, pfrom->GetId(), inv.hash);
                if (!fAlreadyHave && !action->fImporting && !action->fReindex && !mapBlocksInFlight.count(inv.hash)) {
                    // First request the headers preceding the announced block. In the normal fully-synced
                    // case where a new block is announced that succeeds the current tip (no reorganization),
                    // there are no such headers.
                    // Secondly, and only when we are close to being synced, we request the announced block directly,
                    // to avoid an extra round-trip. Note that we must *first* ask for the headers, so by the
                    // time the block arrives, the header chain leading up to it is already validated. Not
                    // doing this will result in the received block being rejected as an orphan in case it is
                    // not a direct successor.
                    pfrom->PushMessage(action_identifier,NetMsgType::GETHEADERS, action->chain->GetLocator(action->pindexBestHeader), inv.hash);
                    CNodeState *nodestate = State(pfrom->GetId());
                    if (CanDirectFetch(action_identifier, action->params->GetConsensus()) &&
                        nodestate->nBlocksInFlight < MAX_BLOCKS_IN_TRANSIT_PER_PEER) {
                        vToFetch.push_back(inv);
                        // Mark block as in flight already, even though the actual "getdata" message only goes out
                        // later (within the same cs_main lock, though).
                        MarkBlockAsInFlight(action_identifier, pfrom->GetId(), inv.hash, action->params->GetConsensus());
                    }
                    LogPrint("net", "getheaders (%d) %s to peer=%d\n", action->pindexBestHeader->nHeight, inv.hash.ToString(), pfrom->id);
                }
            }
            else
            {
                if (fBlocksOnly)
                    LogPrint("net", "transaction (%s) inv sent in violation of protocol peer=%d\n", inv.hash.ToString(), pfrom->id);
                else if (!fAlreadyHave && !action->fImporting && !action->fReindex && !IsInitialBlockDownload(action))
                    pfrom->AskFor(inv);
            }
            
            // Track requests for our stuff
            GetMainSignals().Inventory(inv.hash);
            
            if (pfrom->nSendSize > (SendBufferSize() * 2)) {
                Misbehaving(pfrom->GetId(), 50);
                return error("send buffer size() = %u", pfrom->nSendSize);
            }
        }
        
        if (!vToFetch.empty())
            pfrom->PushMessage(action_identifier,NetMsgType::GETDATA, vToFetch);
    }
    
    
    else if (strCommand == NetMsgType::GETDATA)
    {
        vector<CInv> vInv;
        vRecv >> vInv;
        if (vInv.size() > MAX_INV_SZ)
        {
            Misbehaving(pfrom->GetId(), 20);
            return error("message getdata size() = %u", vInv.size());
        }
        
        if (fDebug || (vInv.size() != 1))
            LogPrint("net", "received getdata (%u invsz) peer=%d\n", vInv.size(), pfrom->id);
        
        if ((fDebug && vInv.size() > 0) || (vInv.size() == 1))
            LogPrint("net", "received getdata for: %s peer=%d\n", vInv[0].ToString(), pfrom->id);
        
        pfrom->vRecvGetData.insert(pfrom->vRecvGetData.end(), vInv.begin(), vInv.end());
        ProcessGetData(pfrom);
    }
    
    
    else if (strCommand == NetMsgType::GETBLOCKS)
    {
        CBlockLocator locator;
        uint256 hashStop;
        vRecv >> locator >> hashStop;
        
        LOCK(cs_main);
        
        // Find the last block the caller has in the main chain
        CBlockIndex* pindex = FindForkInGlobalIndex(*action->chain, locator);
        
        // Send the rest of the chain
        if (pindex)
            pindex = action->chain->Next(pindex);
        int nLimit = 500;
        LogPrint("net", "getblocks %d to %s limit %d from peer=%d\n", (pindex ? pindex->nHeight : -1), hashStop.IsNull() ? "end" : hashStop.ToString(), nLimit, pfrom->id);
        for (; pindex; pindex = action->chain->Next(pindex))
        {
            if (pindex->GetBlockHash() == hashStop)
            {
                LogPrint("net", "  getblocks stopping at %d %s\n", pindex->nHeight, pindex->GetBlockHash().ToString());
                break;
            }
            // If pruning, don't inv blocks unless we have on disk and are likely to still have
            // for some reasonable time window (1 hour) that block relay might require.
            const int nPrunedBlocksLikelyToHave = MIN_BLOCKS_TO_KEEP - 3600 / action->params->GetConsensus().nPowTargetSpacing;
            if (fPruneMode && (!(pindex->nStatus & BLOCK_HAVE_DATA) || pindex->nHeight <= action->chain->Tip()->nHeight - nPrunedBlocksLikelyToHave))
            {
                LogPrint("net", " getblocks stopping, pruned or too old block at %d %s\n", pindex->nHeight, pindex->GetBlockHash().ToString());
                break;
            }
            pfrom->PushInventory(CInv(action_identifier, MSG_BLOCK, pindex->GetBlockHash()));
            if (--nLimit <= 0)
            {
                // When this block is requested, we'll send an inv that'll
                // trigger the peer to getblocks the next batch of inventory.
                LogPrint("net", "  getblocks stopping at limit %d %s\n", pindex->nHeight, pindex->GetBlockHash().ToString());
                nodeAction->hashContinue = pindex->GetBlockHash();
                break;
            }
        }
    }
    
    
    else if (strCommand == NetMsgType::GETHEADERS)
    {
        CBlockLocator locator;
        uint256 hashStop;
        vRecv >> locator >> hashStop;
        
        LOCK(cs_main);
        if (IsInitialBlockDownload(action) && !pfrom->fWhitelisted) {
            LogPrint("net", "Ignoring getheaders from peer=%d because node is in initial block download\n", pfrom->id);
            return true;
        }
        
        CNodeState *nodestate = State(pfrom->GetId());
        CBlockIndex* pindex = NULL;
        if (locator.IsNull())
        {
            // If locator is null, return the hashStop block
            BlockMap::iterator mi = action->blocks.find(hashStop);
            if (mi == action->blocks.end())
                return true;
            pindex = (*mi).second;
        }
        else
        {
            // Find the last block the caller has in the main chain
            pindex = FindForkInGlobalIndex(*(action->chain), locator);
            if (pindex)
                pindex = action->chain->Next(pindex);
        }
        
        // we must use CBlocks, as CBlockHeaders won't include the 0x00 nTx count at the end
        vector<CBlock> vHeaders;
        int nLimit = MAX_HEADERS_RESULTS;
        LogPrint("net", "getheaders %d to %s from peer=%d\n", (pindex ? pindex->nHeight : -1), hashStop.ToString(), pfrom->id);
        for (; pindex; pindex = action->chain->Next(pindex))
        {
            vHeaders.push_back(pindex->GetBlockHeader());
            if (--nLimit <= 0 || pindex->GetBlockHash() == hashStop)
                break;
        }
        // pindex can be NULL either if we sent chainActive.Tip() OR
        // if our peer has chainActive.Tip() (and thus we are sending an empty
        // headers message). In both cases it's safe to update
        // pindexBestHeaderSent to be our tip.
        nodestate->state(action_identifier)->pindexBestHeaderSent = pindex ? pindex : action->chain->Tip();
        pfrom->PushMessage(action_identifier,NetMsgType::HEADERS, vHeaders);
    }
    
    
    else if (strCommand == NetMsgType::TX)
    {
        // Stop processing the transaction early if
        // We are in blocks only mode and peer is either not whitelisted or whitelistrelay is off
        if (GetBoolArg("-blocksonly", DEFAULT_BLOCKSONLY) && (!pfrom->fWhitelisted || !GetBoolArg("-whitelistrelay", DEFAULT_WHITELISTRELAY)))
        {
            LogPrint("net", "transaction sent in violation of protocol peer=%d\n", pfrom->id);
            return true;
        }
        
        vector<uint256> vWorkQueue;
        vector<uint256> vEraseQueue;
        CTransaction tx;
        vRecv >> tx;
        
        CInv inv(action_identifier, MSG_TX, tx.GetHash());
        pfrom->AddInventoryKnown(inv);
        
        LOCK(cs_main);
        
        bool fMissingInputs = false;
        CValidationState state;
        
        pfrom->setAskFor.erase(inv.hash);
        mapAlreadyAskedFor.erase(inv);
        
        if (!AlreadyHave(inv) && AcceptToMemoryPool(action->mempool, state, tx, true, &fMissingInputs))
        {
            action->mempool.check(action->coins);
            RelayTransaction(tx);
            vWorkQueue.push_back(inv.hash);
            
            LogPrint("mempool", "AcceptToMemoryPool: peer=%d: accepted %s (poolsz %u txn, %u kB)\n",
                     pfrom->id,
                     tx.GetHash().ToString(),
                     action->mempool.size(), action->mempool.DynamicMemoryUsage() / 1000);
            
            // Recursively process any orphan transactions that depended on this one
            set<NodeId> setMisbehaving;
            for (unsigned int i = 0; i < vWorkQueue.size(); i++)
            {
                map<uint256, set<uint256> >::iterator itByPrev = action->mapOrphanTransactionsByPrev.find(vWorkQueue[i]);
                if (itByPrev == action->mapOrphanTransactionsByPrev.end())
                    continue;
                for (set<uint256>::iterator mi = itByPrev->second.begin();
                     mi != itByPrev->second.end();
                     ++mi)
                {
                    const uint256& orphanHash = *mi;
                    const CTransaction& orphanTx = action->mapOrphanTransactions[orphanHash].tx;
                    NodeId fromPeer = action->mapOrphanTransactions[orphanHash].fromPeer;
                    bool fMissingInputs2 = false;
                    // Use a dummy CValidationState so someone can't setup nodes to counter-DoS based on orphan
                    // resolution (that is, feeding people an invalid transaction based on LegitTxX in order to get
                    // anyone relaying LegitTxX banned)
                    CValidationState stateDummy;
                    
                    
                    if (setMisbehaving.count(fromPeer))
                        continue;
                    if (AcceptToMemoryPool(action->mempool, stateDummy, orphanTx, true, &fMissingInputs2))
                    {
                        LogPrint("mempool", "   accepted orphan tx %s\n", orphanHash.ToString());
                        RelayTransaction(orphanTx);
                        vWorkQueue.push_back(orphanHash);
                        vEraseQueue.push_back(orphanHash);
                    }
                    else if (!fMissingInputs2)
                    {
                        int nDos = 0;
                        if (stateDummy.IsInvalid(nDos) && nDos > 0)
                        {
                            // Punish peer that gave us an invalid orphan tx
                            Misbehaving(fromPeer, nDos);
                            setMisbehaving.insert(fromPeer);
                            LogPrint("mempool", "   invalid orphan tx %s\n", orphanHash.ToString());
                        }
                        // Has inputs but not accepted to mempool
                        // Probably non-standard or insufficient fee/priority
                        LogPrint("mempool", "   removed orphan tx %s\n", orphanHash.ToString());
                        vEraseQueue.push_back(orphanHash);
                        assert(action->recentRejects);
                        action->recentRejects->insert(orphanHash);
                    }
                    action->mempool.check(action->coins);
                }
            }
            
            BOOST_FOREACH(uint256 hash, vEraseQueue)
            EraseOrphanTx(action, hash);
        }
        else if (fMissingInputs)
        {
            AddOrphanTx(tx, pfrom->GetId());
            
            // DoS prevention: do not allow mapOrphanTransactions to grow unbounded
            unsigned int nMaxOrphanTx = (unsigned int)std::max((int64_t)0, GetArg("-maxorphantx", DEFAULT_MAX_ORPHAN_TRANSACTIONS));
            unsigned int nEvicted = LimitOrphanTxSize(nMaxOrphanTx);
            if (nEvicted > 0)
                LogPrint("mempool", "mapOrphan overflow, removed %u tx\n", nEvicted);
        } else {
            assert(action->recentRejects);
            action->recentRejects->insert(tx.GetHash());
            
            if (pfrom->fWhitelisted && GetBoolArg("-whitelistforcerelay", DEFAULT_WHITELISTFORCERELAY)) {
                // Always relay transactions received from whitelisted peers, even
                // if they were already in the mempool or rejected from it due
                // to policy, allowing the node to function as a gateway for
                // nodes hidden behind it.
                //
                // Never relay transactions that we would assign a non-zero DoS
                // score for, as we expect peers to do the same with us in that
                // case.
                int nDoS = 0;
                if (!state.IsInvalid(nDoS) || nDoS == 0) {
                    LogPrintf("Force relaying tx %s from whitelisted peer=%d\n", tx.GetHash().ToString(), pfrom->id);
                    RelayTransaction(tx);
                } else {
                    LogPrintf("Not relaying invalid transaction %s from whitelisted peer=%d (%s)\n", tx.GetHash().ToString(), pfrom->id, FormatStateMessage(state));
                }
            }
        }
        int nDoS = 0;
        if (state.IsInvalid(nDoS))
        {
            LogPrint("mempoolrej", "%s from peer=%d was not accepted: %s\n", tx.GetHash().ToString(),
                     pfrom->id,
                     FormatStateMessage(state));
            if (state.GetRejectCode() < REJECT_INTERNAL) // Never send AcceptToMemoryPool's internal codes over P2P
                pfrom->PushMessage(action_identifier,NetMsgType::REJECT, strCommand, (unsigned char)state.GetRejectCode(),
                                   state.GetRejectReason().substr(0, MAX_REJECT_MESSAGE_LENGTH), inv.hash);
            if (nDoS > 0)
                Misbehaving(pfrom->GetId(), nDoS);
        }
        FlushStateToDisk(action, state, FLUSH_STATE_PERIODIC);
    }
    
    
    else if (strCommand == NetMsgType::HEADERS && !action->fImporting && !action->fReindex) // Ignore headers received while importing
    {
        std::vector<CBlockHeader> headers;
        
        // Bypass the normal CBlock deserialization, as we don't want to risk deserializing 2000 full blocks.
        unsigned int nCount = ReadCompactSize(vRecv);
        if (nCount > MAX_HEADERS_RESULTS) {
            Misbehaving(pfrom->GetId(), 20);
            return error("headers message size = %u", nCount);
        }
        headers.resize(nCount);
        for (unsigned int n = 0; n < nCount; n++) {
            vRecv >> headers[n];
            ReadCompactSize(vRecv); // ignore tx count; assume it is 0.
        }
        
        LOCK(cs_main);
        
        if (nCount == 0) {
            // Nothing interesting. Stop asking this peers for more headers.
            return true;
        }
        
        CBlockIndex *pindexLast = NULL;
        BOOST_FOREACH(const CBlockHeader& header, headers) {
            CValidationState state;
            if (pindexLast != NULL && header.hashPrevBlock != pindexLast->GetBlockHash()) {
                Misbehaving(pfrom->GetId(), 20);
                return error("non-continuous headers sequence");
            }
            if (!AcceptBlockHeader(header, state, chainparams, &pindexLast)) {
                int nDoS;
                if (state.IsInvalid(nDoS)) {
                    if (nDoS > 0)
                        Misbehaving(pfrom->GetId(), nDoS);
                    return error("invalid header received");
                }
            }
        }
        
        if (pindexLast)
            UpdateBlockAvailability(action_identifier, pfrom->GetId(), pindexLast->GetBlockHash());
        
        if (nCount == MAX_HEADERS_RESULTS && pindexLast) {
            // Headers message had its maximum size; the peer may have more headers.
            // TODO: optimize: if pindexLast is an ancestor of chainActive.Tip or pindexBestHeader, continue
            // from there instead.
            LogPrint("net", "more getheaders (%d) to end to peer=%d for %s (startheight:%d)\n", pindexLast->nHeight, pfrom->id, action_identifier, nodeAction->nStartingHeight);
            pfrom->PushMessage(action_identifier,NetMsgType::GETHEADERS, action->chain->GetLocator(pindexLast), uint256());
        }
        
        bool fCanDirectFetch = CanDirectFetch(action_identifier, action->params->GetConsensus());
        CNodeState *nodestate = State(pfrom->GetId());
        // If this set of headers is valid and ends in a block with at least as
        // much work as our tip, download as much as possible.
        if (fCanDirectFetch && pindexLast->IsValid(BLOCK_VALID_TREE) && action->chain->Tip()->nChainWork <= pindexLast->nChainWork) {
            vector<CBlockIndex *> vToFetch;
            CBlockIndex *pindexWalk = pindexLast;
            // Calculate all the blocks we'd need to switch to pindexLast, up to a limit.
            while (pindexWalk && !action->chain->Contains(pindexWalk) && vToFetch.size() <= MAX_BLOCKS_IN_TRANSIT_PER_PEER) {
                if (!(pindexWalk->nStatus & BLOCK_HAVE_DATA) &&
                    !mapBlocksInFlight.count(pindexWalk->GetBlockHash())) {
                    // We don't have this block, and it's not yet in flight.
                    vToFetch.push_back(pindexWalk);
                }
                pindexWalk = pindexWalk->pprev;
            }
            // If pindexWalk still isn't on our main chain, we're looking at a
            // very large reorg at a time we think we're close to caught up to
            // the main chain -- this shouldn't really happen.  Bail out on the
            // direct fetch and rely on parallel download instead.
            if (!action->chain->Contains(pindexWalk)) {
                LogPrint("net", "Large reorg, won't direct fetch to %s (%d)\n",
                         pindexLast->GetBlockHash().ToString(),
                         pindexLast->nHeight);
            } else {
                vector<CInv> vGetData;
                // Download as much as possible, from earliest to latest.
                BOOST_REVERSE_FOREACH(CBlockIndex *pindex, vToFetch) {
                    if (nodestate->nBlocksInFlight >= MAX_BLOCKS_IN_TRANSIT_PER_PEER) {
                        // Can't download any more from this peer
                        break;
                    }
                    vGetData.push_back(CInv(action_identifier, MSG_BLOCK, pindex->GetBlockHash()));
                    MarkBlockAsInFlight(action_identifier, pfrom->GetId(), pindex->GetBlockHash(), action->params->GetConsensus(), pindex);
                    LogPrint("net", "Requesting block %s from  peer=%d\n",
                             pindex->GetBlockHash().ToString(), pfrom->id);
                }
                if (vGetData.size() > 1) {
                    LogPrint("net", "Downloading blocks toward %s (%d) via headers direct fetch\n",
                             pindexLast->GetBlockHash().ToString(), pindexLast->nHeight);
                }
                if (vGetData.size() > 0) {
                    pfrom->PushMessage(action_identifier,NetMsgType::GETDATA, vGetData);
                }
            }
        }
        
        CheckBlockIndex(action_identifier, action->params->GetConsensus());
    }
    
    else if (strCommand == NetMsgType::BLOCK && !action->fImporting && !action->fReindex) // Ignore blocks received while importing
    {
        CBlock block;
        block.action_identifier = action_identifier;
        vRecv >> block;
        
        CInv inv(action_identifier, MSG_BLOCK, block.GetHash());
        LogPrint("net", "received block %s peer=%d\n", inv.hash.ToString(), pfrom->id);
        
        pfrom->AddInventoryKnown(inv);
        
        CValidationState state;
        // Process all blocks from whitelisted peers, even if not requested,
        // unless we're still syncing with the network.
        // Such an unrequested block may still be processed, subject to the
        // conditions in AcceptBlock().
        bool forceProcessing = pfrom->fWhitelisted && !IsInitialBlockDownload(action);
        ProcessNewBlock(state, chainparams, pfrom, &block, forceProcessing, NULL);
        int nDoS;
        if (state.IsInvalid(nDoS)) {
            assert (state.GetRejectCode() < REJECT_INTERNAL); // Blocks are never rejected with internal reject codes
            pfrom->PushMessage(action_identifier,NetMsgType::REJECT, strCommand, (unsigned char)state.GetRejectCode(),
                               state.GetRejectReason().substr(0, MAX_REJECT_MESSAGE_LENGTH), inv.hash);
            if (nDoS > 0) {
                LOCK(cs_main);
                Misbehaving(pfrom->GetId(), nDoS);
            }
        }
        
    }
    
    
    // This asymmetric behavior for inbound and outbound connections was introduced
    // to prevent a fingerprinting attack: an attacker can send specific fake addresses
    // to users' AddrMan and later request them by sending getaddr messages.
    // Making nodes which are behind NAT and can only make outgoing connections ignore
    // the getaddr message mitigates the attack.
    else if ((strCommand == NetMsgType::GETADDR) && (pfrom->fInbound))
    {
        pfrom->vAddrToSend.clear();
        vector<CAddress> vAddr = addrman.GetAddr();
        BOOST_FOREACH(const CAddress &addr, vAddr)
        pfrom->PushAddress(addr);
    }
    
    
    else if (strCommand == NetMsgType::MEMPOOL)
    {
        if (CNode::OutboundTargetReached(false) && !pfrom->fWhitelisted)
        {
            LogPrint("net", "mempool request with bandwidth limit reached, disconnect peer=%d\n", pfrom->GetId());
            pfrom->fDisconnect = true;
            return true;
        }
        LOCK2(cs_main, pfrom->cs_filter);
        
        std::vector<uint256> vtxid;
        action->mempool.queryHashes(vtxid);
        vector<CInv> vInv;
        BOOST_FOREACH(uint256& hash, vtxid) {
            CInv inv(action_identifier, MSG_TX, hash);
            if (pfrom->pfilter) {
                CTransaction tx;
                bool fInMemPool = action->mempool.lookup(hash, tx);
                if (!fInMemPool) continue; // another thread removed since queryHashes, maybe...
                if (!pfrom->pfilter->IsRelevantAndUpdate(tx)) continue;
            }
            vInv.push_back(inv);
            if (vInv.size() == MAX_INV_SZ) {
                pfrom->PushMessage(action_identifier,NetMsgType::INV, vInv);
                vInv.clear();
            }
        }
        if (vInv.size() > 0)
            pfrom->PushMessage(action_identifier,NetMsgType::INV, vInv);
    }
    
    
    else if (strCommand == NetMsgType::PING)
    {
        if (pfrom->nVersion > BIP0031_VERSION)
        {
            uint64_t nonce = 0;
            vRecv >> nonce;
            // Echo the message back with the nonce. This allows for two useful features:
            //
            // 1) A remote node can quickly check if the connection is operational
            // 2) Remote nodes can measure the latency of the network thread. If this node
            //    is overloaded it won't respond to pings quickly and the remote node can
            //    avoid sending us more work, like chain download requests.
            //
            // The nonce stops the remote getting confused between different pings: without
            // it, if the remote node sends a ping once per second and this node takes 5
            // seconds to respond to each, the 5th ping the remote sends would appear to
            // return very quickly.
            pfrom->PushMessage(action_identifier,NetMsgType::PONG, nonce);
        }
    }
    
    
    else if (strCommand == NetMsgType::PONG)
    {
        int64_t pingUsecEnd = nTimeReceived;
        uint64_t nonce = 0;
        size_t nAvail = vRecv.in_avail();
        bool bPingFinished = false;
        std::string sProblem;
        
        if (nAvail >= sizeof(nonce)) {
            vRecv >> nonce;
            
            // Only process pong message if there is an outstanding ping (old ping without nonce should never pong)
            if (pfrom->nPingNonceSent != 0) {
                if (nonce == pfrom->nPingNonceSent) {
                    // Matching pong received, this ping is no longer outstanding
                    bPingFinished = true;
                    int64_t pingUsecTime = pingUsecEnd - pfrom->nPingUsecStart;
                    if (pingUsecTime > 0) {
                        // Successful ping time measurement, replace previous
                        pfrom->nPingUsecTime = pingUsecTime;
                        pfrom->nMinPingUsecTime = std::min(pfrom->nMinPingUsecTime, pingUsecTime);
                    } else {
                        // This should never happen
                        sProblem = "Timing mishap";
                    }
                } else {
                    // Nonce mismatches are normal when pings are overlapping
                    sProblem = "Nonce mismatch";
                    if (nonce == 0) {
                        // This is most likely a bug in another implementation somewhere; cancel this ping
                        bPingFinished = true;
                        sProblem = "Nonce zero";
                    }
                }
            } else {
                sProblem = "Unsolicited pong without ping";
            }
        } else {
            // This is most likely a bug in another implementation somewhere; cancel this ping
            bPingFinished = true;
            sProblem = "Short payload";
        }
        
        if (!(sProblem.empty())) {
            LogPrint("net", "pong peer=%d: %s, %x expected, %x received, %u bytes\n",
                     pfrom->id,
                     sProblem,
                     pfrom->nPingNonceSent,
                     nonce,
                     nAvail);
        }
        if (bPingFinished) {
            pfrom->nPingNonceSent = 0;
        }
    }
    
    
    else if (fAlerts && strCommand == NetMsgType::ALERT)
    {
        CAlert alert;
        vRecv >> alert;
        
        uint256 alertHash = alert.GetHash();
        if (pfrom->setKnown.count(alertHash) == 0)
        {
            if (alert.ProcessAlert(shares()->AlertKey()))
            {
                // Relay
                pfrom->setKnown.insert(alertHash);
                {
                    LOCK(cs_vNodes);
                    BOOST_FOREACH(CNode* pnode, vNodes)
                    alert.RelayTo(action_identifier, pnode);
                }
            }
            else {
                // Small DoS penalty so peers that send us lots of
                // duplicate/expired/invalid-signature/whatever alerts
                // eventually get banned.
                // This isn't a Misbehaving(100) (immediate ban) because the
                // peer might be an older or different implementation with
                // a different signature key, etc.
                Misbehaving(pfrom->GetId(), 10);
            }
        }
    }
    
    
    else if (strCommand == NetMsgType::FILTERLOAD)
    {
        CBloomFilter filter;
        vRecv >> filter;
        
        if (!filter.IsWithinSizeConstraints())
            // There is no excuse for sending a too-large filter
            Misbehaving(pfrom->GetId(), 100);
        else
        {
            LOCK(pfrom->cs_filter);
            delete pfrom->pfilter;
            pfrom->pfilter = new CBloomFilter(filter);
            pfrom->pfilter->UpdateEmptyFull();
        }
        pfrom->fRelayTxes = true;
    }
    
    
    else if (strCommand == NetMsgType::FILTERADD)
    {
        vector<unsigned char> vData;
        vRecv >> vData;
        
        // Nodes must NEVER send a data item > 520 bytes (the max size for a script data object,
        // and thus, the maximum size any matched object can have) in a filteradd message
        if (vData.size() > MAX_SCRIPT_ELEMENT_SIZE)
        {
            Misbehaving(pfrom->GetId(), 100);
        } else {
            LOCK(pfrom->cs_filter);
            if (pfrom->pfilter)
                pfrom->pfilter->insert(vData);
            else
                Misbehaving(pfrom->GetId(), 100);
        }
    }
    
    
    else if (strCommand == NetMsgType::FILTERCLEAR)
    {
        LOCK(pfrom->cs_filter);
        delete pfrom->pfilter;
        pfrom->pfilter = new CBloomFilter();
        pfrom->fRelayTxes = true;
    }
    
    
    else if (strCommand == NetMsgType::REJECT)
    {
        if (fDebug) {
            try {
                string strMsg; unsigned char ccode; string strReason;
                vRecv >> LIMITED_STRING(strMsg, CMessageHeader::COMMAND_SIZE) >> ccode >> LIMITED_STRING(strReason, MAX_REJECT_MESSAGE_LENGTH);
                
                ostringstream ss;
                ss << strMsg << " code " << itostr(ccode) << ": " << strReason;
                
                if (strMsg == NetMsgType::BLOCK || strMsg == NetMsgType::TX)
                {
                    uint256 hash;
                    vRecv >> hash;
                    ss << ": hash " << hash.ToString();
                }
                LogPrint("net", "Reject %s\n", SanitizeString(ss.str()));
            } catch (const std::ios_base::failure&) {
                // Avoid feedback loops by preventing reject messages from triggering a new reject message.
                LogPrint("net", "Unparseable reject message received\n");
            }
        }
    }
    
    else if(strCommand == NetMsgType::ALLOWEDACTIONS)
    {
        std::cout << "Allowed actions header receive\n";
    }
    
    else
    {
        // Ignore unknown commands for extensibility
        LogPrint("net", "Unknown command \"%s\" from peer=%d\n", SanitizeString(strCommand), pfrom->id);
    }
    
    
    
    return true;
}

// requires LOCK(cs_vRecvMsg)
bool ProcessMessages(CNode* pfrom)
{
    //if (fDebug)
    //    LogPrintf("%s(%u messages)\n", __func__, pfrom->vRecvMsg.size());
    
    //
    // Message format
    //  (4) message start
    //  (7) action identifier
    //  (13) command
    //  (4) size
    //  (4) checksum
    //  (x) data
    //
    bool fOk = true;
    
    if (!pfrom->vRecvGetData.empty())
        ProcessGetData(pfrom);
    
    // this maintains the order of responses
    if (!pfrom->vRecvGetData.empty()) return fOk;
    
    std::deque<CNetMessage>::iterator it = pfrom->vRecvMsg.begin();
    while (!pfrom->fDisconnect && it != pfrom->vRecvMsg.end()) {
        // Don't bother if send buffer is too full to respond anyway
        if (pfrom->nSendSize >= SendBufferSize())
            break;
        
        // get next message
        CNetMessage& msg = *it;
        
        //if (fDebug)
        //    LogPrintf("%s(message %u msgsz, %u bytes, complete:%s)\n", __func__,
        //            msg.hdr.nMessageSize, msg.vRecv.size(),
        //            msg.complete() ? "Y" : "N");
        
        // end, if an incomplete message is found
        if (!msg.complete())
            break;
        
        // at this point, any failure means we can delete the current message
        it++;
        
        // Scan for message start
        if (memcmp(msg.hdr.pchMessageStart, shares()->MessageStart(), MESSAGE_START_SIZE) != 0) {
            LogPrintf("PROCESSMESSAGE: INVALID MESSAGESTART %s peer=%d\n", SanitizeString(msg.hdr.GetCommand()), pfrom->id);
            fOk = false;
            break;
        }
        
        // Read header
        CMessageHeader& hdr = msg.hdr;
        if (!hdr.IsValid(shares()->MessageStart()))
        {
            LogPrintf("PROCESSMESSAGE: ERRORS IN HEADER %s peer=%d\n", SanitizeString(hdr.GetCommand()), pfrom->id);
            pfrom->CloseSocketDisconnect();
            continue;
        }
        string strCommand = hdr.GetCommand();
        
        // Message size
        unsigned int nMessageSize = hdr.nMessageSize;
        
        // Checksum
        CDataStream& vRecv = msg.vRecv;
        uint256 hash = Hash(vRecv.begin(), vRecv.begin() + nMessageSize);
        unsigned int nChecksum = ReadLE32((unsigned char*)&hash);
        if (nChecksum != hdr.nChecksum)
        {
            LogPrintf("%s(%s, %u bytes): CHECKSUM ERROR nChecksum=%08x hdr.nChecksum=%08x\n", __func__,
                      SanitizeString(strCommand), nMessageSize, nChecksum, hdr.nChecksum);
            continue;
        }
        
        // Process message
        bool fRet = false;
        try
        {
            fRet = ProcessMessage(pfrom, hdr.action_identifier, strCommand, vRecv, msg.nTime);
            boost::this_thread::interruption_point();
        }
        catch (const std::ios_base::failure& e)
        {
            pfrom->PushMessage(hdr.action_identifier, NetMsgType::REJECT, strCommand, REJECT_MALFORMED, string("error parsing message"));
            if (strstr(e.what(), "end of data"))
            {
                // Allow exceptions from under-length message on vRecv
                LogPrintf("%s(%s, %u bytes): Exception '%s' caught, normally caused by a message being shorter than its stated length\n", __func__, SanitizeString(strCommand), nMessageSize, e.what());
            }
            else if (strstr(e.what(), "size too large"))
            {
                // Allow exceptions from over-long size
                LogPrintf("%s(%s, %u bytes): Exception '%s' caught\n", __func__, SanitizeString(strCommand), nMessageSize, e.what());
            }
            else
            {
                PrintExceptionContinue(&e, "ProcessMessages()");
            }
        }
        catch (const boost::thread_interrupted&) {
            throw;
        }
        catch (const std::exception& e) {
            PrintExceptionContinue(&e, "ProcessMessages()");
        } catch (...) {
            PrintExceptionContinue(NULL, "ProcessMessages()");
        }
        
        if (!fRet)
            LogPrintf("%s(%s, %u bytes) FAILED peer=%d\n", __func__, SanitizeString(strCommand), nMessageSize, pfrom->id);
        
        break;
    }
    
    // In case the connection got shut down, its receive buffer was wiped
    if (!pfrom->fDisconnect)
        pfrom->vRecvMsg.erase(pfrom->vRecvMsg.begin(), it);
    
    return fOk;
}


bool SendMessages(FAction *action, CNode* pto)
{
    if(action->genesisSearching)
        return false;
    
    CNodeActionInfo *nodeAction = pto->action(action->action_identifier());

    const Consensus::Params& consensusParams = action->params->GetConsensus();
    {
        // Don't send anything until we get its version message
        if (pto->nVersion == 0)
            return true;
        
        //
        // Message: ping
        //
        bool pingSend = false;
        if (pto->fPingQueued) {
            // RPC ping request by user
            pingSend = true;
        }
        if (pto->nPingNonceSent == 0 && pto->nPingUsecStart + PING_INTERVAL * 1000000 < GetTimeMicros()) {
            // Ping automatically sent as a latency probe & keepalive.
            pingSend = true;
        }
        if (pingSend) {
            uint64_t nonce = 0;
            while (nonce == 0) {
                GetRandBytes((unsigned char*)&nonce, sizeof(nonce));
            }
            pto->fPingQueued = false;
            pto->nPingUsecStart = GetTimeMicros();
            if (pto->nVersion > BIP0031_VERSION) {
                pto->nPingNonceSent = nonce;
                pto->PushMessage(action->action_identifier(), NetMsgType::PING, nonce);
            } else {
                // Peer is too old to support ping command with nonce, pong will never arrive.
                pto->nPingNonceSent = 0;
                pto->PushMessage(action->action_identifier(), NetMsgType::PING);
            }
        }
        
        TRY_LOCK(cs_main, lockMain); // Acquire cs_main for IsInitialBlockDownload() and CNodeState()
        if (!lockMain)
            return true;
        
        // Address refresh broadcast
        int64_t nNow = GetTimeMicros();
        if (!IsInitialBlockDownload(action) && pto->nNextLocalAddrSend < nNow) {
            AdvertiseLocal(pto);
            pto->nNextLocalAddrSend = PoissonNextSend(nNow, AVG_LOCAL_ADDRESS_BROADCAST_INTERVAL);
        }
        
        //
        // Message: addr
        //
        if (pto->nNextAddrSend < nNow) {
            pto->nNextAddrSend = PoissonNextSend(nNow, AVG_ADDRESS_BROADCAST_INTERVAL);
            vector<CAddress> vAddr;
            vAddr.reserve(pto->vAddrToSend.size());
            BOOST_FOREACH(const CAddress& addr, pto->vAddrToSend)
            {
                if (!pto->addrKnown.contains(addr.GetKey()))
                {
                    pto->addrKnown.insert(addr.GetKey());
                    vAddr.push_back(addr);
                    // receiver rejects addr messages larger than 1000
                    if (vAddr.size() >= 1000)
                    {
                        pto->PushMessage(action->action_identifier(), NetMsgType::ADDR, vAddr);
                        vAddr.clear();
                    }
                }
            }
            pto->vAddrToSend.clear();
            if (!vAddr.empty())
                pto->PushMessage(action->action_identifier(), NetMsgType::ADDR, vAddr);
        }
        
        CNodeState &state = *State(pto->GetId());
        if (state.fShouldBan) {
            if (pto->fWhitelisted)
                LogPrintf("Warning: not punishing whitelisted peer %s!\n", pto->addr.ToString());
            else {
                pto->fDisconnect = true;
                if (pto->addr.IsLocal())
                    LogPrintf("Warning: not banning local peer %s!\n", pto->addr.ToString());
                else
                {
                    CNode::Ban(pto->addr, BanReasonNodeMisbehaving);
                }
            }
            state.fShouldBan = false;
        }
        
        BOOST_FOREACH(const CBlockReject& reject, state.rejects)
        pto->PushMessage(action->action_identifier(), NetMsgType::REJECT, (string)NetMsgType::BLOCK, reject.chRejectCode, reject.strRejectReason, reject.hashBlock);
        state.rejects.clear();
        
        // Start block sync
        if (action->pindexBestHeader == NULL)
            action->pindexBestHeader = action->chain->Tip();
        bool fFetch = state.fPreferredDownload || (nPreferredDownload == 0 && !pto->fClient && !pto->fOneShot); // Download if this is a nice peer, or we have no nice peers and this one might do.
        if (!state.state(action->action_identifier())->fSyncStarted && !pto->fClient && !action->fImporting && !action->fReindex) {
            // Only actively request headers from a single peer, unless we're close to today.
            if ((actionSyncStarted[action->action_identifier()] == 0 && fFetch) || action->pindexBestHeader->GetBlockTime() > GetAdjustedTime() - 24 * 60 * 60) {
                state.state(action->action_identifier())->fSyncStarted = true;
                actionSyncStarted[action->action_identifier()]++;
                const CBlockIndex *pindexStart = action->pindexBestHeader;
                /* If possible, start at the block preceding the currently
                 best known header.  This ensures that we always get a
                 non-empty list of headers back as long as the peer
                 is up-to-date.  With a non-empty response, we can initialise
                 the peer's known best block.  This wouldn't be possible
                 if we requested starting at pindexBestHeader and
                 got back an empty response.  */
    
                if (pindexStart->pprev)
                    pindexStart = pindexStart->pprev;
                LogPrint("net", "initial getheaders (%d) to peer=%d identifier=%s (startheight:%d)\n", pindexStart->nHeight, pto->id, action->action_identifier(), nodeAction->nStartingHeight);
                pto->PushMessage(action->action_identifier(), NetMsgType::GETHEADERS, action->chain->GetLocator(pindexStart), uint256());
            }
        }
        
        // Resend wallet transactions that haven't gotten in a block yet
        // Except during reindex, importing and IBD, when old wallet
        // transactions become unconfirmed and spams other nodes.
        if (!action->fReindex && !action->fImporting && !IsInitialBlockDownload(action))
        {
            GetMainSignals().Broadcast(nTimeBestReceived);
        }
        
        //
        // Try sending block announcements via headers
        //
        {
            // If we have less than MAX_BLOCKS_TO_ANNOUNCE in our
            // list of block hashes we're relaying, and our peer wants
            // headers announcements, then find the first header
            // not yet known to our peer but would connect, and send.
            // If no header would connect, or if we have too many
            // blocks, or if the peer doesn't want headers, just
            // add all to the inv queue.
            LOCK(pto->cs_inventory);
            vector<CBlock> vHeaders;
            bool fRevertToInv = (!state.fPreferHeaders || nodeAction->vBlockHashesToAnnounce.size() > MAX_BLOCKS_TO_ANNOUNCE);
            CBlockIndex *pBestIndex = NULL; // last header queued for delivery
            ProcessBlockAvailability(pto->id); // ensure pindexBestKnownBlock is up-to-date
            
            if (!fRevertToInv) {
                bool fFoundStartingHeader = false;
                // Try to find first header that our peer doesn't have, and
                // then send all headers past that one.  If we come across any
                // headers that aren't on chainActive, give up.
                BOOST_FOREACH(const uint256 &hash, nodeAction->vBlockHashesToAnnounce) {
                    BlockMap::iterator mi = action->blocks.find(hash);
                    assert(mi != action->blocks.end());
                    CBlockIndex *pindex = mi->second;
                    if ((*(action->chain))[pindex->nHeight] != pindex) {
                        // Bail out if we reorged away from this block
                        fRevertToInv = true;
                        break;
                    }
                    assert(pBestIndex == NULL || pindex->pprev == pBestIndex);
                    pBestIndex = pindex;
                    if (fFoundStartingHeader) {
                        // add this to the headers message
                        vHeaders.push_back(pindex->GetBlockHeader());
                    } else if (PeerHasHeader(&state, pindex)) {
                        continue; // keep looking for the first new block
                    } else if (pindex->pprev == NULL || PeerHasHeader(&state, pindex->pprev)) {
                        // Peer doesn't have this header but they do have the prior one.
                        // Start sending headers.
                        fFoundStartingHeader = true;
                        vHeaders.push_back(pindex->GetBlockHeader());
                    } else {
                        // Peer doesn't have this header or the prior one -- nothing will
                        // connect, so bail out.
                        fRevertToInv = true;
                        break;
                    }
                }
            }
            if (fRevertToInv) {
                // If falling back to using an inv, just try to inv the tip.
                // The last entry in vBlockHashesToAnnounce was our tip at some point
                // in the past.
                if (!nodeAction->vBlockHashesToAnnounce.empty()) {
                    const uint256 &hashToAnnounce = nodeAction->vBlockHashesToAnnounce.back();
                    BlockMap::iterator mi = action->blocks.find(hashToAnnounce);
                    assert(mi != action->blocks.end());
                    CBlockIndex *pindex = mi->second;
                    
                    // Warn if we're announcing a block that is not on the main chain.
                    // This should be very rare and could be optimized out.
                    // Just log for now.
                    if ((*(action->chain))[pindex->nHeight] != pindex) {
                        LogPrint("net", "Announcing block %s not on main chain (tip=%s)\n",
                                 hashToAnnounce.ToString(), action->chain->Tip()->GetBlockHash().ToString());
                    }
                    
                    // If the peer announced this block to us, don't inv it back.
                    // (Since block announcements may not be via inv's, we can't solely rely on
                    // setInventoryKnown to track this.)
                    if (!PeerHasHeader(&state, pindex)) {
                        pto->PushInventory(CInv(action->action_identifier(), MSG_BLOCK, hashToAnnounce));
                        LogPrint("net", "%s: sending inv peer=%d for=%s, hash=%s\n", __func__,
                                 pto->id, action->action_identifier(), hashToAnnounce.ToString());
                    }
                }
            } else if (!vHeaders.empty()) {
                if (vHeaders.size() > 1) {
                    LogPrint("net", "%s: %u headers, range (%s, %s) for %s, to peer=%d\n", __func__,
                             vHeaders.size(),
                             vHeaders.front().GetHash().ToString(),
                             vHeaders.back().GetHash().ToString(), action->action_identifier(), pto->id);
                } else {
                    LogPrint("net", "%s: sending header %s for %s to peer=%d\n", __func__,
                             vHeaders.front().GetHash().ToString(), action->action_identifier(), pto->id);
                }
                pto->PushMessage(action->action_identifier(), NetMsgType::HEADERS, vHeaders);
                state.state(action->action_identifier())->pindexBestHeaderSent = pBestIndex;
            }
            nodeAction->vBlockHashesToAnnounce.clear();
        }
        
        //
        // Message: inventory
        //
        vector<CInv> vInv;
        vector<CInv> vInvWait;
        {
            bool fSendTrickle = pto->fWhitelisted;
            if (pto->nNextInvSend < nNow) {
                fSendTrickle = true;
                pto->nNextInvSend = PoissonNextSend(nNow, AVG_INVENTORY_BROADCAST_INTERVAL);
            }
            LOCK(pto->cs_inventory);
            vInv.reserve(std::min<size_t>(1000, pto->vInventoryToSend.size()));
            vInvWait.reserve(pto->vInventoryToSend.size());
            BOOST_FOREACH(const CInv& inv, pto->vInventoryToSend)
            {
                if (inv.type == MSG_TX && pto->filterInventoryKnown.contains(inv.hash))
                    continue;
                
                // trickle out tx inv to protect privacy
                if (inv.type == MSG_TX && !fSendTrickle)
                {
                    // 1/4 of tx invs blast to all immediately
                    static uint256 hashSalt;
                    if (hashSalt.IsNull())
                        hashSalt = GetRandHash();
                    uint256 hashRand = ArithToUint256(UintToArith256(inv.hash) ^ UintToArith256(hashSalt));
                    hashRand = Hash(BEGIN(hashRand), END(hashRand));
                    bool fTrickleWait = ((UintToArith256(hashRand) & 3) != 0);
                    
                    if (fTrickleWait)
                    {
                        vInvWait.push_back(inv);
                        continue;
                    }
                }
                
                pto->filterInventoryKnown.insert(inv.hash);
                
                vInv.push_back(inv);
                if (vInv.size() >= 1000)
                {
                    pto->PushMessage(action->action_identifier(), NetMsgType::INV, vInv);
                    vInv.clear();
                }
            }
            pto->vInventoryToSend = vInvWait;
        }
        if (!vInv.empty())
            pto->PushMessage(action->action_identifier(), NetMsgType::INV, vInv);
        
        // Detect whether we're stalling
        nNow = GetTimeMicros();
        if (!pto->fDisconnect && state.nStallingSince && state.nStallingSince < nNow - 1000000 * BLOCK_STALLING_TIMEOUT) {
            // Stalling only triggers when the block download window cannot move. During normal steady state,
            // the download window should be much larger than the to-be-downloaded set of blocks, so disconnection
            // should only happen during initial block download.
            LogPrintf("Peer=%d is stalling block download, disconnecting\n", pto->id);
            pto->fDisconnect = true;
        }
        // In case there is a block that has been in flight from this peer for (2 + 0.5 * N) times the block interval
        // (with N the number of validated blocks that were in flight at the time it was requested), disconnect due to
        // timeout. We compensate for in-flight blocks to prevent killing off peers due to our own downstream link
        // being saturated. We only count validated in-flight blocks so peers can't advertise non-existing block hashes
        // to unreasonably increase our timeout.
        // We also compare the block download timeout originally calculated against the time at which we'd disconnect
        // if we assumed the block were being requested now (ignoring blocks we've requested from this peer, since we're
        // only looking at this peer's oldest request).  This way a large queue in the past doesn't result in a
        // permanently large window for this block to be delivered (ie if the number of blocks in flight is decreasing
        // more quickly than once every 5 minutes, then we'll shorten the download window for this block).
        if (!pto->fDisconnect && state.vBlocksInFlight.size() > 0) {
            QueuedBlock &queuedBlock = state.vBlocksInFlight.front();
            int64_t nTimeoutIfRequestedNow = GetBlockTimeout(nNow, nQueuedValidatedHeaders - state.nBlocksInFlightValidHeaders, consensusParams);
            if (queuedBlock.nTimeDisconnect > nTimeoutIfRequestedNow) {
                LogPrint("net", "Reducing block download timeout for peer=%d block=%s, orig=%d new=%d\n", pto->id, queuedBlock.hash.ToString(), queuedBlock.nTimeDisconnect, nTimeoutIfRequestedNow);
                queuedBlock.nTimeDisconnect = nTimeoutIfRequestedNow;
            }
            if (queuedBlock.nTimeDisconnect < nNow) {
                LogPrintf("Timeout downloading block %s from peer=%d, disconnecting\n", queuedBlock.hash.ToString(), pto->id);
                pto->fDisconnect = true;
            }
        }
        
        //
        // Message: getdata (blocks)
        //
        vector<CInv> vGetData;
        if (!pto->fDisconnect && !pto->fClient && (fFetch || !IsInitialBlockDownload(action)) && state.nBlocksInFlight < MAX_BLOCKS_IN_TRANSIT_PER_PEER) {
            vector<CBlockIndex*> vToDownload;
            NodeId staller = -1;
            FindNextBlocksToDownload(action->action_identifier(), pto->GetId(), MAX_BLOCKS_IN_TRANSIT_PER_PEER - state.nBlocksInFlight, vToDownload, staller);
            BOOST_FOREACH(CBlockIndex *pindex, vToDownload) {
                vGetData.push_back(CInv(action->action_identifier(), MSG_BLOCK, pindex->GetBlockHash()));
                MarkBlockAsInFlight(action->action_identifier(), pto->GetId(), pindex->GetBlockHash(), consensusParams, pindex);
                LogPrint("net", "Requesting block %s (%d) peer=%d\n", pindex->GetBlockHash().ToString(),
                         pindex->nHeight, pto->id);
            }
            if (state.nBlocksInFlight == 0 && staller != -1) {
                if (State(staller)->nStallingSince == 0) {
                    State(staller)->nStallingSince = nNow;
                    LogPrint("net", "Stall started peer=%d\n", staller);
                }
            }
        }
        
        //
        // Message: getdata (non-blocks)
        //
        while (!pto->fDisconnect && !pto->mapAskFor.empty() && (*pto->mapAskFor.begin()).first <= nNow)
        {
            const CInv& inv = (*pto->mapAskFor.begin()).second;
            if (!AlreadyHave(inv))
            {
                if (fDebug)
                    LogPrint("net", "Requesting %s peer=%d\n", inv.ToString(), pto->id);
                vGetData.push_back(inv);
                if (vGetData.size() >= 1000)
                {
                    pto->PushMessage(action->action_identifier(), NetMsgType::GETDATA, vGetData);
                    vGetData.clear();
                }
            } else {
                //If we're not going to ask, don't expect a response.
                pto->setAskFor.erase(inv.hash);
            }
            pto->mapAskFor.erase(pto->mapAskFor.begin());
        }
        if (!vGetData.empty())
            pto->PushMessage(action->action_identifier(), NetMsgType::GETDATA, vGetData);
        
    }
    return true;
}

