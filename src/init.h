#ifndef INIT_H
#define INIT_H

#include "util.h"

class CScheduler;

namespace boost
{
class thread_group;
} // namespace boost

void Shutdown();
void StartShutdown();
bool ShutdownRequested();

bool AppInit2(boost::thread_group& threadGroup, CScheduler& scheduler);
void InitParameterInteraction();

/** The help message mode determines what help message to show */
enum HelpMessageMode {
    HMM_BITCOIND,
    HMM_BITCOIN_QT
};

/** Help for options shared between UI and daemon (for -help) */
std::string HelpMessage(HelpMessageMode mode);
/** Returns licensing information (for -version) */
std::string LicenseInfo();

void CleanupBlockRevFiles(std::string action_identifier);
bool InitError(const std::string &str);

void AppendParamsHelpMessages(std::string& strUsage, bool debugHelp=true);
void InitLogging();
void Interrupt(boost::thread_group& threadGroup);


#ifdef ENABLE_WALLET
#include "wallet/wallet.h"
extern CWallet* pwalletMain;
#endif

#endif // INIT_H
