// Copyright (c) 2011-2015 The Bitcoin Core developers
// Distributed under the MIT software license, see the accompanying
// file COPYING or http://www.opensource.org/licenses/mit-license.php.

#define BOOST_TEST_MODULE Bitcoin Test Suite

#include "test_bitcoin.h"

#include "../fameshares.h"
#include "../consensus/consensus.h"
#include "../consensus/validation.h"
#include "../key.h"
#include "../miner.h"
#include "../pubkey.h"
#include "../random.h"
#include "../txdb.h"
#include "../txmempool.h"
#include "../ui_interface.h"
#ifdef ENABLE_WALLET
#include "../wallet/db.h"
#include "../wallet/wallet.h"
#endif

#include "./testutil.h"
#include "fameshares.h"
#include "util.h"
#include "block_utils.h"
#include "miner.h"

#include <boost/filesystem.hpp>
#include <boost/test/unit_test.hpp>
#include <boost/thread.hpp>

CClientUIInterface uiInterface; // Declared but not defined in ui_interface.h
CWallet* pwalletMain;

extern bool fPrintToConsole;
extern void noui_connect();
extern FAction *action;

BasicTestingSetup::BasicTestingSetup(const std::string& chainName)
{
    ECC_Start();
    SetupEnvironment();
    SetupNetworking();
    fPrintToDebugLog = false; // don't want to write to debug.log file
    fCheckBlockIndex = true;
    
    if(chainName == "main")
    {
        init_shares(mode_main);
    }
    
    noui_connect();
}

BasicTestingSetup::~BasicTestingSetup()
{
    ECC_Stop();
}

TestingSetup::TestingSetup(std::string action_identifier, const std::string& chainName) : BasicTestingSetup(chainName)
{
    action = getAction(action_identifier);
    FChainParams* chainparams = action->params;
#ifdef ENABLE_WALLET
    bitdb.MakeMock();
    walletRegisterRPCCommands();
#endif
    ClearDatadirCache();
    pathTemp = GetTempPath() / strprintf("test_bitcoin_%lu_%i", (unsigned long)GetTime(), (int)(GetRand(100000)));
    boost::filesystem::create_directories(pathTemp);
    mapArgs["-datadir"] = pathTemp.string();
    action->db = new CBlockTreeDB(action_identifier, 1 << 20, true);
    action->coinsdbview = new CCoinsViewDB(action_identifier, 1 << 23, true);
    action->coins = new CCoinsViewCache(pcoinsdbview);
    InitBlockIndex(chainparams);
#ifdef ENABLE_WALLET
    bool fFirstRun;
    pwalletMain = new CWallet("wallet.dat");
    pwalletMain->LoadWallet(fFirstRun);
    RegisterValidationInterface(pwalletMain);
#endif
    nScriptCheckThreads = 3;
    for (int i=0; i < nScriptCheckThreads-1; i++)
        threadGroup.create_thread(&ThreadScriptCheck);
    RegisterNodeSignals(GetNodeSignals());
}

TestingSetup::~TestingSetup()
{
    UnregisterNodeSignals(GetNodeSignals());
    threadGroup.interrupt_all();
    threadGroup.join_all();
#ifdef ENABLE_WALLET
    UnregisterValidationInterface(pwalletMain);
    delete pwalletMain;
    pwalletMain = NULL;
#endif
    UnloadBlockIndex(action);
    
    //delete pcoinsTip;
    //delete pcoinsdbview;
    //delete pblocktree;
    
    delete action->coins;
    delete action->coinsdbview;
    delete action->db;
    
#ifdef ENABLE_WALLET
    bitdb.Flush(true);
    bitdb.Reset();
#endif
    boost::filesystem::remove_all(pathTemp);
}

TestChain100Setup::TestChain100Setup(std::string action_identifier) : TestingSetup(action_identifier, "regtest")
{
    // Generate a 100-block chain:
    coinbaseKey.MakeNewKey(true);
    CScript scriptPubKey = CScript() <<  ToByteVector(coinbaseKey.GetPubKey()) << OP_CHECKSIG;
    for (int i = 0; i < COINBASE_MATURITY; i++)
    {
        std::vector<CMutableTransaction> noTxns;
        CBlock b = CreateAndProcessBlock(noTxns, scriptPubKey);
        coinbaseTxns.push_back(b.vtx[0]);
    }
}

//
// Create a new block with just given transactions, coinbase paying to
// scriptPubKey, and try to add it to the current chain.
//
CBlock
TestChain100Setup::CreateAndProcessBlock(const std::vector<CMutableTransaction>& txns, const CScript& scriptPubKey)
{
    FChainParams *chainparams = action->params;
    CBlockTemplate *pblocktemplate = CreateNewBlock(chainparams, scriptPubKey);
    CBlock& block = pblocktemplate->block;
    
    // Replace mempool-selected txns with just coinbase plus passed-in txns:
    block.vtx.resize(1);
    BOOST_FOREACH(const CMutableTransaction& tx, txns)
    block.vtx.push_back(tx);
    // IncrementExtraNonce creates a valid coinbase and merkleRoot
    unsigned int extraNonce = 0;
    
    BlockMap::iterator mi = action->blocks.end();
    CBlockIndex *pindex = mi->second;
    
    IncrementExtraNonce(&block, pindex, extraNonce);
    
    while (!CheckProofOfWork(block.GetHash(), block.nBits, chainparams->GetConsensus())) ++block.nNonce;
    
    CValidationState state;
    ProcessNewBlock(state, chainparams, NULL, &block, true, NULL);
    
    CBlock result = block;
    delete pblocktemplate;
    return result;
}

TestChain100Setup::~TestChain100Setup()
{
}


CTxMemPoolEntry TestMemPoolEntryHelper::FromTx(CMutableTransaction &tx, CTxMemPool *pool) {
    CTransaction txn(tx);
    bool hasNoDependencies = pool ? pool->HasNoInputsOf(tx) : hadNoDependencies;
    // Hack to assume either its completely dependent on other mempool txs or not at all
    CAmount inChainValue = hasNoDependencies ? txn.GetValueOut() : 0;
    
    return CTxMemPoolEntry(txn, nFee, nTime, dPriority, nHeight,
                           hasNoDependencies, inChainValue, spendsCoinbase, sigOpCount);
}

void Shutdown(void* parg)
{
    exit(0);
}

void StartShutdown()
{
    exit(0);
}

bool ShutdownRequested()
{
    return false;
}
