//
//  trans_utils.h
//  Lib-FameCoin
//
//  Created by Sacha Vandamme on 10/03/16.
//
//

#ifndef trans_utils_h
#define trans_utils_h

#include "net_utils.h"
#include "coins.h"
#include "validationinterface.h"
#include "txmempool.h"
#include <string>


//////////////////////////////////////////////////////////////////////////////
//
// mapOrphanTransactions
//

struct FAction;

bool AddOrphanTx(const CTransaction& tx, NodeId peer);
void EraseOrphanTx(FAction *action, uint256 hash);
void EraseOrphansFor(NodeId peer);
unsigned int LimitOrphanTxSize(FAction *action, unsigned int nMaxOrphans);
unsigned int LimitOrphanTxSize(unsigned int nMaxOrphans);
bool IsFinalTx(const CTransaction &tx, int nBlockHeight, int64_t nBlockTime);
bool CheckFinalTx(const CTransaction &tx, int flags);

/*static std::pair<int, int64_t> CalculateSequenceLocks(const CTransaction &tx, int flags, std::vector<int>* prevHeights, const CBlockIndex& block);
static bool EvaluateSequenceLocks(const CBlockIndex& block, std::pair<int, int64_t> lockPair);*/
bool SequenceLocks(const CTransaction &tx, int flags, std::vector<int>* prevHeights, const CBlockIndex& block);
bool CheckSequenceLocks(const CTransaction &tx, int flags);
unsigned int GetLegacySigOpCount(const CTransaction& tx);
unsigned int GetP2SHSigOpCount(const CTransaction& tx, const CCoinsViewCache& inputs);

bool CheckTransaction(const CTransaction& tx, CValidationState &state);
void LimitMempoolSize(std::string action_identifier, CTxMemPool& pool, size_t limit, unsigned long age);
std::string FormatStateMessage(const CValidationState &state);
bool AcceptToMemoryPoolWorker(CTxMemPool& pool, CValidationState& state, const CTransaction& tx, bool fLimitFree,
                              bool* pfMissingInputs, bool fOverrideMempoolLimit, const CAmount nAbsurdFee,
                              std::vector<uint256>& vHashTxnToUncache);
bool AcceptToMemoryPool(CTxMemPool& pool, CValidationState &state, const CTransaction &tx, bool fLimitFree,
                        bool* pfMissingInputs, bool fOverrideMempoolLimit=false, const CAmount nAbsurdFee=0);

bool GetTransaction(std::string action_identifier, const uint256 &hash, CTransaction &txOut, const Consensus::Params& consensusParams, uint256 &hashBlock, bool fAllowSlow);

#endif /* trans_utils_h */
