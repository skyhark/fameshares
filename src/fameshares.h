#ifndef FAMESHARES_H
#define FAMESHARES_H

#include <leveldb/db.h>
#include <univalue/univalue.h>

#include <string>
#include <vector>
#include <set>

enum AppMode { mode_regtest, mode_nettest, mode_main };

#include "utils.h"
#include "uint256.h"
#include "protocol.h"
#include "fchainparams.h"
#include "txdb.h"
#include "primitives/block.h"
#include "blockinfo.h"
#include <boost/filesystem.hpp>
#include <boost/thread.hpp>

class CCoinsViewErrorCatcher;
class CCoinsViewCache;
class CBlockIndex;
struct BlockHasher;
struct CDNSSeedData;
struct SeedSpec6;
struct FAction;
struct COrphanTx;
class CChain;

typedef boost::unordered_map<uint256, CBlockIndex*, BlockHasher> BlockMap;
typedef boost::unordered_map<std::string, FAction*> ActionsMap;

boost::filesystem::path GetDefaultDataDir();

void preinit_shares();
void init_shares(AppMode mode);

class FameShares
{
public:
    enum Base58Type {
        PUBKEY_ADDRESS,
        SCRIPT_ADDRESS,
        SECRET_KEY,
        EXT_PUBLIC_KEY,
        EXT_SECRET_KEY,

        MAX_BASE58_TYPES
    };

    FameShares();
    void init(AppMode mode = mode_main);
    const std::vector<unsigned char>& Base58Prefix(Base58Type type) const { return base58Prefixes[type]; }

    //Server server();
    //Server rpcServer();

    //Preferences
    unsigned short default_port(AppMode _mode);
    unsigned short default_RPCPort(AppMode _mode);
    unsigned short port();
    unsigned short RPCPort();
    bool DefaultConsistencyChecks();

    //leveldb::DB *chainstate(std::string identifier);
    std::string action_path(std::string identifier);
    std::string path(std::string custom);
    std::string debug_path();
    std::string pid_path();
    std::string config_path();
    std::string peers_path();
    std::string action_state_path(std::string identifier);
    std::string action_index_path(std::string identifier);
    
    UniValue read_json(std::string path);
    void import_data(std::string identifier, UniValue val);
    void init_actions();

    const std::vector<unsigned char>& AlertKey() const { return vAlertPubKey; }
    const CMessageHeader::MessageStartChars& MessageStart() const { return pchMessageStart; }
    const std::vector<CDNSSeedData>& DNSSeeds() const { return vSeeds; };
    const std::vector<SeedSpec6>& FixedSeeds() const { return vFixedSeeds; }
    uint64_t PruneAfterHeight() const { return nPruneAfterHeight; }
    bool MineBlocksOnDemand() const { return fMineBlocksOnDemand; }
    bool MiningRequiresPeers() const { return fMiningRequiresPeers; }
    bool DefaultConsistencyChecks() const { return fDefaultConsistencyChecks; }
    bool RequireStandard() const { return fRequireStandard; }
    
    FAction *action(std::string identifier);
    CChain *chain(std::string identifier);
    CCoinsViewCache *coins(std::string identifier);
    BlockMap blocks(std::string identifier);
    FChainParams *params(std::string identifier);
    CBlockTreeDB *db(std::string identifier);

    std::string mode_string(bool empty_main = false);
    AppMode mode();

    FAction *add_action(FChainParams *params);
    void remove_action(FAction *action);
    ActionsMap get_actions();

private:
    std::string _root_path, _debug_path, _pid_path, _config_path, _peers_path;
    AppMode a_mode;


protected:
    std::vector<unsigned char> base58Prefixes[MAX_BASE58_TYPES];
    CMessageHeader::MessageStartChars pchMessageStart;
    std::vector<CDNSSeedData> vSeeds;
    std::vector<SeedSpec6> vFixedSeeds;
    uint64_t nPruneAfterHeight;
    ActionsMap actions;
    std::vector<unsigned char> vAlertPubKey;
    bool fMineBlocksOnDemand, fMiningRequiresPeers;
    bool fDefaultConsistencyChecks, fRequireStandard;
    
    //List of action_Box

    void init_main_mode();
    void init_regtest_mode();
    void init_nettest_mode();
};

struct BlockHasher
{
    size_t operator()(const uint256& hash) const { return hash.GetCheapHash(); }
};

struct CBlockIndexWorkComparator
{
    bool operator()(CBlockIndex *pa, CBlockIndex *pb) const {
        // First sort by most total work, ...
        if (pa->nChainWork > pb->nChainWork) return false;
        if (pa->nChainWork < pb->nChainWork) return true;
        
        // ... then by earliest time received, ...
        if (pa->nSequenceId < pb->nSequenceId) return false;
        if (pa->nSequenceId > pb->nSequenceId) return true;
        
        // Use pointer address as tie breaker (should only happen with blocks
        // loaded from disk, as those all have id 0).
        if (pa < pb) return false;
        if (pa > pb) return true;
        
        // Identical blocks.
        return false;
    }
};


struct FAction
{
    bool disableMining;
    
    CCoinsViewCache *coins;
    CChain *chain;
    BlockMap blocks;
    FChainParams *params;
    CBlockTreeDB *db;
    
    CCoinsViewDB *coinsdbview;
    CCoinsViewErrorCatcher *coinscatcher;
    CTxMemPool mempool;

    CBlockIndex *bestHeader;
    CCriticalSection cs_LastBlockFile;
    std::vector<CBlockFileInfo> vinfoBlockFile;
    int nLastBlockFile;
    bool fCheckForPruning;
    bool fReindex;
    bool fImporting;
    bool fHavePruned;
    bool fTxIndex;
    bool genesisSearching;
    
    std::string callback;
    
    std::set<CBlockIndex*> setDirtyBlockIndex;
    std::set<int> setDirtyFileInfo;
    std::multimap<CBlockIndex*, CBlockIndex*> mapBlocksUnlinked;
    std::map<uint256, NodeId> mapBlockSource;
    std::set<CBlockIndex*, CBlockIndexWorkComparator> setBlockIndexCandidates;
    
    CBlockIndex *pindexBestHeader;
    CBlockIndex *pindexBestInvalid;
    
    CBlockIndex *pindexBestForkTip;
    CBlockIndex *pindexBestForkBase;
    
    /** Blocks loaded from disk are assigned id 0, so start the counter at 1. */
    uint32_t nBlockSequenceId;
    
    
    CWaitableCriticalSection csBestBlock;
    uint64_t nLastBlockTx;
    uint64_t nLastBlockSize;
    
    boost::thread_group* minerThreads = NULL;
    
    /**
     * Filter for transactions that were recently rejected by
     * AcceptToMemoryPool. These are not rerequested until the chain tip
     * changes, at which point the entire filter is reset. Protected by
     * cs_main.
     *
     * Without this filter we'd be re-requesting txs from each of our peers,
     * increasing bandwidth consumption considerably. For instance, with 100
     * peers, half of which relay a tx we don't accept, that might be a 50x
     * bandwidth increase. A flooding attacker attempting to roll-over the
     * filter using minimum-sized, 60byte, transactions might manage to send
     * 1000/sec if we have fast peers, so we pick 120,000 to give our peers a
     * two minute window to send invs to us.
     *
     * Decreasing the false positive rate is fairly cheap, so we pick one in a
     * million to make it highly unlikely for users to have issues with this
     * filter.
     *
     * Memory used: 1.3 MB
     */
    boost::scoped_ptr<CRollingBloomFilter> recentRejects;
    uint256 hashRecentRejectsChainTip;
    
    std::map<uint256, COrphanTx> mapOrphanTransactions;
    std::map<uint256, std::set<uint256> > mapOrphanTransactionsByPrev;

    FAction(std::string action_identifier);
    ~FAction();
    
    std::string action_identifier();
    void init_blocks();
    bool load_blocks(int64_t nBlockTreeDBCache, int64_t nCoinDBCache);
    void shutdown();
    
    CBlock load_lastblock();
    
    bool MinerWait()
    {
        return chain == 0 || thread_wait;
    }
    
    bool thread_wait;
    
};

#endif // FAMESHARES_H
