#ifndef DEFINES_H
#define DEFINES_H

/*if ac_fn_cxx_try_compile "$LINENO"; then :
   fdelt_type="long unsigned int"
else
   fdelt_type="long int"
fi*/


#define FDELT_TYPE long int
//#define __NFDBITS 8

#ifndef __NFDBITS
#define __NFDBITS	(8 * sizeof (FDELT_TYPE))
#endif

#define ACTION_IDENTIFIER_LENGTH 7
#define DEFAULT_ACTION_IDENTIFIER "JEN-001" //Version

/* Major version */
#define CLIENT_VERSION_MAJOR 0

/* Minor version */
#define CLIENT_VERSION_MINOR 13

/* Build revision */
#define CLIENT_VERSION_REVISION 1

/* Copyright holder(s) before %s replacement */
#define COPYRIGHT_HOLDERS "The %s developers"

/* Copyright holder(s) */
#define COPYRIGHT_HOLDERS_FINAL "The FameShares Core developers"

/* Replacement for %s in copyright holders string */
#define COPYRIGHT_HOLDERS_SUBSTITUTION "FameShares Core"

/* Define to the full name of this package. */
#define PACKAGE_NAME "FameShares Core"

/* Define to the full name and version of this package. */
#define PACKAGE_STRING "FameShares Core 0.0.1"

/* Define to the one symbol short name of this package. */
#define PACKAGE_TARNAME "fameshares"

/* Define to the home page for this package. */
#define PACKAGE_URL "https://www.famebroker.com/"

/* Define to the version of this package. */

#endif // DEFINES_H
