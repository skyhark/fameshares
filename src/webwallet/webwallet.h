//
//  webwallet.h
//  fameshares
//
//  Created by Sacha Vandamme on 20/06/16.
//  Copyright © 2016 Sacha Vandamme. All rights reserved.
//

#ifndef webwallet_h
#define webwallet_h

#include "../fameshares.h"
#include "alert.h"

#include <string>
#include <boost/unordered_map.hpp>
#include <univalue/univalue.h>

struct PKeyTransaction
{
    uint256 hash;
    CAmount amount;
    unsigned int n = -1;
    unsigned int confirmations = 0;
    bool is_vout;
    CBlockHeader block_header;
    CAmount wallet_balance;
    
    //Only for outputs
    bool spended = false;
    
    explicit operator UniValue() const
    {
        UniValue result(UniValue::VOBJ);
        
        result.pushKV("action_identifier", block_header.action_identifier);
        result.pushKV("block_hash", block_header.GetHash().GetHex());
        result.pushKV("block_time", block_header.GetBlockTime());
        result.pushKV("txid", hash.GetHex());
        result.pushKV(is_vout ? "vout" : "vin",  (int) n);
        result.pushKV("amount", amount);
        result.pushKV("confirmations", (int)confirmations);
        
        if(is_vout)
        {
            result.pushKV("spendable", !spended);
        }
        
        return result;
    }
};

typedef boost::unordered_map<uint256, PKeyTransaction, BlockHasher> PKeyTransactionMap;

class PKeyActionSummary
{
public:
    PKeyActionSummary(std::string identifier, std::string addr);
    
    std::string GetAddress();
    std::string GetActionIdentifier();
    PKeyTransactionMap GetTransactions();
    PKeyTransactionMap GetUnspent();
    CAmount GetBalance(int min_confirm=0);
    unsigned int LoadedBlocks();
    size_t GetMemoryUsage();

    bool CanBeRemoved();
    
protected:
    void Using(bool load_more = true);
    void ImportBlock(CBlock block, CBlockIndex *index);
    
    std::string strAddress;
    std::string strIdentifier;
    PKeyTransactionMap transactions;
    PKeyTransactionMap unspent;
    unsigned int used_time;
    unsigned int loaded_blocks;
    CAmount cache_balance;
};

typedef boost::unordered_map<std::string, PKeyActionSummary*> PKeyActionSummaryMap;

class PKeySummary
{
public:
    PKeySummary(std::string addr);
    ~PKeySummary();
    
    std::string GetAddress();
    PKeyTransactionMap GetTransactions(std::string action_identifier);
    PKeyTransactionMap GetUnspent(std::string action_identifier);
    CAmount GetBalance(std::string action_identifier, int min_confirm=0);
    PKeyActionSummaryMap Actions();
    size_t GetMemoryUsage();
    
    bool CanBeRemoved();

protected:
    void Using(std::string action_identifier);
    
    PKeyActionSummaryMap actions;
    std::string strAddress;
    unsigned int used_time;
};


typedef boost::unordered_map<std::string, PKeySummary*> PKeySummaryMap;

class WebWallet
{
public:
    WebWallet();
    
    UniValue GetTransactions(std::string strAddr, std::string action_identifier, int start=0, int limit=10, int min_conf=0);
    UniValue GetUnspent(std::string strAddr, std::string action_identifier, int start=0, int limit=10, int min_conf=0);
    CAmount GetBalance(std::string strAddr, std::string action_identifier, int min_conf=0);
    
    PKeySummary *Summary(std::string strAddr);
    PKeySummaryMap Keys();
    size_t GetMemoryUsage();

protected:
    void AutoMemoryThread();
    PKeySummaryMap KeysCache;
    
};

        

#endif /* webwallet_h */
