//
//  webwallet.cpp
//  fameshares
//
//  Created by Sacha Vandamme on 20/06/16.
//  Copyright © 2016 Sacha Vandamme. All rights reserved.
//

#include "webwallet.h"
#include "base58.h"
#include "core_io.h"
#include "../rpc/protocol.h"

#define webwallet_memory_time 15 //Minutes

WebWallet::WebWallet()
{
    boost::thread(boost::bind(&WebWallet::AutoMemoryThread, this));
}

PKeySummary *WebWallet::Summary(std::string strAddr)
{
    if(!KeysCache.count(strAddr))
    {
        KeysCache[strAddr] = new PKeySummary(strAddr);
    }

    return KeysCache[strAddr];
}

UniValue WebWallet::GetTransactions(std::string strAddr, std::string action_identifier, int start, int limit, int min_confirm)
{
    PKeyTransactionMap map = Summary(strAddr)->GetTransactions(action_identifier);
    
    UniValue result(UniValue::VARR);
    int i = 0, x = 0;
    BOOST_FOREACH(const PKeyTransactionMap::value_type &v, map)
    {
        if(v.second.confirmations >= min_confirm)
        {
            if(i >= start)
            {
                result.push_back((UniValue) v.second);
                
                x++;
                if(x >= limit)
                {
                    break;
                }
            }
            
            i++;
        }
    }
    
    return result;
}

UniValue WebWallet::GetUnspent(std::string strAddr, std::string action_identifier, int start, int limit, int min_confirm)
{
    PKeyTransactionMap map = Summary(strAddr)->GetUnspent(action_identifier);
    
    UniValue result(UniValue::VARR);
    int i = 0, x = 0;
    BOOST_FOREACH(const PKeyTransactionMap::value_type &v, map)
    {
        if(i >= start)
        {
            if(v.second.confirmations >= min_confirm && v.second.spended == false)
            {
                result.push_back((UniValue) v.second);
                    
                x++;
                if(x >= limit)
                {
                    break;
                }
            }
        }
    }
    
    return result;
}

CAmount WebWallet::GetBalance(std::string strAddr, std::string action_identifier, int min_confirm)
{
    return Summary(strAddr)->GetBalance(action_identifier, min_confirm);
}

PKeySummaryMap WebWallet::Keys()
{
    return KeysCache;
}

size_t WebWallet::GetMemoryUsage()
{
    size_t size = sizeof(WebWallet);
    BOOST_FOREACH(const PKeySummaryMap::value_type &v, KeysCache)
    {
        size += v.second->GetMemoryUsage() + sizeof(v.first);
    }
    
    return size;
}

void WebWallet::AutoMemoryThread()
{
    boost::posix_time::time_duration interval(boost::posix_time::seconds(15));
    while(1)
    {
        boost::this_thread::sleep(interval);
        
        BOOST_FOREACH(const PKeySummaryMap::value_type &v, KeysCache)
        {
            if(v.second->CanBeRemoved())
            {
                KeysCache.erase(v.first);
            }
        }
    }
}

//-----------------------------------------------------------------

PKeyActionSummary::PKeyActionSummary(std::string identifier, std::string addr)
{
    strAddress = addr;
    strIdentifier = identifier;
    loaded_blocks = 0;
    cache_balance = 0;
    Using();
}

unsigned int PKeyActionSummary::LoadedBlocks()
{
    return loaded_blocks;
}

std::string PKeyActionSummary::GetAddress()
{
    Using(false);
    return strAddress;
}

std::string PKeyActionSummary::GetActionIdentifier()
{
    return strIdentifier;
}

PKeyTransactionMap PKeyActionSummary::GetTransactions()
{
    Using();
    return transactions;
}

PKeyTransactionMap PKeyActionSummary::GetUnspent()
{
    Using();
    return unspent;
}

CAmount PKeyActionSummary::GetBalance(int min_confirm)
{
    Using();
    
    if(min_confirm <= 0)
    {
        return cache_balance;
    }
    
    CAmount balance = 0;
    BOOST_FOREACH(const PKeyTransactionMap::value_type &v, unspent)
    {
        if(v.second.confirmations >= min_confirm)
        {
            balance += v.second.amount;
        }
    }

    return balance;
}

bool PKeyActionSummary::CanBeRemoved()
{
    return (GetTime() - used_time) > (15000 * 60);
}

void PKeyActionSummary::Using(bool load_more)
{
    used_time = GetTime();
    
    if(!load_more)
        return;
    
    FAction *action = getAction(strIdentifier);
    CChain chain = *action->chain;
    if(chain.Height() > loaded_blocks+1)
    {
        //Update unspents confirmations
        unsigned int add_conf = chain.Height() + 1 - loaded_blocks;
        BOOST_FOREACH(PKeyTransactionMap::value_type &v, unspent)
        {
            v.second.confirmations += add_conf;
        }
        
        BOOST_FOREACH(PKeyTransactionMap::value_type &v, transactions)
        {
            v.second.confirmations += add_conf;
        }
        
        //Import new block
        Consensus::Params consensus = action->params->GetConsensus();
        CBlock block;
        block.action_identifier = strIdentifier;
        
        int height = chain.Height();
        for(int i=loaded_blocks; i <= height; i++)
        {
            CBlockIndex* pblockindex = chain[i];
            if(ReadBlockFromDisk(block, pblockindex, consensus))
            {
                loaded_blocks = i;
                ImportBlock(block, pblockindex);
            }
        }
        
        if(loaded_blocks > height)
        {
            loaded_blocks = height+1;
        }
    }
    
    //Remove mempool transactions from unspended list
    BOOST_FOREACH(const CTxMemPoolEntry& e, action->mempool.mapTx)
    {
        CTransaction tx = e.GetTx();
        BOOST_FOREACH(const CTxIn &txin, tx.vin)
        {
            CHashWriter writer(SER_GETHASH, 0);
            writer << txin.prevout.hash << txin.prevout.n;
            uint256 xhash       = writer.GetHash();
            
            if(unspent.count(xhash))
            {
                cache_balance -= unspent[xhash].amount;
                unspent.erase(xhash);
            }
        }
    }
}

size_t PKeyActionSummary::GetMemoryUsage()
{
    return sizeof(PKeyActionSummary) + (sizeof(PKeyTransaction) * transactions.size())
                                     + (sizeof(std::string) * transactions.size())
                                     + (sizeof(PKeyTransaction) * unspent.size())
                                     + (sizeof(std::string) * unspent.size());
}

void PKeyActionSummary::ImportBlock(CBlock block, CBlockIndex *index)
{
    int chain_height = getAction(block.action_identifier)->chain->Height();
    
    BOOST_FOREACH(const CTransaction &tx, block.vtx)
    {
        unsigned int n = 0;
        BOOST_FOREACH(const CTxIn &txin, tx.vin)
        {
            if(txin.prevout.hash.ToString() == "0000000000000000000000000000000000000000000000000000000000000000")
            {
                continue;
            }
            
            CHashWriter writer(SER_GETHASH, 0);
            writer << txin.prevout.hash << txin.prevout.n;
            uint256 xhash       = writer.GetHash();

            if(unspent.count(xhash))
            {
                PKeyTransaction transaction;
                cache_balance -= unspent[xhash].amount;
                
                transaction.block_header = block.GetBlockHeader();
                transaction.hash    = tx.GetHash();
                transaction.is_vout = false;
                transaction.n       = n;
                transaction.amount  = -unspent[xhash].amount;
                transaction.wallet_balance = cache_balance;
                transaction.confirmations  = chain_height - index->nHeight;
                
                unspent.erase(xhash);
                writer << 'I';
                transactions[writer.GetHash()] = transaction;
            }
            n++;
        }

        n = 0;
        BOOST_FOREACH(const CTxOut &txout, tx.vout)
        {
            txnouttype type;
            std::vector<CTxDestination> addresses;
            int nRequired;
            
            if (!ExtractDestinations(txout.scriptPubKey, type, addresses, nRequired))
            {
                continue;
            }
            
            BOOST_FOREACH(const CTxDestination& addr, addresses)
            {
#warning verify transaction amount is correct with multiple addresses
                if(CBitcoinAddress(addr).ToString() == strAddress)
                {
                    cache_balance += txout.nValue;
                    PKeyTransaction transaction;
                    
                    transaction.block_header = block.GetBlockHeader();
                    transaction.hash    = tx.GetHash();
                    transaction.is_vout = true;
                    transaction.n       = n;
                    transaction.amount  = txout.nValue;
                    transaction.wallet_balance = cache_balance;
                    transaction.confirmations  = chain_height - index->nHeight;
                    
                    //Add to transactions & unspents
                    CHashWriter writer(SER_GETHASH, 0);
                    writer << transaction.hash << transaction.n;
                    uint256 xhash       = writer.GetHash();
                    
                    //std::cout << "Transaction out n: " << transaction.n << ", " << transaction.hash.ToString() << "\n" << xhash.ToString() << "\n---------------------------------\n";
                    
                    
                    transactions[xhash] = transaction;
                    unspent[xhash]      = transaction;
                }
            }
            
            n++;
        }
    }
}

//-----------------------------------------------------------------

PKeySummary::PKeySummary(std::string addr)
{
    strAddress = addr;
    used_time = GetTime();
}

PKeySummary::~PKeySummary()
{
    BOOST_FOREACH(const PKeyActionSummaryMap::value_type &v, actions)
    {
        delete v.second;
        actions[v.first] = NULL;
    }
    
    actions.clear();
}

std::string PKeySummary::GetAddress()
{
    return strAddress;
}

PKeyTransactionMap PKeySummary::GetTransactions(std::string action_identifier)
{
    Using(action_identifier);
    return actions[action_identifier]->GetTransactions();
}

PKeyTransactionMap PKeySummary::GetUnspent(std::string action_identifier)
{
    Using(action_identifier);
    return actions[action_identifier]->GetUnspent();
}

CAmount PKeySummary::GetBalance(std::string action_identifier, int min_confirm)
{
    Using(action_identifier);
    return actions[action_identifier]->GetBalance(min_confirm);
}

PKeyActionSummaryMap PKeySummary::Actions()
{
    return actions;
}

size_t PKeySummary::GetMemoryUsage()
{
    size_t size = sizeof(PKeySummary);
    BOOST_FOREACH(const PKeyActionSummaryMap::value_type &v, actions)
    {
        size += v.second->GetMemoryUsage() + sizeof(v.first);
    }
    
    return size;
}

bool PKeySummary::CanBeRemoved()
{
    return (GetTime() - used_time) > (60 * webwallet_memory_time);
}

void PKeySummary::Using(std::string action_identifier)
{
    used_time = GetTime();
    
    if(!actions.count(action_identifier))
    {
        actions[action_identifier] = new PKeyActionSummary(action_identifier, strAddress);
    }
}
