//
//  rpcwebwallet.h
//  fameshares
//
//  Created by Sacha Vandamme on 14/04/16.
//  Copyright © 2016 Sacha Vandamme. All rights reserved.
//

#ifndef rpcwebwallet_h
#define rpcwebwallet_h

#include "../rpcgenesis.h"

void webwalletRegisterRPCCommands();

#endif /* rpcwebwallet_h */
