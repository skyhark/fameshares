//
//  rpcwebwallet.cpp
//  fameshares
//
//  Created by Sacha Vandamme on 14/04/16.
//  Copyright © 2016 Sacha Vandamme. All rights reserved.
//

#include "rpcwebwallet.h"
#include "../wallet/rpcwallet.h"
#include "amount.h"
#include "base58.h"
#include "chain.h"
#include "core_io.h"
#include "init.h"
#include "utils.h"
#include "net.h"
#include "netbase.h"
#include "../policy/rbf.h"
#include "../rpc/server.h"
#include "timedata.h"
#include "util.h"
#include "utilmoneystr.h"
#include "../wallet/wallet.h"
#include "../wallet/walletdb.h"

#include <stdint.h>
#include <boost/assign/list_of.hpp>
#include <univalue/univalue.h>

#include "./webwallet.h"

static WebWallet uni_wallet;

bool EnsureWalletIsAvailable(bool avoidException);
void AcentryToJSON(const CAccountingEntry& acentry, const string& strAccount, UniValue& ret);
void ImportAddress(const CBitcoinAddress& address, const string& strLabel);
void ImportScript(const CScript& script, const string& strLabel, bool isRedeemScript);



bool AddPublicKey(std::string strPubKey, bool fRescan = true)
{
    // Whether to perform rescan after import
    
    CBitcoinAddress address(strPubKey);
    if (address.IsValid())
    {
        ImportAddress(address, strPubKey);
    }
    else if (IsHex(strPubKey))
    {
        std::vector<unsigned char> data(ParseHex(strPubKey));
        ImportScript(CScript(data.begin(), data.end()), strPubKey, false);
    }
    else {
        throw JSONRPCError(RPC_INVALID_ADDRESS_OR_KEY, "Invalid Bitcoin address or script");
    }
    
    if (fRescan)
    {
        BOOST_FOREACH(const ActionsMap::value_type& v, shares()->get_actions() )
        {
            pwalletMain->ScanForWalletTransactions(v.second->chain->Genesis(), true);
        }
        pwalletMain->ReacceptWalletTransactions();
    }
    
    return true;
}

UniValue publickey_balance(const UniValue& params, bool fHelp)
{
    if (fHelp || params.size() != 2)
        throw runtime_error(
                            "publickey_balance ( \"identifier\" \"address\")\n"
                            "\n....\n"
                            "\nArguments:\n"
                            "1. action identifier   (string, required) The action identifier\n"
                            "2. \"address\"    (string, required) The public key that will be watched.\n"
                            );
    
    
    return uni_wallet.GetBalance(params[1].get_str(), params[0].get_str());
}

UniValue publickey_balances(const UniValue& params, bool fHelp)
{
    if (fHelp || params.size() < 1 || params.size() > 2)
        throw runtime_error(
                            "publickey_balances ( \"address\")\n"
                            "\n....\n"
                            "\nArguments:\n"
                            "1. \"address\"    (string, required) The public key that will be watched.\n"
                            "2. \"min_confirmes\"    (numeric, optional, default=0) The public key that will be watched.\n"
                            );
    
    int min_confirm = 0;
    
    if(params.size() > 1)
        min_confirm = params[1].get_int();
    
    UniValue result(UniValue::VOBJ);
    BOOST_FOREACH(const ActionsMap::value_type &v, shares()->get_actions())
    {
        CAmount balance = uni_wallet.GetBalance(params[0].get_str(), v.first, min_confirm);
        result.pushKV(v.first, balance);
    }
    
    return result;
}

UniValue publickey_transactions(const UniValue& params, bool fHelp)
{
    if (fHelp || params.size() < 2 || params.size() > 5)
        throw runtime_error(
                            "publickey_transactions ( \"action_identifier\" \"address\" count from)\n"
                            "\n....\n"
                            "\nArguments:\n"
                            "1. action identifier   (string, required) The action identifier\n"
                            "2. \"address\"    (string, required) The public key that will be watched.\n"
                            "3. count          (numeric, optional, default=10) The number of transactions to return\n"
                            "4. from           (numeric, optional, default=0) The number of transactions to skip\n"
                            "5. min_conf           (numeric, optional, default=0) The minimum number of confirmations\n"
                            );
    
    int count = (params.size() > 2 ? params[2].get_int() : 10);
    int start = (params.size() > 3 ? params[3].get_int() : 0);
    int min_confirm = (params.size() > 4 ? params[4].get_int() : 0);
    
    std::string identifier = params[0].get_str();
    std::string address    = params[1].get_str();
    
    return uni_wallet.GetTransactions(address, identifier, start, count, min_confirm);
}

UniValue publickey_unspents(const UniValue& params, bool fHelp)
{
    if (fHelp || params.size() < 2 || params.size() > 5)
        throw runtime_error(
                            "publickey_transactions ( \"action_identifier\" \"address\" count from)\n"
                            "\n....\n"
                            "\nArguments:\n"
                            "1. action identifier   (string, required) The action identifier\n"
                            "2. \"address\"    (string, required) The public key that will be watched.\n"
                            "3. count          (numeric, optional, default=10) The number of transactions to return\n"
                            "4. from           (numeric, optional, default=0) The number of transactions to skip\n"
                            "5. min_conf           (numeric, optional, default=0) The minimum number of confirmations\n"
                            );
    
    int count = (params.size() > 2 ? params[2].get_int() : 10);
    int start = (params.size() > 3 ? params[3].get_int() : 0);
    int min_confirm = (params.size() > 4 ? params[4].get_int() : 0);
    
    std::string identifier = params[0].get_str();
    std::string address    = params[1].get_str();
    
    return uni_wallet.GetUnspent(address, identifier, start, count, min_confirm);
}

UniValue publickeys_cache(const UniValue& params, bool fHelp)
{
    if (fHelp || params.size() != 0)
        throw runtime_error(
                            "publickeys_cache\n"
                            "\n....\n"
                            );
    
    UniValue result(UniValue::VOBJ);
    result.pushKV("KeysCount", (int) uni_wallet.Keys().size());
    result.pushKV("CacheSize", (int) uni_wallet.GetMemoryUsage());
    
    UniValue keys(UniValue::VOBJ);
    BOOST_FOREACH(const PKeySummaryMap::value_type &v, uni_wallet.Keys())
    {
        UniValue obj(UniValue::VOBJ);
        
        BOOST_FOREACH(const PKeyActionSummaryMap::value_type &v2, v.second->Actions())
        {
            obj.pushKV(v2.first, (int)v2.second->LoadedBlocks());
        }

        keys.pushKV(v.first, obj);
    }
    
    result.pushKV("KeysHeight", keys);
    return result;
}

UniValue savelisttransactions(const UniValue& params, bool fHelp)
{
    if (!EnsureWalletIsAvailable(fHelp))
        return NullUniValue;
    
    if (fHelp || params.size() > 4 || params.size() < 2)
        throw runtime_error(
                            "saveliststransactions ( \"identifier\" \"account\" count from includeWatchonly)\n"
                            "\nReturns up to 'count' most recent transactions skipping the first 'from' transactions for account 'account'.\n"
                            "\nArguments:\n"
                            "1. action identifier   (string, required) The action identifier\n"
                            "2. \"address\"    (string, required) The public key that will be watched.\n"
                            "3. count          (numeric, optional, default=10) The number of transactions to return\n"
                            "4. from           (numeric, optional, default=0) The number of transactions to skip\n"
                            "\nResult:\n"
                            "[\n"
                            "  {\n"
                            "    \"account\":\"accountname\",       (string) DEPRECATED. The account name associated with the transaction. \n"
                            "                                                It will be \"\" for the default account.\n"
                            "    \"address\":\"bitcoinaddress\",    (string) The bitcoin address of the transaction. Not present for \n"
                            "                                                move transactions (category = move).\n"
                            "    \"category\":\"send|receive|move\", (string) The transaction category. 'move' is a local (off blockchain)\n"
                            "                                                transaction between accounts, and not associated with an address,\n"
                            "                                                transaction id or block. 'send' and 'receive' transactions are \n"
                            "                                                associated with an address, transaction id and block details\n"
                            "    \"amount\": x.xxx,          (numeric) The amount in " + CURRENCY_UNIT + ". This is negative for the 'send' category, and for the\n"
                            "                                         'move' category for moves outbound. It is positive for the 'receive' category,\n"
                            "                                         and for the 'move' category for inbound funds.\n"
                            "    \"vout\": n,                (numeric) the vout value\n"
                            "    \"fee\": x.xxx,             (numeric) The amount of the fee in " + CURRENCY_UNIT + ". This is negative and only available for the \n"
                            "                                         'send' category of transactions.\n"
                            "    \"confirmations\": n,       (numeric) The number of confirmations for the transaction. Available for 'send' and \n"
                            "                                         'receive' category of transactions. Negative confirmations indicate the\n"
                            "                                         transaction conflicts with the block chain\n"
                            "    \"trusted\": xxx            (bool) Whether we consider the outputs of this unconfirmed transaction safe to spend.\n"
                            "    \"blockhash\": \"hashvalue\", (string) The block hash containing the transaction. Available for 'send' and 'receive'\n"
                            "                                          category of transactions.\n"
                            "    \"blockindex\": n,          (numeric) The block index containing the transaction. Available for 'send' and 'receive'\n"
                            "                                          category of transactions.\n"
                            "    \"blocktime\": xxx,         (numeric) The block time in seconds since epoch (1 Jan 1970 GMT).\n"
                            "    \"txid\": \"transactionid\", (string) The transaction id. Available for 'send' and 'receive' category of transactions.\n"
                            "    \"time\": xxx,              (numeric) The transaction time in seconds since epoch (midnight Jan 1 1970 GMT).\n"
                            "    \"timereceived\": xxx,      (numeric) The time received in seconds since epoch (midnight Jan 1 1970 GMT). Available \n"
                            "                                          for 'send' and 'receive' category of transactions.\n"
                            "    \"comment\": \"...\",       (string) If a comment is associated with the transaction.\n"
                            "    \"label\": \"label\"        (string) A comment for the address/transaction, if any\n"
                            "    \"otheraccount\": \"accountname\",  (string) For the 'move' category of transactions, the account the funds came \n"
                            "                                          from (for receiving funds, positive amounts), or went to (for sending funds,\n"
                            "                                          negative amounts).\n"
                            "    \"bip125-replaceable\": \"yes|no|unknown\"  (string) Whether this transaction could be replaced due to BIP125 (replace-by-fee);\n"
                            "                                                     may be unknown for unconfirmed transactions not in the mempool\n"
                            "  }\n"
                            "]\n"
                            
                            "\nExamples:\n"
                            "\nList the most recent 10 transactions in the systems\n"
                            + HelpExampleCli("savelisttransactions", "SHK-001") +
                            "\nList transactions 100 to 120\n"
                            + HelpExampleCli("savelisttransactions", "SHK-001 \"*\" 20 100") +
                            "\nAs a json rpc call\n"
                            + HelpExampleRpc("savelisttransactions", "SHK-001 \"*\", 20, 100")
                            );
    
    LOCK2(cs_main, pwalletMain->cs_wallet);
    
    std::string identifier = params[0].get_str();
    std::string strPubKey  = params[1].get_str();
    
    AddPublicKey(strPubKey);
    
    //--------------------------------------
    
    int nCount = 10;
    if (params.size() > 2)
        nCount = params[2].get_int();
    int nFrom = 0;
    if (params.size() > 3)
        nFrom = params[3].get_int();
    
    isminefilter filter = ISMINE_SPENDABLE | ISMINE_WATCH_ONLY;
    
    if (nCount < 0)
        throw JSONRPCError(RPC_INVALID_PARAMETER, "Negative count");
    if (nFrom < 0)
        throw JSONRPCError(RPC_INVALID_PARAMETER, "Negative from");
    
    UniValue ret(UniValue::VARR);
    
    const CWallet::TxItems & txOrdered = pwalletMain->wtxOrdered;
    
    // iterate backwards until we have nCount items to return:
    for (CWallet::TxItems::const_reverse_iterator it = txOrdered.rbegin(); it != txOrdered.rend(); ++it)
    {
        CWalletTx *const pwtx = (*it).second.first;
        if (pwtx != 0)
        {
            if(pwtx->action_identifier == identifier || identifier == "*")
                ListTransactions(*pwtx, strPubKey, 0, true, ret, filter);
        }
        
        CAccountingEntry *const pacentry = (*it).second.second;
        if (pacentry != 0)
            AcentryToJSON(*pacentry, strPubKey, ret);
        
        if ((int)ret.size() >= (nCount+nFrom)) break;
    }
    // ret is newest to oldest
    
    if (nFrom > (int)ret.size())
        nFrom = ret.size();
    if ((nFrom + nCount) > (int)ret.size())
        nCount = ret.size() - nFrom;
    
    vector<UniValue> arrTmp = ret.getValues();
    
    vector<UniValue>::iterator first = arrTmp.begin();
    std::advance(first, nFrom);
    vector<UniValue>::iterator last = arrTmp.begin();
    std::advance(last, nFrom+nCount);
    
    if (last != arrTmp.end()) arrTmp.erase(last, arrTmp.end());
    if (first != arrTmp.begin()) arrTmp.erase(arrTmp.begin(), first);
    
    std::reverse(arrTmp.begin(), arrTmp.end()); // Return oldest to newest
    
    ret.clear();
    ret.setArray();
    ret.push_backV(arrTmp);
    
    return ret;
}

UniValue getlastblocks(const UniValue& params, bool fHelp)
{
    if (fHelp || params.size() > 1)
        throw runtime_error(
                            "getlastblocks\n"
                            "\nArguments:\n"
                            "1. count   (numeric, optional, default=5) The number of requestesd blocks\n"
                            );
    
    unsigned int count = 5;
    if(params.size() == 1)
        count = params[0].get_int();
    
    UniValue result(UniValue::VARR);
    
    BOOST_FOREACH(const ActionsMap::value_type& v, shares()->get_actions() )
    {
        CBlock block = v.second->load_lastblock();
        
        UniValue val(UniValue::VOBJ);
        val.pushKV("action_identifier", v.first);
        val.pushKV("hash", block.GetHash().ToString());
        val.pushKV("time", (int) block.nTime);
        val.pushKV("transactions", (int) block.vtx.size());
        val.pushKV("height", (int) v.second->chain->Height());
        
        result.push_back(val);
    }
    
    return result;
}

void TxToJSON(const CTransaction& tx, const uint256 hashBlock, UniValue& entry);

UniValue getblocktransactions(const UniValue& params, bool fHelp)
{
    if (fHelp || params.size() != 2)
        throw runtime_error(
                            "getblocktransactions\n"
                            "\nArguments:\n"
                            "1. action_identifier   (String, required)\n"
                            "2. block_hash   (String, required)\n"
                            );

    LOCK(cs_main);
    
    std::string identifier = params[0].get_str();
    std::string strHash = params[1].get_str();
    uint256 hash(uint256S(strHash));
    
    BlockMap mapBlockIndex = shares()->blocks(identifier);
    if (mapBlockIndex.count(hash) == 0)
        throw JSONRPCError(RPC_INVALID_ADDRESS_OR_KEY, "Block not found");
    
    CBlock block;
    block.action_identifier = identifier;
    CBlockIndex* pblockindex = mapBlockIndex[hash];
    
    if (getAction(identifier)->fHavePruned && !(pblockindex->nStatus & BLOCK_HAVE_DATA) && pblockindex->nTx > 0)
        throw JSONRPCError(RPC_INTERNAL_ERROR, "Block not available (pruned data)");
    
    if(!ReadBlockFromDisk(block, pblockindex, shares()->params(identifier)->GetConsensus()))
        throw JSONRPCError(RPC_INTERNAL_ERROR, "Can't read block from disk");

    UniValue result(UniValue::VARR);
    
    BOOST_FOREACH(CTransaction tx, block.vtx)
    {
        UniValue objTx(UniValue::VOBJ);
        TxToJSON(tx, hash, objTx);
        result.push_back(objTx);
    }

    return result;
}





const CRPCCommand vWebwalletRPCCommands[] =
{ //  category              name                        actor (function)           okSafeMode
    //  --------------------- ------------------------    -----------------------    ----------
    { "webwallet",    "saveliststransactions",    &savelisttransactions,       true },
    { "webwallet",    "publickey_balance",        &publickey_balance,          false },
    { "webwallet",    "publickey_balances",       &publickey_balances,         false },
    { "webwallet",    "publickey_transactions",   &publickey_transactions,     false },
    { "webwallet",    "publickey_unspents",       &publickey_unspents,         false },
    { "webwallet",    "publickeys_cache",         &publickeys_cache,           true },
    { "webwallet",    "getlastblocks",            &getlastblocks,              true },
    { "webwallet",    "getblocktransactions",     &getblocktransactions,       true },
    { "genesis",      "generate_genesis",         &generate_genesis,           true },
    { "genesis",      "generate_genesis_cpu",         &generate_genesis_cpu,           true },
    { "genesis",      "stop_genesisgenerator_cpu",    &stop_genesisgenerator_cpu,      true },
    { "genesis",      "searching_genesis_cpu",        &searching_genesis_cpu,          true }
};

void webwalletRegisterRPCCommands()
{
    unsigned int vcidx;
    for (vcidx = 0; vcidx < ARRAYLEN(vWebwalletRPCCommands); vcidx++)
    {
        const CRPCCommand *pcmd;
        
        pcmd = &vWebwalletRPCCommands[vcidx];
        tableRPC.appendCommand(pcmd->name, pcmd);
    }
}
