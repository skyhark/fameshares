//
//  rpcgenesis.hpp
//  fameshares
//
//  Created by Sacha Vandamme on 13/10/16.
//  Copyright © 2016 Sacha Vandamme. All rights reserved.
//

#ifndef rpcgenesis_hpp
#define rpcgenesis_hpp

#include <univalue/univalue.h>
#include <boost/unordered_map.hpp>

typedef boost::unordered_map<std::string, bool> searchActionNounce;

struct FAction;
UniValue executeCallback(FAction *action);
UniValue getActionConfig(FAction *action);
UniValue generate_genesis(const UniValue& params, bool fHelp);
UniValue generate_genesis_cpu(const UniValue& params, bool fHelp);
UniValue stop_genesisgenerator_cpu(const UniValue& params, bool fHelp);
UniValue searching_genesis_cpu(const UniValue& params, bool fHelp);

#endif /* rpcgenesis_hpp */
