//
//  rpcgenesis.cpp
//  fameshares
//
//  Created by Sacha Vandamme on 13/10/16.
//  Copyright © 2016 Sacha Vandamme. All rights reserved.
//

#include "rpcgenesis.h"
#include "util.h"

#include <stdexcept>
#include <boost/thread.hpp>
#include <cmath>
#include <curl/curl.h>

#include "fameshares.h"

using namespace std;
typedef boost::unordered_map<std::string, bool> searchActionAllowed;

static searchActionAllowed _searchActionsAllowed;
static boost::thread_group GeneratorThreadsGroup;

struct GenesisGeneratorParams
{
    string ActionIdentifier;
    int GenesisAmount;
    int HalveInterval;
    string CallBack;
    
    int NonceStart;
    int NonceStop;
    
    int StartTime;
    int TimeInterval;
};

void ExecCallback(std::string url, const UniValue& params)
{
    CURLcode res;
    CURL *curl = curl_easy_init();
    if(curl) {
        
        struct curl_slist *headerlist=NULL;
        headerlist = curl_slist_append(headerlist, "Content-Type: application/json");
        headerlist = curl_slist_append(headerlist, "Accept: application/json");
        headerlist = curl_slist_append(headerlist, "charsets: utf-8");
        
        std::string data = params.write();
        
        curl_easy_setopt(curl, CURLOPT_URL, url.c_str());
        curl_easy_setopt(curl, CURLOPT_SSL_VERIFYPEER, 0L);
        curl_easy_setopt(curl, CURLOPT_HEADER, true);
        curl_easy_setopt(curl, CURLOPT_HTTPHEADER, headerlist);
        curl_easy_setopt(curl, CURLOPT_POST, 1L);
        curl_easy_setopt(curl, CURLOPT_POSTFIELDSIZE, data.length());
        curl_easy_setopt(curl, CURLOPT_POSTFIELDS, data.c_str());

        res = curl_easy_perform(curl);

        if(res != CURLE_OK)
            LogPrint("GenesisGenerator", "curl_easy_perform() failed: %s\n", curl_easy_strerror(res));

        curl_easy_cleanup(curl);
    }
    
    curl_global_cleanup();
}


//Thread
void ThreadGenerateGenesis(const GenesisGeneratorParams& params)
{
    //Generate consensus
    Consensus::Params consensus;
    consensus.initialAmount = params.GenesisAmount;
    consensus.nSubsidyHalvingInterval = params.HalveInterval;
    consensus.nMajorityEnforceBlockUpgrade = 750;
    consensus.nMajorityRejectBlockOutdated = 950;
    consensus.nMajorityWindow = 1000;
    consensus.BIP34Height = 227931;
    consensus.BIP34Hash = uint256S("0x000000000000024b89b42a942fe0d9fea3bb44ab7bd1b19115dd6a759c0808b8");
    consensus.powLimit = uint256S("00000000ffffffffffffffffffffffffffffffffffffffffffffffffffffffff");
    consensus.nPowTargetTimespan = 1609600; // two weeks
    consensus.nPowTargetSpacing = 600;
    consensus.fPowAllowMinDifficultyBlocks = false;
    consensus.fPowNoRetargeting = false;
    
    //Generate genesis
    CAmount amount = params.GenesisAmount * COIN;
    CBlock genesis = CreateGenesisBlock(params.ActionIdentifier,
                                        "5000000101000000732030332f4a616e2f32303136204368616e63656c6c6f72206f6e206272696e6b206f66207365636f6e64206261696c6f757420666f722062616e6b73",
                                        params.StartTime,
                                        params.NonceStart,
                                        486604799,
                                        1,
                                        amount);
    
    consensus.hashGenesisBlock = genesis.GetHash();

    //----------------------------------------------------------------------
    
    bool fNegative;
    bool fOverflow;
    arith_uint256 bnTarget;
    bnTarget.SetCompact(genesis.nBits, &fNegative, &fOverflow);

    // Check range
    if (fNegative || bnTarget == 0 || fOverflow || bnTarget > UintToArith256(consensus.powLimit))
    {
        std::cout << "-----------Nonce not found-----------\n";
        std::cout << "BITS OUT OF RANGE ! \n";
        
        if(fNegative)
        {
            std::cout << "Bits value too big (fNegative: " << (fNegative ? "true" : "false") << ")\n";
        }
        else if(bnTarget == 0)
        {
            std::cout << "Bits value too small (bnTarget is 0)\n";
        }
        else if(fOverflow || bnTarget > UintToArith256(consensus.powLimit))
        {
            std::cout << "Bits value too big (fOverflow: " << (fOverflow ? "true" : "false") << ")\n";
        }
        
        return;
    }
    
    arith_uint256 best_hash = UintToArith256(genesis.GetCachedHash());
    arith_uint256 nw = UintToArith256(genesis.GetCachedHash());

    while(_searchActionsAllowed[params.ActionIdentifier])
    {
        while (nw > bnTarget && genesis.nNonce < params.NonceStop && _searchActionsAllowed[params.ActionIdentifier]) {
            ++genesis.nNonce;
            nw = UintToArith256(genesis.GetCachedHash());
            
            if(nw < best_hash)
            {
                best_hash = nw;
            }

            if(genesis.nNonce % 10000 == 0)
                boost::this_thread::sleep(boost::posix_time::milliseconds(5));
        }
        
        if(CheckProofOfWork(genesis.GetHash(), genesis.nBits, consensus))
        {
            _searchActionsAllowed[params.ActionIdentifier] = false;

            UniValue ConsensusVal(UniValue::VOBJ);
            ConsensusVal.pushKV("HalvingInterval", params.HalveInterval);
            ConsensusVal.pushKV("MajorityEnforceBlockUpgrade", consensus.nMajorityEnforceBlockUpgrade);
            ConsensusVal.pushKV("MajorityRejectBlockOutdated", consensus.nMajorityRejectBlockOutdated);
            ConsensusVal.pushKV("MajorityWindow", consensus.nMajorityWindow);
            ConsensusVal.pushKV("BIP34Height", consensus.BIP34Height);
            ConsensusVal.pushKV("BIP34Hash", consensus.BIP34Hash.ToString());
            ConsensusVal.pushKV("powLimit", consensus.powLimit.ToString());
            ConsensusVal.pushKV("PowTargetTimespan", consensus.nPowTargetTimespan);
            ConsensusVal.pushKV("PowTargetSpacing", consensus.nPowTargetSpacing);
            ConsensusVal.pushKV("PowAllowMinDifficultyBlocks", consensus.fPowAllowMinDifficultyBlocks);
            ConsensusVal.pushKV("PowNoRetargeting", consensus.fPowNoRetargeting);
            ConsensusVal.pushKV("hashGenesisBlock", genesis.GetHash().ToString());
            
            UniValue GenesisVal(UniValue::VOBJ);
            GenesisVal.pushKV("Time", (int) genesis.nTime);
            GenesisVal.pushKV("Nonce", (int) genesis.nNonce);
            GenesisVal.pushKV("Bits", (int) genesis.nBits);
            GenesisVal.pushKV("Version", genesis.nVersion);
            GenesisVal.pushKV("Amount", params.GenesisAmount);
            GenesisVal.pushKV("HashMerkleRoot", genesis.hashMerkleRoot.ToString());

            
            UniValue res(UniValue::VOBJ);
            res.pushKV("action_identifier", params.ActionIdentifier);
            res.pushKV("Consensus", ConsensusVal);
            res.pushKV("Genesis", GenesisVal);
            
            ExecCallback(params.CallBack, res);
            break;
        }
        
        genesis.ResetCachedHash();
        genesis.nNonce = params.NonceStart;
        genesis.nTime += params.TimeInterval;
    }
    
    boost::this_thread::interruption_point();
}

UniValue generate_genesis_cpu(const UniValue& params, bool fHelp)
{
    if (fHelp || params.size() != 7)
        throw runtime_error(
                            "generate_genesis ( \"action_identifier\" \"address\" count from)\n"
                            "\n....\n"
                            "\nArguments:\n"
                            "1. action identifier   (string, required) The action identifier\n"
                            "2. genesis amount      (integer, required) The start amount of the genesis block\n"
                            "3. halve_interval      (integer, required) The interval of blocks at which blockchain halving will occur\n"
                            "4. threads             (integer, required) The number of threads to start per miner\n"
                            "5. callback            (string, required) The callback (url) to fire once a nounce has been found\n"
                            "6. StartTime           (integer required) The start time at which each miner will be launched\n"
                            "7. TimeInterval        (integer required) The time offset at which each miner will be launched seperatly\n"
                            );
    
    _searchActionsAllowed[params[0].get_str()]  = true;
    
    int threads = params[3].get_int();
    GenesisGeneratorParams genParams;
    
    genParams.ActionIdentifier = params[0].get_str();
    genParams.GenesisAmount = params[1].get_int();
    genParams.HalveInterval = params[2].get_int();
    genParams.CallBack = params[4].get_str();
    genParams.StartTime = params[5].get_int();
    genParams.TimeInterval = params[6].get_int();
    

    //Launch threads...
    GeneratorThreadsGroup.interrupt_all();
    for(int i=1; i <= threads; i++)
    {
        genParams.NonceStart = (i == 1 ? 0 : (std::floor((INT_MAX / threads)) * (i - 1)));
        genParams.NonceStop = (i == threads ? INT_MAX : (std::floor((INT_MAX / threads)) * i));

        GeneratorThreadsGroup.create_thread(boost::bind(&ThreadGenerateGenesis, genParams));
    }

    UniValue res(UniValue::VOBJ);
    res.pushKV("action_identifier", genParams.ActionIdentifier);
    res.pushKV("threads", threads);
    res.pushKV("notify_url", genParams.CallBack);
    return res;
}

UniValue stop_genesisgenerator_cpu(const UniValue& params, bool fHelp)
{
    if (fHelp || params.size() != 1)
        throw runtime_error(
                            "stop_genesisgenerator ( \"action_identifier\" \"address\" count from)\n"
                            "\n....\n"
                            "\nArguments:\n"
                            "1. action identifier   (string, required) The action identifier\n"
                            );
    
    _searchActionsAllowed[params[0].get_str()] = false;

    UniValue res(UniValue::VOBJ);
    res.pushKV("Threads stopped", params[0].get_str());
    
    return res;
}

UniValue searching_genesis_cpu(const UniValue& params, bool fHelp)
{
    if (fHelp || params.size() > 1)
        throw runtime_error(
                            "searching_genesis ( \"action_identifier\" \"address\" count from)\n"
                            "\n....\n"
                            "\nArguments:\n"
                            "1. action identifier   (string, optional) The action identifier\n"
                            );
    
    
    if(params.size() == 0)
    {
        UniValue res(UniValue::VOBJ);
        
        BOOST_FOREACH(const searchActionAllowed::value_type& srch, _searchActionsAllowed)
        {
            if(srch.second)
            {
                UniValue res(UniValue::VSTR);
                res.setStr(srch.first);
                return res;
            }
        }
    }
    else if(_searchActionsAllowed.count(params[0].get_str()) != 0)
    {
        UniValue res(UniValue::VBOOL);
        res.setBool(_searchActionsAllowed[params[0].get_str()]);
        return res;
    }
    
    UniValue res(UniValue::VBOOL);
    res.setBool(false);
    return res;
}


UniValue getActionConfig(FAction *action)
{
    Consensus::Params consensus = action->params->GetConsensus();
    CBlock genesis = action->params->GenesisBlock();
    UniValue consensusRes(UniValue::VOBJ);
    
    consensusRes.pushKV("HalvingInterval", consensus.nSubsidyHalvingInterval);
    consensusRes.pushKV("MajorityEnforceBlockUpgrade", consensus.nMajorityEnforceBlockUpgrade);
    consensusRes.pushKV("MajorityRejectBlockOutdated", consensus.nMajorityRejectBlockOutdated);
    consensusRes.pushKV("MajorityWindow", consensus.nMajorityWindow);
    consensusRes.pushKV("BIP34Height", consensus.BIP34Height);
    consensusRes.pushKV("BIP34Hash", consensus.BIP34Hash.GetHex());
    consensusRes.pushKV("powLimit", consensus.powLimit.GetHex());
    consensusRes.pushKV("PowTargetTimespan", consensus.nPowTargetTimespan);
    consensusRes.pushKV("PowTargetSpacing", consensus.nPowTargetSpacing);
    consensusRes.pushKV("PowAllowMinDifficultyBlocks", consensus.fPowAllowMinDifficultyBlocks);
    consensusRes.pushKV("PowNoRetargeting", consensus.fPowNoRetargeting);
    consensusRes.pushKV("hashGenesisBlock", genesis.GetHash().GetHex());
    
    //----------------------------------
    UniValue genesisRes(UniValue::VOBJ);
    
    genesisRes.pushKV("Time", (int64_t) genesis.nTime);
    genesisRes.pushKV("Nonce", (int64_t) genesis.nNonce);
    genesisRes.pushKV("Bits", (int64_t) genesis.nBits);
    genesisRes.pushKV("Version", genesis.nVersion);
    genesisRes.pushKV("Amount", consensus.initialAmount);
    genesisRes.pushKV("HashMerkleRoot", genesis.hashMerkleRoot.GetHex());

    std::string strHex = HexStr(genesis.vtx[0].vin[0].scriptSig.begin(), genesis.vtx[0].vin[0].scriptSig.end());
    genesisRes.pushKV("CoinBase", strHex.substr(16) );
    
    //----------------------------------
    
    UniValue result(UniValue::VOBJ);
    result.pushKV("Consensus", consensusRes);
    result.pushKV("Genesis", genesisRes);
    result.pushKV("CheckPoints", UniValue(UniValue::VARR));
    
    //---
    //CDataStream ssBlock(SER_NETWORK, PROTOCOL_VERSION);
    //ssBlock << genesis;
    //std::string strHex = HexStr(ssBlock.begin(), ssBlock.end());
    //result.pushKV("hex", strHex);
    //---
    
    UniValue globalResult(UniValue::VOBJ);
    globalResult.pushKV(action->action_identifier(), result);
    return globalResult;
}


UniValue generate_genesis(const UniValue& params, bool fHelp)
{
    if (fHelp || params.size() != 7)
        throw runtime_error(
                            "generate_genesis ( \"action_identifier\" \"address\" count from)\n"
                            "\n....\n"
                            "\nArguments:\n"
                            "1. action identifier   (string, required) The action identifier\n"
                            "2. genesis amount      (integer, required) The start amount of the genesis block\n"
                            "3. halve_interval      (integer, required) The interval of blocks at which blockchain halving will occur\n"
                            "4. StartTime           (integer required) The start time at which each miner will be launched\n"
                            "5. BlockTime           (integer required) The time required to mine a block in seconds\n"
                            "6. PowTargetTime       (integer required) The time required before reevaluating the difficulty in seconds\n"
                            "7. callback            (string, required) The callback (url) to fire once a nounce has been found\n"
                            );

    std::string callback = params[6].get_str();
    
    //Generate consensus
    Consensus::Params consensus;
    consensus.initialAmount = params[1].get_int();
    consensus.nSubsidyHalvingInterval = params[2].get_int();
    consensus.nMajorityEnforceBlockUpgrade = 750;
    consensus.nMajorityRejectBlockOutdated = 950;
    consensus.nMajorityWindow = 1000;
    consensus.BIP34Height = 227931;
    consensus.BIP34Hash = uint256S("0x000000000000024b89b42a942fe0d9fea3bb44ab7bd1b19115dd6a759c0808b8");
    consensus.powLimit = uint256S("00000000ffffffffffffffffffffffffffffffffffffffffffffffffffffffff");
    consensus.nPowTargetTimespan = params[5].get_int();
    consensus.nPowTargetSpacing = params[4].get_int();
    consensus.fPowAllowMinDifficultyBlocks = false;
    consensus.fPowNoRetargeting = false;
    
    //Generate genesis
    CAmount amount = consensus.initialAmount * COIN;
    CBlock genesis = CreateGenesisBlock(params[0].get_str(), "5000000101000000732030332f4a616e2f32303136204368616e63656c6c6f72206f6e206272696e6b206f66207365636f6e64206261696c6f757420666f722062616e6b73", params[3].get_int(), 0, 486604799, 1, amount);

    consensus.hashGenesisBlock = genesis.GetHash();

    //Generate checkpoints
    FChainParams *fc_params = new FChainParams(genesis, consensus, CCheckpointData());
    FAction *action = shares()->add_action(fc_params);
    action->genesisSearching = true;
    action->callback = callback;

    return getActionConfig(action);
}

UniValue executeCallback(FAction *action)
{
    UniValue config = getActionConfig(action);
    ExecCallback( action->callback, config );

    
    CDataStream ssBlock(SER_NETWORK, PROTOCOL_VERSION);
    ssBlock << action->params->GenesisBlock();
    std::string strHex = HexStr(ssBlock.begin(), ssBlock.end());
    std::cout << strHex << "\n\n";
    
    return config;
}
