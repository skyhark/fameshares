<?php

    function paged_action($name, $cb)
    {
        if(isset($_POST['paged_action']))
        {
            if($name == $_POST['paged_action'])
            {
                $res = $_POST;
                unset($res['paged_action']);
                unset($res['additional']);
                $result = $cb($res);
                
                print json_encode($result);
                exit;
            }
        }
    }

?>