<?php

    function view_path($page)
    {
        $path = dirname(__FILE__).'/../pages/';
        return multi_path_find($path, $page);
    }

    function get_view_name()
    {
        global $request_path_f;

        if(($pos = strpos($request_path_f, '/')))
        {
            $page = substr($request_path_f, 0, $pos);
        }
        else
        {
            $page = $request_path_f;
        }
        if(empty($page))
        {
            $page = "index";
        }

        //Generate page path
        global $view_path;
        $path = dirname(__FILE__).'/../pages/';
        $view_path = multi_path_find($path, $page);
        
        if($view_path != null)
        {
            $path = substr($view_path, 0, -(strlen($page)+4));
            return view_path_security_check($path, $page);
        }
        
        //404 Page not found
        $view_path = multi_path_find($path, "404");
        $path = substr($view_path, 0, -7);
        
        global $view_config;
        $view_config = get_view_config($path);
        
        return "404";
    }

    function include_view($name)
    {
        $path = view_path($name);
        
        if($path == null)
        {
            $path = view_path('404');
            /*$p = substr($path, 0, strrpos('/', $path));
            print $p."<br>";
            $view_config = json_file($p);*/
        }
        
        global $view_config;
        
        if(!isset($view_config))
        {
            $view_config = array();
        }

        if(($menu = getProperty($view_config, 'header_module')))
        {
            module($menu);
        }
        
        include($path);
        
        if(($footer = getProperty($view_config, 'footer_module')))
        {
            module($footer);
        }
    }

    function get_subview_parts()
    {
        global $request_path_f;

        if(($pos = strpos($request_path_f, '/')))
        {
            $subview = substr($request_path_f, $pos+1);
            
            if(empty($subview))
            {
                return array();
            }
        
            return explode('/', $subview);
        }
        
        return array();
    }

    function get_subview($default = false)
    {
        return getProperty( get_subview_parts(), '0', $default );
    }

    function getPathVariables($keys)
    {
        $parts = get_subview_parts();
        
        $i = 0;
        foreach($keys as $name)
        {
            if(isset($parts[$i]))
            {
                $parts[$name] = $parts[$i];
                unset($parts[$i]);
            }
            
            $i++;
        }
        
        return $parts;
    }

    function is_view_only()
    {
        $view = get_view_name();
        if($view == 'banner')
        {
            return true;
        }
        
        if(isset($_POST['additional']))
        {
            if($_POST['additional'] == 'no-menu')
            {
                return true;
            }
        }
        
        return false;
    }

?>