<?php

    function url_path($path, $domain = "default")
    {
        if($path == 'index')
        {
            return platform_url();
        }
        
        
        if($domain == "image")
        {
            $path = "img/".$path;
        }
        
        return platform_url().$path;
    }

    function img_path($path)
    {
        return url_path($path, 'image');
    }

    function image_path($path)
    {
        return img_path($path);
    }

    function lib_path($path)
    {
        return url_path('lib/'.$path);
    }

    function css_path($path)
    {
        return url_path('css/'.$path);
    }

    function lib($path)
    {
        print '<script src="'.lib_path($path).'"></script>';
    }

    function script($path)
    {
        lib($path);
    }

    function css($path, $media = null)
    {
        print '<link rel="stylesheet" href="'.css_path($path).'"';
        
        if($media != null)
        {
            print ' media="'.$media.'"';
        }
        
        print '>';
    }

    function image($path, $class=null)
    {
        print '<img src="'.img_path($path).'"';
        
        if($class != null)
        {
            print ' class="'.$class.'"';
        }
        
        print '>';
    }

    //------------------------------------

    function isSecure() {
        if(!empty($_SERVER['HTTPS']))
        {
            return ($_SERVER['HTTPS'] !== 'off');
        }
        
        return ($_SERVER['SERVER_PORT'] == 443);
    }

    function requestProto()
    {
         if(isset($_SERVER["HTTP_X_ONECOM_FORWARDED_PROTO"]))
         {
            return $_SERVER["HTTP_X_ONECOM_FORWARDED_PROTO"];
         }
         else if(isset($_SERVER["REQUEST_SCHEME"]))
         {
             return $_SERVER["REQUEST_SCHEME"];
         }
        
         return isSecure() ? "https" : "http";
    }

	function platform_url($withoutHttp=false)
	{
        global $platform_uri;
        
        if(!isset($platform_uri))
        {
            $url = $_SERVER['SCRIPT_NAME'];

            if(substr($url, 0, 1) != '/')
            {
                $url = '/'.$url;
            }

            if(($pos = strrpos($url, '/')))
            {
                $url = substr($url, 0, $pos+1);
            }
            
            $platform_uri = $url;
            
            if($platform_uri == 'index.php' || $platform_uri == '/index.php')
            {
                $platform_uri = '/';
            }
        }
        
		return ($withoutHttp ? "" : requestProto())."://".$_SERVER["HTTP_HOST"].$platform_uri;
	}

?>