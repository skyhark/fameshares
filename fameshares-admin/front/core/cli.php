<?php

    function default_cli_config()
    {
        return getConfig('shares_server');
    }


    function server_config_from_db( $id )
    {
        return getProperty( api('servers', 'info', array("id" => $id)), 0, array() );
    }

    function server_config()
    {
        if(!connected())
        {
            return default_cli_config();
        }
        
        global $_SERVER_CONFIG;
        
        if(!isset($_SERVER_CONFIG))
        {
            $_SERVER_CONFIG = getProperty( $_SESSION["servers"], $_SESSION['actif_server'], array());
        }
        
        return $_SERVER_CONFIG;
    }

    //--------------------------------------------------------

    function cli($url, $user, $pass, $command, $params)
    {
        if(!is_array($params))
        {
            $params = (array) $params;
        }
        
        foreach($params as $key => $val)
        {
            if(is_numeric($val))
            {
                $params[$key] = (int) $val;
            }
            else if($val == 'true')
            {
                $params[$key] = true;
            }
            else if($val == 'false')
            {
                $params[$key] = false;
            }
        }
        
        $data = array
        (
            "jsonrpc" => "1.0",
            "id" => "online_terminal",
            "method" => $command,
            "params" => $params
        );
        
        $ch = curl_init( $url );
        # Setup request to send json via POST.
        $payload = json_encode( $data );
        curl_setopt( $ch, CURLOPT_USERPWD, $user . ":" . $pass);
        curl_setopt( $ch, CURLOPT_POSTFIELDS, $payload );
        curl_setopt( $ch, CURLOPT_HTTPHEADER, array('Content-Type:application/json'));
        # Return response instead of printing.
        curl_setopt( $ch, CURLOPT_RETURNTRANSFER, true );
        # Send request.
        $result = curl_exec($ch);
        $httpcode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        curl_close($ch);
        # Print response.
        
        if($httpcode == 401)
        {
            return array("error" => array("code" => 401, "message" => "Authentification failed"));
        }
        
        return json_decode($result); 
    }

    function auto_cli($command, $params = array())
    {
        $config = server_config();
        return cli(getProperty($config, 'server', ''), getProperty($config, 'username', ''), getProperty($config, 'password', ''), $command, $params);
    }

?>