<?php
    if(is_view_only())
    {
        return;
    }
   
        
    function menu_li($title, $page, $cls, $hr)
    {
        print '<li';
        if($page == get_view_name())
        {
            print ' class="active"';
        }
        
        print '><a href="'.url_path($page).'">';
        print '<svg class="'.$cls.'"><use xlink:href="'.$hr.'"></use></svg> ';
        print $title.'</a></li>';
    }
    
       
    function tab_li($title, $page, $cls , $exit=true , $tab=array())
    {       
            $message =  (!empty($tab))? cli($tab['server'],$tab['username'], $tab['password'], 'help', array()):'';
            $ledclass= "";
            if($message !== "")
            {
               $ledclass = ($message != NULL) ? 'led-green' : 'led-red';
            }
            print '<li';
            if($page == $_SESSION['actif_server'])
            {
                print ' class="active"';
            }

            if(empty($page))
            {
                $page = url_path("connect");
            }
        
            print '>';
            print '<a href="'.$page.'" class="'.$cls.'">&nbsp;&nbsp;'.$title;
            print ($exit)? '<div class="'.$ledclass.' pull-left"></div>&nbsp;&nbsp; &nbsp;<p class="exit-tab" href="'.$page.'">x</p>': '';
            print '</a></li>';
    }

?>
<style>
    .exit-tab{
        float: right;
        color:red;
    }
    .glyph
    {
        height:1em;
        float:left;
    }
    .led-red {
          margin: 5px 0px 0px 0;
    width: 12px;
    height: 12px;
    background-color: #F30101;
    border-radius: 50%;
    box-shadow: #E20B0B 0 -1px 7px 1px, inset #600 0 -1px 3px, #F00 0 2px 15px;
    }

   
    .led-green {
       margin: 5px 0px 0px 0;
    width: 12px;
    height: 12px;
    background-color: #8CD200;
    border-radius: 50%;
    box-shadow: #26EF02 0 -1px 7px 1px, inset #8ED00B 0 -1px 9px, #7D0 0 2px 12px;
}


    

</style>
	<nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
		<div class="container-fluid">
			<div class="navbar-header">
				<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#sidebar-collapse">
					<span class="sr-only">Toggle navigation</span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</button>
				<a class="navbar-brand" href="#"><span>FameShares</span>Admin</a>
				
				
				<ul class="user-menu">
					<li class="dropdown pull-right">
						<a href="#" class="dropdown-toggle" data-toggle="dropdown"><svg class="glyph stroked male-user"><use xlink:href="#stroked-male-user"></use></svg> User <span class="caret"></span></a>
						<ul class="dropdown-menu" role="menu">
							<li><a href="#"><svg class="glyph stroked male-user"><use xlink:href="#stroked-male-user"></use></svg> Profile</a></li>
							<li><a href="#"><svg class="glyph stroked gear"><use xlink:href="#stroked-gear"></use></svg> Settings</a></li>
							<li><a href="#"><svg class="glyph stroked cancel"><use xlink:href="#stroked-cancel"></use></svg> Logout</a></li>
						</ul>
					</li>
				</ul>
			</div>
							
		</div><!-- /.container-fluid -->
	</nav>
 
		
	<div id="sidebar-collapse" class="col-sm-3 col-lg-2 sidebar">
		<form role="search">
			<div class="form-group">
				<input type="text" class="form-control" placeholder="Search">
			</div>
		</form>
		<ul class="nav menu">
            <?php
                //menu_li("Dashboard", "index", "glyph stroked dashboard-dial", "#stroked-dashboard-dial");
                menu_li("Terminal", "index", "glyph stroked external hard drive", "#stroked-external-hard-drive");
                menu_li("Network", "network", "glyph stroked dashboard-dial", "#stroked-dashboard-dial");
                menu_li("Actions", "actions_list", "glyph stroked dashboard-dial", "#stroked-dashboard-dial");
                menu_li("LastBlocks", "last-blocks", "glyph stroked dashboard-dial", "#stroked-dashboard-dial");
                menu_li("Wallet", "wallet", "glyph stroked dashboard-dial", "#stroked-dashboard-dial");
                menu_li("Settings", "settings", "glyph stroked dashboard-dial", "#stroked-dashboard-dial");
            ?>
		</ul>

	</div><!--/.sidebar-->
  

	<div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">	
   <ul class="nav nav-tabs server-tab">
          <?php 
                foreach ( $_SESSION["servers"] as $key => $tab )
                {
                    tab_li($tab["name"] ,$key , "server-link" ,true, $tab);
                }
                tab_li("+" , "" , "server-link new" , false);
            ?>
    </ul>		
		<div class="row">
			<div class="col-lg-12">
				<h1 class="page-header"><?= ucwords(str_replace('_', ' ', get_view_name())); ?></h1>
			</div>
		</div><!--/.row-->
<script>
$(document).ready(function(){
    $(document).on('click' , '.exit-tab', function(e){
         e.preventDefault();
         $.post(window.base_path, {serverdelete: $(this).attr("href")});
        
    });
    
    $(document).on('click' , '.server-link', function(e){
        e.preventDefault();
        if(!$(this).hasClass("new"))
        {
            $.post(window.base_path, {actifserver: $(this).attr("href")}, function()
            {
                window.location = window.location.href;    
            });
            
        }
        else
        {
            window.location = $(this).attr("href");  
        }
        
        $(".server-link").parent().removeClass("active");
        $(this).parent().addClass("active");

    });
});
        </script>