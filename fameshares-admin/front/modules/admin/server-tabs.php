<?php
       
    if(!isset($_SESSION["servers"]))
    { 
        $_SESSION["servers"] =array();
    }

    if(!isset($_SESSION['actif_server']))
    {
        $_SESSION['actif_server'] = "0";
    }

    //------------------------------

    front_api_handle('servers', 'delete_session', array('serverdelete'));
    front_api_handle('servers', 'change_session', array('actifserver'));

    if(is_view_only())
    {
        return;
    }

    //------------------------------

    function tab_li($title, $page, $cls , $exit=true , $tab=array())
    {       
            $message =  (!empty($tab))? cli($tab['server'],$tab['username'], $tab['password'], 'help', array()):'';
            $ledclass= "";
            if($message !== "")
            {
               $ledclass = ($message != NULL) ? 'led-green' : 'led-red';
            }
            print '<li';
            if($page == $_SESSION['actif_server'])
            {
                print ' class="active"';
            }

            if(empty($page))
            {
                $page = url_path("connect");
            }
        
            print '>';
            print '<a href="'.$page.'" class="'.$cls.'">&nbsp;&nbsp;'.$title;
            print ($exit)? '<div class="'.$ledclass.' pull-left"></div>&nbsp;&nbsp; &nbsp;<p class="exit-tab" href="'.$page.'">x</p>': '';
            print '</a></li>';
    }

?>
<style>
    .exit-tab{
        float: right;
        color:red;
    }
    .glyph
    {
        height:1em;
        float:left;
    }
    .led-red {
          margin: 5px 0px 0px 0;
    width: 12px;
    height: 12px;
    background-color: #F30101;
    border-radius: 50%;
    box-shadow: #E20B0B 0 -1px 7px 1px, inset #600 0 -1px 3px, #F00 0 2px 15px;
    }

   
    .led-green {
       margin: 5px 0px 0px 0;
    width: 12px;
    height: 12px;
    background-color: #8CD200;
    border-radius: 50%;
    box-shadow: #26EF02 0 -1px 7px 1px, inset #8ED00B 0 -1px 9px, #7D0 0 2px 12px;
}

</style>

<ul class="nav nav-tabs server-tab">
      <?php 
            foreach ( $_SESSION["servers"] as $key => $tab )
            {
                tab_li($tab["name"] ,$key , "server-link" ,true, $tab);
            }
            tab_li("+" , "" , "server-link new" , false);
        ?>
</ul>


<script>
$(document).ready(function(){
    $(document).on('click' , '.exit-tab', function(e){
         e.preventDefault();
         $.post('<?= url_path('settings'); ?>', {serverdelete: $(this).attr("href"), additional: 'no-menu'}, function(data)
         {
             window.location = window.location.href;  
         });
        
        return false;
    });
    
    $(document).on('click' , '.server-link', function(e){
        e.preventDefault();
        if(!$(this).hasClass("new"))
        {
            $.post('<?= url_path('settings'); ?>', {actifserver: $(this).attr("href"), additional: 'no-menu'}, function(data)
            {
                window.location = window.location.href;    
            });
        }
        else
        {
            window.location = $(this).attr("href");  
        }
        
        $(".server-link").parent().removeClass("active");
        $(this).parent().addClass("active");

        return false;
    });
});
</script>