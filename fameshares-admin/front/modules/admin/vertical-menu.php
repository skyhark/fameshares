<?php

?>
<div id="sidebar-collapse" class="col-sm-3 col-lg-2 sidebar">
    <form role="search">
        <div class="form-group">
            <input type="text" class="form-control" placeholder="Search">
        </div>
    </form>
    <ul class="nav menu">
        <?php
            menu_li("Terminal", "terminal", "glyph stroked external hard drive", "#stroked-external-hard-drive");
            menu_li("Network", "network", "glyph stroked dashboard-dial", "#stroked-dashboard-dial");
            menu_li("Actions", "actions_list", "glyph stroked dashboard-dial", "#stroked-dashboard-dial");
            menu_li("Last Blocks", "last-blocks", "glyph stroked dashboard-dial", "#stroked-dashboard-dial");
            menu_li("Wallet", "my_wallet", "glyph stroked dashboard-dial", "#stroked-dashboard-dial");
            menu_li("Settings", "settings", "glyph stroked dashboard-dial", "#stroked-dashboard-dial");
        ?>
    </ul>
</div><!--/.sidebar-->
