<?php
    if(is_view_only())
    {
        if(is_admin())
        {
            module('server-tabs');
        }
        
        return;
    }


    function menu_li($title, $page, $cls='', $hr='')
    {
        print '<li';
        if($page == get_view_name())
        {
            print ' class="active"';
        }
        
        print '><a href="'.url_path($page).'">';
        if(!empty($cls))
        {
            print '<svg class="'.$cls.'"><use xlink:href="'.$hr.'"></use></svg> ';
        }
        print $title.'</a></li>';
    }


?>
<nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
    <div class="container-fluid">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#sidebar-collapse">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="#"><span>FameShares</span><?= is_admin() ? ' Admin' : ''; ?></a>

            <?php
                if(!is_admin())
                {
            ?>
            <ul class="hmenu" style="float: left">
                <?php
                    menu_li("Home", "index");
                    menu_li("Actions Explorer", "explorer");
                    //menu_li("Wallet view", "wallet");
                    menu_li("API", "api");
                ?>
            </ul>
            <?php
                }
            ?>
            
            <ul class="user-menu">
                <li class="pull-right">
                    <a href="<?= url_path(connected() ? 'Logout' : 'Login'); ?>" class="dropdown-toggle"><svg class="glyph stroked male-user"><use xlink:href="#stroked-male-user"></use></svg> <?= connected() ? 'Logout' : 'Login'; ?></a>
                </li>
            </ul>
        </div>

    </div><!-- /.container-fluid -->
</nav>
<style>
    
    .hmenu
    {
        display: inline-block;
        margin-top: 14px;
        margin-right: 10px;
        border-left: 1px solid #ccc;
        padding-left: 20px !important;
        margin-left: 10px;
        list-style: none;
        padding: 0;
        color: #fff;
    }
    
    .hmenu li .glyph
    {
        width: 32px;
        height: 32px;
        margin-right: 10px
    }
    
    .hmenu li
    {
        display: inline-block;
        padding: 0 30px;
        text-align: center;
        font-weight: bold;
    }
    
    .hmenu a
    {
        color: #fff;
    }
    
</style>
<?php
    if(is_admin())
    {
        module('vertical-menu');
        print '<div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">';
        module('server-tabs');
        module('page-title');
    }
?>