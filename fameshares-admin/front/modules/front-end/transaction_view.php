<?php

    $transactions   = api('transaction', 'details', $settings);
    $vin            = getProperty($transactions, 'vin', array());
    $vout           = getProperty($transactions, 'vout', array());

?>
<table style="width: 100%">
    <td>
        <?php
            $ins = 0;
        
            foreach($vin as $row)
            {
                $row = (array) $row;
                
                if(!isset($row['txid']))
                {
                    continue;
                }
                $ins++;

                $addr = getProperty(api('transaction', 'out-address', array
                        (
                            "identifier" => $settings['identifier'],
                            "txid" => getProperty($row, 'txid'),
                            "vout" => getProperty($row, 'vout')
                        )
                      ), 'result', 'Error');
        ?>
        <div>
            <a href="<?= url_path('address/'.$addr); ?>">
                <?= $addr; ?>
            </a>
        </div>
        <?php
            }
        
            if($ins == 0)
            {
                print 'NO INPUTS (New generated shares)';
            }
        ?>
    </td>
    <td width="50px">
        <?php image('arrow-right.png'); ?>
    </td>
    <td>
        <?php
            foreach($vout as $row)
            {
        ?>
        <table style="width: 100%">
            <td>
                <?php
                    $adds = (array)getProperty($row, 'scriptPubKey', array());
                    $adds = (array)getProperty($adds, 'addresses', array());
                
                    foreach($adds as $addr)
                    {
                        print '<a href="'.url_path('address/'.$addr).'">'.$addr.'</a>';
                    }
                ?>
            </td>
            <td style="width: 80px">
                <?= getProperty($row, 'value', ''); ?>
            </td>
        </table>
        <?php
            }
        ?>
    </td>
</table>