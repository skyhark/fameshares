<?php
    $actions = api('actions', 'all');

    foreach($actions as $key => $action)
    {
        $action = (array) $action;
?>
<div class="row">
    <div class="col-lg-12">
        <div class="panel panel-default clickable-panel" onclick="location.href = '<?= url_path('action/'.$key); ?>'">
            <div class="panel-heading"><?= $key; ?></div>
            <div class="panel-body">
                <div class="row">
                    <div class="col-lg-4">
                        <label class="col-lg-6">Chain Height</label>
                        <span class="col-lg-6"><?= getProperty($action, 'chain_height', 'Error'); ?></span>
                    </div>
                    <div class="col-lg-4">
                        <label class="col-lg-6">Genesis Bits</label>
                        <span class="col-lg-6"><?= getProperty($action, 'genesis_bits', 'Error'); ?></span>
                    </div>
                    <div class="col-lg-4">
                        <label class="col-lg-6">Genesis Nonce</label>
                        <span class="col-lg-6"><?= getProperty($action, 'genesis_nonce', 'Error'); ?></span>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-4">
                        <label class="col-lg-6">Genesis Time</label>
                        <span class="col-lg-6"><?= get_date($action, 'genesis_time'); ?></span>
                    </div>
                    <div class="col-lg-4">
                        <label class="col-lg-6">Genesis Version</label>
                        <span class="col-lg-6"><?= getProperty($action, 'genesis_version', 'Error'); ?></span>
                    </div>
                </div>
                <hr>
                <div class="row">
                    <div class="col-lg-4">
                        <label class="col-lg-6">Genesis Hash</label>
                        <span class="col-lg-6"><?= getProperty($action, 'genesis_hash', 'Error'); ?></span>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php
    }
?>


<style>

    .clickable-panel:hover, .clickable-panel:hover .panel-heading
    {
        cursor: pointer;
        background: #ecf0f1;
    }
    
</style>