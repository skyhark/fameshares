<?php
    if(is_admin())
    {
        return redirect('terminal');
    }
?>
<header class="well">
    <img src="<?= img_path('header.jpg'); ?>">
    
    <h2>The official Famebroker's shares protocol</h2>
    
</header>


<div class="container">

    <h2>Open Source</h2>
    <p>
        Quamquam, si plane sic verterem Platonem aut Aristotelem, ut verterunt nostri poetae fabulas, male, credo, mererer de meis civibus, si ad eorum cognitionem divina illa ingenia transferrem. sed id neque feci adhuc nec mihi tamen, ne faciam, interdictum puto. locos quidem quosdam, si videbitur, transferam, et maxime ab iis, quos modo nominavi, cum inciderit, ut id apte fieri possit, ut ab Homero Ennius, Afranius a Menandro solet. Nec vero, ut noster Lucilius, recusabo, quo minus omnes mea legant. utinam esset ille Persius, Scipio vero et Rutilius multo etiam magis, quorum ille iudicium reformidans Tarentinis ait se et Consentinis et Siculis scribere. facete is quidem, sicut alia; sed neque tam docti tum erant, ad quorum iudicium elaboraret, et sunt illius scripta leviora, ut urbanitas summa appareat, doctrina mediocris.
    </p>
    
    <a href="">Github</a>

</div>


<style>

    .well
    {
        border: 0;
        color: #fff;
        padding: 0;
        border-bottom: 1px solid #333;
        position: relative;
    }
    
    .well img
    {
        width: 100%;
    }

    .well h2
    {
        position: absolute;
        font-size: 3em;
        top: 50%;
        left: 0;
        width: 100%;
        margin-top: -1em;
        color: #fff;
        text-align: center;
    }
    
</style>