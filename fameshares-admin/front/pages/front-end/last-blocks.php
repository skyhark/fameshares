<?php
    css("bootstrap-table.css");
    script("bootstrap-table.js");
?>
<div class="row">
    <div class="col-lg-12">
        <div class="panel panel-default">
            <div class="panel-body">
                <?php
                    module('table', array(

                        "colls" => array("Identifier", "Hash", "Time", "Transactions", "Height"),
                        "data" => api('blocks', 'lasts')

                    ));
                ?>
            </div>
        </div>
    </div>
</div>