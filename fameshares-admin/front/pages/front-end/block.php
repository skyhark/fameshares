<?php
    $parts = get_subview_parts();

    if(count($parts) < 2)
    {
        redirect("action_list");
    }

    global $identifier;
    $identifier = $parts[0];
    $info = api('blocks', 'get', array("identifier" => $identifier, "hash" => $parts[1]));
    $transactions = getProperty($info, 'tx', array());

    $title = $identifier.': Block #'.getProperty($info, 'height', '<b>Error</b>');
    if(!is_admin())
    {
        print '<div class="container" style="width: 90%">';
        module('page-title', array('title' => $title));
    }
    else
    {
        print '<h3 style="margin-top: 0">'.$title.'</h3>';
        print '<hr>';
    }

    function block_hash($fields, $prop, $auto_first = false)
    {
        $hash = getProperty($fields, $prop, '');
        
        if($auto_first)
        {
            if(empty($hash) || $hash == '0000000000000000000000000000000000000000000000000000000000000000')
            {
                return '0000000000000000000000000000000000000000000000000000000000000000';
            }
        }
        
        global $identifier;
        print '<a href="'.url_path('block/'.$identifier.'/'.$hash).'">'.$hash.'</a>';
    }
?>
<div class="row">
    <div class="col-lg-5">
        <table class="table">
            <th colspan="2">Summary</th>
            <tr>
                <td>Number Of Transactions</td>
                <td><?= count(getProperty($info, 'tx', array())); ?></td>
            </tr>
            <tr>
                <td>Output Total</td>
                <td><?= getProperty($info, '', '<b>Error</b>'); ?></td>
            </tr>
            <tr>
                <td>Estimated Transaction Volume</td>
                <td><?= getProperty($info, '', '<b>Error</b>'); ?></td>
            </tr>
            <tr>
                <td>Transaction Fees</td>
                <td><?= getProperty($info, '', '<b>Error</b>'); ?></td>
            </tr>
            <tr>
                <td>Confirmations</td>
                <td><?= getProperty($info, 'confirmations', '<b>Error</b>'); ?></td>
            </tr>
            <tr>
                <td>Height</td>
                <td><?= getProperty($info, 'height', '<b>Error</b>'); ?></td>
            </tr>
            <tr>
                <td>Timestamp</td>
                <td><?= get_date($info, 'time', 0); ?></td>
            </tr>
            <tr>
                <td>Difficulty</td>
                <td><?= getProperty($info, 'difficulty', '<b>Error</b>'); ?></td>
            </tr>
            <tr>
                <td>Bits</td>
                <td><?= getProperty($info, 'bits', '<b>Error</b>'); ?></td>
            </tr>
            <tr>
                <td>Size</td>
                <td><?= getProperty($info, 'size', '<b>Error</b>'); ?></td>
            </tr>
            <tr>
                <td>Version</td>
                <td><?= getProperty($info, 'version', '<b>Error</b>'); ?></td>
            </tr>
            <tr>
                <td>Nonce</td>
                <td><?= getProperty($info, 'nonce', '<b>Error</b>'); ?></td>
            </tr>
            <tr>
                <td>Block Reward</td>
                <td><?= getProperty($info, '', '<b>Error</b>'); ?></td>
            </tr>
        </table>
    </div>
    <div class="col-lg-7">
        <table class="table">
            <th colspan="2">Hashes</th>
            <tr>
                <td>Hash</td>
                <td><?= block_hash($info, 'hash'); ?></td>
            </tr>
            <tr>
                <td>Previous Block</td>
                <td><?= block_hash($info, 'prev_hash', true); ?></td>
            </tr>
            <tr>
                <td>Next Block(s)</td>
                <td><?= block_hash($info, 'next_hash'); ?></td>
            </tr>
            <tr>
                <td>Merkle Root</td>
                <td><?= getProperty($info, 'merkleroot', '<b>Error</b>'); ?></td>
            </tr>
        </table>
    </div>
</div>

<style>

    .transactions-table td
    {
        width: 33%;
    }
    
    .transactions-table td:nth-child(2)
    {
        text-align: center;
    }
    
    .transactions-table td:last-child
    {
        text-align: right;
    }
    
    
</style>

<h3>Transactions</h3>
<div class="row">
    <div class="col-lg-12">
        <div class="panel panel-body">
            <?php
                foreach($transactions as $trans)
                {
            ?>
            <a href="<?= url_path('transaction/'.$identifier.'/'.$trans); ?>">
                <?= $trans; ?>
            </a>
            <hr>
            <?php
                    module('transaction_view', array("txid" => $trans, "identifier" => $identifier));
                }
            
            ?>
        </div>
    </div>
</div>

<?php
    if(!is_admin())
    {
        print '</div>';
    }
?>