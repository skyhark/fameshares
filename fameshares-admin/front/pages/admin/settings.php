<?php
    front_api_handle('users', 'save-settings', array('api_key', 'api_token'));

    $info  = $_SESSION["servers"][$_SESSION["actif_server"]];
    $ainfo = api('users', 'get_settings');

    library('GoogleAuthenticator');
?>
<div class="container">
    <div class="panel panel-default">
        <div class="panel-heading">
            <svg class="glyph stroked external hard drive"><use xlink:href="#stroked-external-hard-drive"/></svg> Current Connection
        </div>
        <div class="panel-body">
           
            <form class="form-horizontal" action="<?= url_path('settings'); ?>" method="post">
                <div class="pull-right">
                <button type="submit" class="btn btn-primary" style="margin-left: 10px">Save</button>
                </div>
                <fieldset>
                    <input name="server_id" hidden value="<?= $_SESSION["actif_server"] ?>"/>
                
                    <div class="form-group">
                         
                        <label class="col-md-3 control-label">Name</label>
                        <div class="col-md-7">
                            <?php input("name", "", $info, ''); ?>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-3 control-label">Server</label>
                        <div class="col-md-7">
                            <?php input("server", "http://", $info, 'http://127.0.0.1:1451'); ?>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-3 control-label">User</label>
                        <div class="col-md-7">
                            <?php input("username", "...", $info, 'NOTFOUND'); ?>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-3 control-label">Pass</label>
                        <div class="col-md-7">
                             <?php input("password", "...", $info, 'NOTFOUND'); ?>
                        </div>
                    </div>
                
                </fieldset>
            </form>
            
         </div>
    </div>
</div>


<div class="container">
    <div class="panel panel-default">
        <div class="panel-heading">
            <svg class="glyph stroked external hard drive"><use xlink:href="#stroked-external-hard-drive"/></svg> Credentials
        </div>
        <div class="panel-body">
            <form class="form-horizontal" action="<?= url_path('settings'); ?>" method="post">
                <div class="pull-right">
                    <button type="submit" class="btn btn-primary" style="margin-left: 10px">Save</button>
                </div>
                <fieldset>
                    <div class="form-group">
                        <label class="col-md-3 control-label">API-KEY</label>
                        <div class="col-md-7">
                            <?php input("api_key", "", $ainfo, ''); ?>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-3 control-label">API-Token</label>
                        <div class="col-md-7">
                            <?php input("api_token", "", $ainfo, ''); ?>
                            <br>
                            <img src="<?= token_image(getProperty($info, 'api_token')); ?>">
                        </div>
                    </div>
                    <hr>
                    <div class="form-group">
                        <label class="col-md-3 control-label">Old Password</label>
                        <div class="col-md-7">
                            <input type="password" name="password" class="form-control">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-3 control-label">New Password</label>
                        <div class="col-md-7">
                             <input type="password" name="npassword" class="form-control">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-3 control-label">Confirm Password</label>
                        <div class="col-md-7">
                             <input type="password" name="nrpassword" class="form-control">
                        </div>
                    </div>
                </fieldset>
            </form>
            
         </div>
    </div>
</div>