<?php
    $stats = api('wallet', 'stats');
    $err=array();
    css("bootstrap-table.css");
    script("bootstrap-table.js");

if(isset($_POST["rescan"])  && isset($_POST["privkey"]) && isset($_POST["label"]))
{
    $err = auto_cli("importprivkey", array($_POST["privkey"] ,$_POST["label"] , $_POST["rescan"]));
}
if(isset($_POST["rescan"])  && isset($_POST["pubkey"]) && isset($_POST["label"]))
{
    $err = auto_cli("importpubkey", array($_POST["pubkey"] ,$_POST["label"] , $_POST["rescan"]));
}
if(isset($_POST["rescan"])  && isset($_POST["scriptkey"]) && isset($_POST["label"]) && isset($_POST["p2sh"]))
{
    $err = auto_cli("importaddress", array($_POST["scriptkey"] ,$_POST["label"] , $_POST["rescan"] , $_POST["p2sh"]));
}
?>
<style>
    
    .icon 
    {
        margin: 4px 0;
        font-size: 3em;
    }
    
    .extra_btns .btn
    {
        margin-bottom: 10px;
    }
    
</style>
<div class="row">
    <div class="col-xs-12 col-md-6 col-lg-3">
        <div class="panel panel-blue panel-widget ">
            <div class="row no-padding">
                <div class="col-sm-3 col-lg-5 widget-left">
                    <i class="glyphicon glyphicon-usd icon"></i>
                </div>
                <div class="col-sm-9 col-lg-7 widget-right">
                    <div class="large"><?= getProperty($stats, 'balance', 'Error'); ?></div>
                    <div class="text-muted">Balance</div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-xs-12 col-md-6 col-lg-3">
        <div class="panel panel-orange panel-widget">
            <div class="row no-padding">
                <div class="col-sm-3 col-lg-5 widget-left">
                    <i class="glyphicon glyphicon-usd icon"></i>
                </div>
                <div class="col-sm-9 col-lg-7 widget-right">
                    <div class="large"><?= getProperty($stats, 'unconfirmed_balance', 'Error'); ?></div>
                    <div class="text-muted">Unconfirmed Balance</div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-xs-12 col-md-6 col-lg-3">
        <div class="panel panel-teal panel-widget">
            <div class="row no-padding">
                <div class="col-sm-3 col-lg-5 widget-left">
                    <i class="glyphicon glyphicon-usd icon"></i>
                </div>
                <div class="col-sm-9 col-lg-7 widget-right">
                    <div class="large"><?= getProperty($stats, 'immature_balance', 'Error'); ?></div>
                    <div class="text-muted">Immature Balance</div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-xs-12 col-md-6 col-lg-3">
        <div class="panel panel-red panel-widget">
            <div class="row no-padding">
                <div class="col-sm-3 col-lg-5 widget-left">
                    <i class="glyphicon glyphicon-info-sign icon"></i>
                </div>
                <div class="col-sm-9 col-lg-7 widget-right">
                    <div class="large"><?= getProperty($stats, 'txcount', 'Error'); ?></div>
                    <div class="text-muted">Transactions Count</div>
                </div>
            </div>
        </div>
    </div>
</div><!--/.row-->



<div class="row">
    <div class="col-lg-12">
           <?php
                 function tab($name)
                {
                    $sub = get_subview_parts();
                    $sub = getProperty($sub, 1, 'privatekey');
                    print '<li';

                    if($name == $sub)
                    {
                        print ' class="active"';
                    }

                    print '><a href="'.url_path('my_wallet/new/'.$name).'">'.$name.'</a></li>';
                }
                        
                if(get_subview() === "new")
                 {
                 print '<ul class="nav nav-tabs">';
                    tab("privatekey");
                    tab("publickey");
                    tab("address");    
                print '</ul>' ;
                }
                
                ?>
        <div class="panel panel-default">
            <div class="panel-body">
             
                <div class="tab-content">
                   
                    <?php
                       $sub = get_subview_parts();
                       $sub = getProperty($sub, 1, 'privatekey');
                   
                       
                        if(get_subview() === "new")
                        {
                            $err = (array)$err;
                            if(getProperty($err , 'error' , false))
                            {
                                $error = (array)$err["error"];
                                print '<div class="alert alert-danger" role="alert">
                                          <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
                                          <span class="sr-only">Error '. getProperty($error, 'code', 'NO CODE').'</span>
                                         '. getProperty($error, 'message', 'NO CODE').'
                                        </div>';
                            }
                          ?>
                     <form class="form-horizontal" action="<?= url_path('my_wallet/new/'.$sub); ?>" method="post">
                     <fieldset>
                    <?php
                    if($sub == "privatekey")
                    {
                    ?>
                       
                            <!-- Name input-->
                            <div class="form-group">
                                <label class="col-md-3 control-label">Private key</label>
                                <div class="col-md-7">
                                    <?php input("privkey", "Private key" , $_POST); ?>
                                </div>
                            </div>
                             <div class="form-group">
                                <label class="col-md-3 control-label">Label (optional)</label>
                                <div class="col-md-7">
                                    <?php input("label", "Label" , $_POST); ?>
                                </div>
                            </div>
                              <div class="form-group">
                              <label class="col-md-3 control-label">Rescan</label>
                            <div class="col-md-7">
                               <select class="btn btn-primary chg-datatype" name="rescan">
                                    <?php 
                                    if(isset($_POST["rescan"]))
                                    {
                                        print ' <option value="true" ' .(($_POST["rescan"] === "true")? 'selected' : '') . '>True</option>';
                                        print ' <option value="false" ' .(($_POST["rescan"] === "false")? 'selected' : '') . '>False</option>';

                                    }
                                    else
                                    { ?>
                                    <option value="true">True</option>
                                    <option value="false"  selected>False</option>
                                    <?php    }
                                    ?>
                               </select>
                                  </div>
                            </div>
                           
                            
                         
                <?php
                            }
                             if($sub == "publickey")
                            {?>
                          <!-- Name input-->
                            <div class="form-group">
                                <label class="col-md-3 control-label">Public key</label>
                                <div class="col-md-7">
                                    <?php input("pubkey", "Public key" , $_POST); ?>
                                </div>
                            </div>
                             <div class="form-group">
                                <label class="col-md-3 control-label">Label (optional)</label>
                                <div class="col-md-7">
                                    <?php input("label", "Label" , $_POST); ?>
                                </div>
                            </div>
                              <div class="form-group">
                              <label class="col-md-3 control-label">Rescan</label>
                            <div class="col-md-7">
                               <select class="btn btn-primary chg-datatype" name="rescan">
                                    <?php 
                                    if(isset($_POST["rescan"]))
                                    {
                                        print ' <option value="true" ' .(($_POST["rescan"] === "true")? 'selected' : '') . '>True</option>';
                                        print ' <option value="false" ' .(($_POST["rescan"] === "false")? 'selected' : '') . '>False</option>';

                                    }
                                    else
                                    { ?>
                                    <option value="true">True</option>
                                    <option value="false"  selected>False</option>
                                    <?php    }
                                    ?>
                               </select>
                                  </div>
                            </div>

                        
                         
                         <?php  }
                        if($sub == "address")
                            {?>
                           <!-- Name input-->
                            <div class="form-group">
                                <label class="col-md-3 control-label">Script key</label>
                                <div class="col-md-7">
                                    <?php input("scriptkey", "The hex-encoded script (or address)" , $_POST); ?>
                                </div>
                            </div>
                             <div class="form-group">
                                <label class="col-md-3 control-label">Label (optional)</label>
                                <div class="col-md-7">
                                    <?php input("label", "Label" , $_POST); ?>
                                </div>
                            </div>
                             <div class="form-group">
                              <label class="col-md-3 control-label">Rescan</label>
                            <div class="col-md-7">
                               <select class="btn btn-primary chg-datatype" name="rescan">
                                    <?php 
                                    if(isset($_POST["rescan"]))
                                    {
                                        print ' <option value="true" ' .(($_POST["rescan"] === "true")? 'selected' : '') . '>True</option>';
                                        print ' <option value="false" ' .(($_POST["rescan"] === "false")? 'selected' : '') . '>False</option>';

                                    }
                                    else
                                    { ?>
                                    <option value="true">True</option>
                                    <option value="false"  selected>False</option>
                                    <?php    }
                                    ?>
                               </select>
                                  </div>
                            </div>
                          <div class="form-group">
                              <label class="col-md-3 control-label">P2SH version</label>
                            <div class="col-md-7">
                               <select class="btn btn-primary chg-datatype" name="p2sh">
                                    <?php 
                                    if(isset($_POST["p2sh"]))
                                    {
                                        print ' <option value="true" ' .(($_POST["p2sh"] === "true")? 'selected' : '') . '>True</option>';
                                        print ' <option value="false" ' .(($_POST["p2sh"] === "false")? 'selected' : '') . '>False</option>';

                                    }
                                    else
                                    { ?>
                                    <option value="true">True</option>
                                    <option value="false"  selected>False</option>
                                    <?php    }
                                    ?>
                               </select>
                                  </div>
                            </div>
                         
                         <?php
                            }
                         ?>
                          <hr>
                          <div class="form-group">
                                <div class="col-md-7 col-md-offset-3">
                                    <input type="submit" class="btn btn-default btn-md" value="Submit">
                                </div>
                          </div>
                    </fieldset>
                    </form>
                    <?php
                        
                    }
                    else
                    {
                        print ' <a class="btn btn-success" href="'. url_path("my_wallet/new/privatekey").'">ADD WALLET</a>';
                        module('table', array(

                        "colls" => array("name", "balance", "address"),
                        "data" => api('wallet', 'accounts')

                        )); 
                    }
                      
                
                    ?>
                </div>
            </div>
        </div>
    </div>
</div>