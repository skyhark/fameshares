<?php

    paged_action("Disconnect Node", function($data)
    {
        $addr = getProperty($data, 'ip', '');
        return auto_cli("disconnectnode", array($addr, "remove"));
    });

    paged_action("Remove Node", function($data)
    {
        $addr = getProperty($data, 'ip', '');
        return auto_cli("addnode", array($addr, "remove"));
    });

    paged_action("remove", function($data)
    {
        $addr = getProperty($data, 'address', '');
        return auto_cli("setban", array($addr, "remove"));
    });

    $stats      = api('network', 'stats');
    $nettotals  = getProperty($stats, 'nettotals', array());
    $upload     = (array)getProperty($nettotals, 'uploadtarget', array());
    $server     = getProperty(server_config(), 'server', '');

    css("bootstrap-table.css");
    script("bootstrap-table.js");

?>
<style>
    
    .icon 
    {
        margin: 4px 0;
        font-size: 3em;
    }
    
    .extra_btns .btn
    {
        margin-bottom: 10px;
    }
    
</style>
<div class="row">
    <div class="col-xs-12 col-md-6 col-lg-3">
        <div class="panel panel-blue panel-widget ">
            <div class="row no-padding">
                <div class="col-sm-3 col-lg-5 widget-left">
                    <i class="glyphicon glyphicon-link icon"></i>
                </div>
                <div class="col-sm-9 col-lg-7 widget-right">
                    <div class="large"><?= getProperty($stats, 'connections', 0); ?></div>
                    <div class="text-muted">Connections</div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-xs-12 col-md-6 col-lg-3">
        <div class="panel panel-orange panel-widget">
            <div class="row no-padding">
                <div class="col-sm-3 col-lg-5 widget-left">
                    <i class="glyphicon glyphicon-usd icon"></i>
                </div>
                <div class="col-sm-9 col-lg-7 widget-right">
                    <div class="large"><?= number_format(getProperty($stats, 'relayfee', 0), 6); ?></div>
                    <div class="text-muted">Relay Fee</div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-xs-12 col-md-6 col-lg-3">
        <div class="panel panel-teal panel-widget">
            <div class="row no-padding">
                <div class="col-sm-3 col-lg-5 widget-left">
                    <i class="glyphicon glyphicon-ban-circle icon"></i>
                </div>
                <div class="col-sm-9 col-lg-7 widget-right">
                    <div class="large"><?= getProperty($stats, 'banneds', 0); ?></div>
                    <div class="text-muted">Banneds</div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-xs-12 col-md-6 col-lg-3">
        <div class="panel panel-red panel-widget">
            <div class="row no-padding">
                <div class="col-sm-3 col-lg-5 widget-left">
                    <i class="glyphicon glyphicon-info-sign icon"></i>
                </div>
                <div class="col-sm-9 col-lg-7 widget-right">
                    <div class="large"><?= getProperty($stats, 'protocolversion', 0); ?></div>
                    <div class="text-muted">Protocol Version</div>
                </div>
            </div>
        </div>
    </div>
</div><!--/.row-->




<div class="row">
    <div class="col-lg-9">
        <div class="panel panel-default">
            <div class="panel-body">
            
                <div class="row">
                    <label class="col-lg-3">
                        Total Bytes Received
                    </label>
                    <div class="col-lg-3">
                        <?= getProperty($nettotals, 'totalbytesrecv', 0); ?>
                    </div>
             
                    <label class="col-lg-3">
                        Total Bytes Sended
                    </label>
                    <div class="col-lg-3">
                        <?= getProperty($nettotals, 'totalbytessent', 0); ?>
                    </div>
                </div>
                
                <div class="row">
                    <label class="col-lg-3">
                        Time Millis
                    </label>
                    <div class="col-lg-3">
                        <?= getProperty($nettotals, 'timemillis', 0); ?>
                    </div>
             
                    <label class="col-lg-3">
                        Upload time Frame
                    </label>
                    <div class="col-lg-3">
                        <?= getProperty($upload, 'timeframe', 0); ?>
                    </div>
                </div>
                
                <div class="row">
                    <label class="col-lg-3">
                        Upload Target
                    </label>
                    <div class="col-lg-3">
                        <?= getProperty($upload, 'target', 0); ?>
                    </div>
             
                    <label class="col-lg-3">
                        Upload Target Reached
                    </label>
                    <div class="col-lg-3">
                        <?= getProperty($upload, 'target_reached', false) ? 'true' : 'false'; ?>
                    </div>
                </div>
                
                <div class="row">
                    <label class="col-lg-3">
                        Upload Bytes Left In Cycle
                    </label>
                    <div class="col-lg-3">
                        <?= getProperty($upload, 'bytes_left_in_cycle', 0); ?>
                    </div>
             
                    <label class="col-lg-3">
                        Upload Time Histroical Blocks
                    </label>
                    <div class="col-lg-3">
                        <?= getProperty($upload, 'time_left_in_cycle', 0); ?>
                    </div>
                </div>
                
                <div class="row">
                    <label class="col-lg-3">
                        Upload Serve Histroical Blocks
                    </label>
                    <div class="col-lg-3">
                        <?= getProperty($upload, 'serve_historical_blocks', 0); ?>
                    </div>
                    
                    <label class="col-lg-3">
                        IP
                    </label>
                    <div class="col-lg-3">
                        <?= substr($server, 0, strpos($server, ':')); ?>
                    </div>
                </div>
                
            </div>
        </div>
    </div>
    <div class="col-lg-3">
        <div class="panel panel-default">
            <div class="panel-body extra_btns">
                
                <button type="button" class="btn btn-primary col-lg-12" onclick="add_node();">Add Node</button>
                <button type="button" class="btn btn-danger col-lg-12" onclick="set_ban()">Set Ban</button>
                <button type="button" class="btn btn-danger col-lg-12" onclick="clear_ban()">Clear Banneds</button>
                <button type="button" class="btn btn-default col-lg-12">Ping</button>
                
            </div>
        </div>
    </div>
</div>


<div class="row">
    <div class="col-lg-12">
        <div class="panel panel-default">
            <div class="panel-body tabs">
                <ul class="nav nav-tabs">
                    <?php
                        $sub = get_subview('Peers');
                    
                        function tab($name)
                        {
                            print '<li';
                            
                            if($name == get_subview('Peers'))
                            {
                                print ' class="active"';
                            }
                            
                            print '><a href="'.url_path('network/'.$name).'">'.$name.'</a></li>';
                        }
                        
                        tab('Peers');
                        tab('Nodes');
                        tab('Banneds');
                        tab('Interfaces');
                    ?>
                </ul>

                <div class="tab-content">
                    <?php
                    
                        if($sub == 'Peers')
                        {
                            module('table', array(

                                "colls" => array("address", "last send", "last recv", "bytes send", "bytes recv", "starting height", "synced headers", "synced blocks"),
                                "data" => api('network', 'peers')

                            ));
                        }
                        else if($sub == 'Nodes')
                        {
                            module('table', array(

                                "colls" => array("primary address", "connected", "addresses"),
                                "data" => api('network', 'nodes'),
                                "actions" => array("Disconnect Node", "Remove Node")

                            ));
                        }
                        else if($sub == 'Banneds')
                        {
                            module('table', array(

                                "colls" => array("address", "banned until", "ban created", "ban reason"),
                                "data" => api('network', 'banneds'),
                                "removable" => true

                            ));
                        }
                        else if($sub == 'Interfaces')
                        {
                            module('table', array(

                                "colls" => array("name", "limited", "reachable", "proxy", "proxy_randomize_credentials"),
                                "data" => api('network', 'interfaces')

                            ));
                        }
                
                    ?>
                </div>
            </div>
        </div>
    </div>
</div>


<script>

function add_node()
{
    bootbox.prompt("Node IP", function(result)
    {
        if(result == null)
        {
            return;
        }
        
        $.cli("addnode", [result, 'add'], function(data, error)
        {
            if(error)
            {
                return alert(error);
            }
            
            alert("Node added");
        });
    }); 
}

function set_ban()
{
    bootbox.dialog({
                title: "Set Ban",
                message:
                    '<input id="name" name="banip" type="text" placeholder="ip(/netmask)" class="form-control input-md">'  +
                    '<input id="name" name="bantime" type="text" placeholder="bantime optional" class="form-control input-md" style="margin-top: 10px">' ,
                buttons: {
                    success: {
                        label: "Ban",
                        className: "btn-primary",
                        callback: function () {
                            var ip = $('input[name="banip"]').val();
                            var time = $('input[name="bantime"]').val();
                            var data = [ip, 'add'];
                            
                            if(time.replaceAll(' ', '') != "")
                            {
                                data.push(time);
                            }
                            
                            $.cli("setban", data, function(data, error)
                            {
                                if(error)
                                {
                                    return alert(error);
                                }

                                alert("Ban Rule Added");
                            });
                        }
                    }
                }
            }
        );
}
    
function clear_ban()
{
    bootbox.confirm("Do you realy want to clear the banned list ?", function(result)
    {
        if(result)
        {
            $.cli("clearbanned", [], function(data, error)
            {
                if(error)
                {
                    return alert(error);
                }

                alert("Banned Rules cleared");
            });
        }
    });
}


</script>