<?php

    function getConfig($name)
    {
        global $_CONFIGS;
        
        if(!is_array($_CONFIGS))
        {
            $_CONFIGS = array();
        }
        
        if(isset($_CONFIGS[$name]))
        {
            return $_CONFIGS[$name];
        }

        $path = dirname(__FILE__). "/../config/{$name}.json";
           
        if($_SERVER["SERVER_NAME"] != "localhost")
        {
            $path2 = dirname(__FILE__). "/../config/{$name}_online.json";
            
            if(file_exists($path2))
            {
                $path = $path2;
            }
        }
        
        if(!file_exists($path))
        {
            $_CONFIGS[$name] = array();
            return $_CONFIGS[$name];
        }
        
        $content = file_get_contents($path);
        $content = json_decode($content);
   
        if(!$content)
        {
            $_CONFIGS[$name] = array();
            return $_CONFIGS[$name];   
        }
        
        $_CONFIGS[$name] = (array) $content;
        return $_CONFIGS[$name];
    }

?>