<?php

    function api_login_handle($result)
    {
        if(count($result) == 0)
        {
            return array("error" => "Wrong login and or password");
        }
        if(isset($result['error']))
        {
            return $result;
        }
        
        $row = $result[0];
        $_SESSION['actions_auth_id'] = $row->id;
        return array("id" => $row->id);
    }

    function connected()
    {
        $_SESSION["actions_auth_id"] = 1;
        $_SESSION['is_admin'] = true;
        return isset($_SESSION["actions_auth_id"]);
    }

    function is_admin()
    {
        if(!connected())
        {
            return false;
        }
        
        return getProperty($_SESSION, 'is_admin', false);
    }

?>