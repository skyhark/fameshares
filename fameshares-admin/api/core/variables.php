<?php

    function getProperty($array, $var, $default = null)
    {
        if(!is_array($array))
        {
            $array = (array) $array;
        }
        return isset($array[$var]) ? $array[$var] : $default;   
    }

    function getNumericProperty($array, $var, $default = 0)
    {
        $result = getProperty($array, $var, $default);
        return is_numeric($result) ? $result : $default;
    }

    function get_date($info, $var)
    {
        return date("Y-m-d H:i:s", getProperty($info, $var, 0));
    }

    function check_type($var, $type)
    {
        switch($type)
        {
            case "string":
                return is_string($var);
                break;
            case "numeric":
                return is_numeric($var);
                break;
            case "array":
                return is_array($var);
                break;
            case "object":
                return is_object($var);
                break;
            case "no":
                return true;
                break;
        }
        
        return false;
    }

    function check_variables($vars, $inarr = null)
    {
        if($inarr == null)
        {
            $inarr = $_POST;
        }
        
        foreach($vars as $key => $value)
        {
            $type_filter = true;
            
            if(is_numeric($key))
            {
                $key = $value;
                $type_filter = false;
            }
            
            if(!isset($inarr[$key]))
            {
                api_error("Variable $key not found", true);
                return false;
            }
            if($type_filter)
            {
                if(!check_type($inarr[$key], $value))
                {
                    api_error("Variable $key has a wrong type (needed: {$value})", true);
                    return false;
                }
            }
        }
        
        return true;
    }

    function first($arr, $default = array())
    {
        if(is_array($arr))
        {
            if(count($arr) > 0)
            {
                return (array) $arr[0];
            }
            
            return $default;
        }
        
        return $default;
    }

?>