<?php
    class Extendable  {
        private $handlers = array();
        public function registerHandler($handler) {
            $this->handlers[] = $handler;
        }

        public function __call($method, $arguments) {
            foreach ($this->handlers as $handler) {
                if (method_exists($handler, $method)) {
                    return call_user_func_array(
                        array($handler, $method),
                        $arguments
                    );
                }
            }
            
            throw new exception("method '$method' not found");
        }
    }
?>