<?php
    class _Mysql
    {
        var $link;
        
        function __construct($server, $user, $pass, $database=null)
        {
            if(!function_exists("mysql_connect"))
            {
                throw new Exception('mysql_connect not declared');
            }
            else
            {
                $this->link = @mysql_connect($server, $user, $pass);

                if(isset($database))
                {
                    @mysql_select_db($database, $this->link);
                }
            }
        }
        
        function error()
        {
            return mysql_error($this->link);
        }
        
        function connected()
        {
            return $this->link != false;   
        }
        
                
        function set_database($database)
        {
            mysql_select_db($this->link, $database);
        }
        
        function query($query)
        {
            return mysql_query($this->link, $query);
        }
        
        function affected_rows()
        {
            return mysql_affected_rows($this->link);
        }
        
        function insert_id()
        {
            return mysql_insert_id($this->link);   
        }
        
        function databases()
        {
            $dbs = mysql_list_dbs($this->link);
            return json_result($dbs);
        }
        
        function table_structure()
        {
            return $this->query("select * from INFORMATION_SCHEMA.COLUMNS where table_name = '{$_GET["table"]}';");
        }
        
        function tables()
        {
            
        }
        
        function json_query($query)
        {
            if(is_string($query))
            {
                $query = query($query);   
            }
            
            //...
            return $query;
        }
    }

?>