<?php

    function setSqlLimits($sql, $start, $limit, $force=false)
    {
        $pos = strrpos($sql, "LIMIT ");
        $start = is_numeric($start) ? $start : 0;
        $limit = is_numeric($limit) ? $limit : 15;

        if($pos != false && $pos != -1)
        {
            return $sql;   
        }
        else
        {
            return $sql." LIMIT $start, $limit";   
        }
    }

?>