<?php

    function api_preset($category)
    {
        $path = dirname(__FILE__).'/../actions/'.$category.'/config.json';
        
        if(file_exists($path))
        {
            $content = file_get_contents($path);
            $conf = (array) json_decode($content);
            
            if($conf)
            {
                if(($dbname = getProperty($conf, 'database', false)))
                {
                    return use_db($dbname);
                }
            }
        }
        
        return true;
    }

    function sql_api_debug($category, $name, $vars = null)
    {
        if(!api_preset($category))
        {
            global $_RESULT;
            return $_RESULT;
        }
        
        if($vars == null)
        {
            $vars = $_POST;
        }
        
        $path1 = dirname(__FILE__).'/../actions/'.$category.'/'.$name.'.sql';
        
        if(!is_file($path1))
        {
            return array("error" => "action not found ");
        }
        else
        {
            $sql = file_get_contents($path1);
            return prepare_query($sql, $vars);
        }
    }

    function api($category, $name, $vars = null)
    {
        if(!api_preset($category))
        {
            global $_RESULT;
            return $_RESULT;
        }
        
        if($vars == null)
        {
            $vars = $_POST;
        }
        
        if(connected())
        {
            $vars['auth_id'] = $_SESSION['actions_auth_id'];
        }
        
        $path = dirname(__FILE__).'/../actions/'.$category.'/'.$name.'.php';
        $path1 = dirname(__FILE__).'/../actions/'.$category.'/'.$name.'.sql';
        
        if(!is_file($path) && !is_file($path1))
        {
            return array("error" => "action not found ");
        }
        else if(is_file($path1))
        {
            $sql = file_get_contents($path1);
            api_query($sql, $vars);
            global $_RESULT;
            
            if($category == "users" && $name == "login")
            {
                $_RESULT = api_login_handle($_RESULT);
            }
            
            return $_RESULT;
        }
        
        $old_post = $_POST;
        $_POST = $vars;
        
        global $_RESULT;
        $_RESULT = array();
        include($path);
        global $_RESULT;
        
        $_POST = $old_post;        
        return $_RESULT;
    }

    function api_result($res)
    {
        global $_RESULT;
        
        if(!is_array($res) && !is_object($res))
        {
            $res = array("result" => $res);
        }
        
        $_RESULT = $res;
    }

    function api_error($res)
    {
        global $_RESULT;
        $_RESULT = array("error" => $res);
    }

    function api_query($sql, $vars = null, $db_type = null)
    {
        if(!check_connection())
        {
            return;
        }
        
        if($vars == null)
        {
            $vars = $_POST;
        }
        
        $query = query($sql, $vars, $db_type);
        $result = array();
        
        while(($row = $query->fetchObject()))
        {
            $result[] = $row;
        }
        
        $db = db($db_type);
        if(count($result) == 0 && $db->error())
        {
            api_error($db->error());
            return;
        }
        else if(count($result) == 0)
        {
            if(($id = $db->insert_id()))
            {
                $result["id"] = $id;
            }
        }
        
        api_result($result);
    }

?>