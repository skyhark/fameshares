<?php
	ini_set('display_errors',1);
	ini_set('display_startup_errors',1);
	error_reporting(-1);

    global $request_path, $platform_path;
    $request_path = $_SERVER['REQUEST_URI'];
    $platform_path = $_SERVER['REQUEST_URI'];

    if(($x = strrpos ($_SERVER['SCRIPT_NAME'], '/')))
    {
        $platform_path = $_SERVER['DOCUMENT_ROOT'].substr($request_path, 1, $x);
        $request_path = substr($request_path, $x+1);
    }
    if(substr($request_path, 0, 1) == '/')
    {
        $request_path = substr($request_path, 1);
    }
	
	function include_api_dir($reg)
	{
        $platform_path = dirname(__FILE__).'/';

        $arr = (array) glob($platform_path.$reg);
		foreach ($arr as $filename) {
			if($filename != $platform_path."core/Core.php" && !empty($filename))
			{
				include_once($filename);
				$GLOBALS += get_defined_vars();
			}
		}
	}
	
	include_api_dir("*.php");
    include_api_dir("build_in/*.php");
?>