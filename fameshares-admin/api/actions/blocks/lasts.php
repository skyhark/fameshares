<?php

    $content = (array) auto_cli('getlastblocks', array());
    $result = (array) getProperty($content, 'result', array());

    function cmp($a, $b)
    {
        return strcmp($b->time, $a->time);
    }

    usort($result, "cmp");

    foreach($result as $k => $v)
    {
        $v->time = date('Y-m-d H:i:s', $v->time);
        $result[$k] = $v;
    }

    api_result($result);