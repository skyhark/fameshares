<?php

    if(empty($_POST['api_token']))
    {
        library('GoogleAuthenticator');
        $_POST['api_token'] = generate_token(); //...
    }

    if(!empty($_POST['password']))
    {
        if($_POST['npassword'] != $_POST['nrpassword'])
        {
            return api_error("The passwords do not match");
        }
        
        return api_query("UPDATE users SET password=MD5('{@npassword}'), api_key='{@api_key}', api_token='{@api_token}' WHERE id='{@auth_id}'");
    }

    api_query("UPDATE users SET api_key='{@api_key}', api_token='{@api_token}' WHERE id='{@auth_id}'");

?>