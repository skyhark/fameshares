<?php

    if(!check_connection())
    {
        api_error('The server is temporarily offline.');
        return;
    }

    if(!isset($_SESSION["loginAttemps"]))
    {
        $_SESSION["loginAttemps"] = 0;
    }


    //-----------------------------------------
    //Max attemps waiting check

    /*if(isset($_SESSION["loginWaiting"]))
    {
        if($_SESSION["loginWaiting"] < time())
        {
            unset($_SESSION["loginWaiting"]);
        }
        else
        {
            api_error( 'Too many attempts please try again later ('.date('Y-m-d H:i:s', $_SESSION["loginWaiting"]).')' );
            return;
        }
    }*/


    //-----------------------------------------
    //Login check


    if(!isset($_POST["email"]) || !isset($_POST["password"]))
    {   
        api_error('Login variables not found');
        return;
    }

    $_POST['email'] = strtolower($_POST['email']);
    query("SET NAMES utf8");
    $user = query_object("SELECT * FROM users"); // WHERE email='{@email}' and password=MD5('{@password}')

    if(!$user)
    {
        api_error('Wrong username and or password.');
        
        $_SESSION["loginAttemps"]++;
        if($_SESSION["loginAttemps"] == 3)
        {
            $_SESSION["loginWaiting"] = time() + 60;
        }
        else if($_SESSION["loginAttemps"] == 5)
        {
            $_SESSION["loginWaiting"] = time() + 300;
        }
        else if($_SESSION["loginAttemps"] == 8)
        {
            $_SESSION["loginWaiting"] = time() + 600;
        }
        else if($_SESSION["loginAttemps"] == 8)
        {
            $_SESSION["loginWaiting"] = time() + 900;
        }
        
        return;
    }


    //-----------------------------------------
    //User credentials check

    $_SESSION["loginAttemps"] = 0;
    

    $_SESSION["actions_auth_id"] = $user->id;
    $_SESSION["is_admin"] = true;
        
    api_result( array('success' => true, 'id' => $user->id) );

?>