<?php

    $content = auto_cli('listaccounts', array('SHK-001', 1, true));
    $content = getProperty($content, 'result', array());

    $result = array();

    foreach($content as $key => $val)
    {
        $result[] = array
            (
                "name" => $key,
                "amount" => $val,
                "address" => getProperty(auto_cli('getaccountaddress', array($key)), 'result', 'Error')
            );
    }

    api_result($result);
?>