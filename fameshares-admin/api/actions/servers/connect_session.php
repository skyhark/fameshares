<?php

    if(isset($_POST["server"]) && isset($_POST["username"]) && isset($_POST["password"]))
    {
       if(isset($_POST["server_id"]))
       {
           $_SESSION["actif_server"] = $_POST['server_id'];
       }
       else
       {
           $_SESSION["actif_server"] = uniqid();
       }
        
       $_SESSION["servers"][$_SESSION["actif_server"]]["name"] = $_POST["name"];
       $_SESSION["servers"][$_SESSION["actif_server"]]["username"] = $_POST["username"];
       $_SESSION["servers"][$_SESSION["actif_server"]]["server"] = $_POST["server"];
       $_SESSION["servers"][$_SESSION["actif_server"]]["password"] = $_POST["password"];
    }
    else
    {
        api_error('Variables not found');
    }
?>