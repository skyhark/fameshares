<?php

    $identifier = $_POST['identifier'];

    $result = array
    (
        "height" => blocks_count($identifier),
        "balance" => balance($identifier),
        "last_blocks" => array(),
        "creation_time" => getProperty((array)block_from_index($identifier, 0), 'time', 0),
        "last_transactions" => getProperty(auto_cli('listactiontransactions', array($identifier, '*', 10, 0, true)), 'result', array())
    );

    $height = $result["height"];

    for($i = $height; $i > $height - 5 && $i >= 0; $i--)
    {
        $block = block_from_index($identifier, $i);
        array_push($result["last_blocks"], $block);
    }

    api_result($result);

?>