<?php
    $details = (array) api('transaction', 'details');

    if(isset($details['error']))
        return api_result($details);

    $vout    = getProperty($details, 'vout', array());
    $row     = getProperty($vout, $_POST['vout'], array());
    $script  = getProperty($row, 'scriptPubKey', array());

    api_result( getProperty(getProperty($script, 'addresses', 'Error'), 0) );