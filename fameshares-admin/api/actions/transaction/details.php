<?php

    //$transaction = api('transaction', 'get');
    //api_result( getProperty($transaction, 'details', array()) );

    $transaction = (array) auto_cli('getrawtransaction', array($_POST['identifier'], $_POST['txid'], 1));

    if(isset($transaction['error']))
        return api_error($transaction['error']);

    api_result( getProperty($transaction, 'result', array()) );