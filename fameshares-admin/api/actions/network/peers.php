<?php

    $content = auto_cli('getpeerinfo', array('SHK-001'));
    $content = (array) getProperty($content, 'result', array());

    foreach($content as $key => $peer)
    {
        unset($peer->id);
        unset($peer->services);
        unset($peer->relaytxes);
        unset($peer->conntime);
        unset($peer->timeoffset);
        unset($peer->pingtime);
        unset($peer->minping);
        unset($peer->version);
        unset($peer->subver);
        unset($peer->inbound);
        unset($peer->banscore);
        unset($peer->inflight);
        unset($peer->whitelisted);
        unset($peer->bytessent_per_msg);
        unset($peer->bytesrecv_per_msg);
        
        foreach($peer as $k => $v)
        {
            if(is_object($v))
                unset($peer->$k);
        }
        
        $content[$key] = $peer;
    }

    api_result($content);

?>