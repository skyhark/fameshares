
function set_result(res)
{
    data = JSON.parse(res);

    if(typeof(data.error) == "object" && data.error != null)
    {
        if(data.error.code == -1 || data.error.code == -32601)
        {
            $("#jjson").html( "<h4 style='color: red'></h4>" );
            $("#jjson h4").html( data.error.message.replaceAll("\n", "<br>").replaceAll(" ", "&nbsp;") );
            return;
        }
        
        return $("#jjson").JSONView(data.error);
    }
    
    if(typeof(data.result) == "object")
    {
        $("#jjson").JSONView( data.result );
        return;
    }
    
    if(typeof(data.result) == "string")
    {
        $("#jjson").html( "<h4 style='color: green'></h4>" );
        $("#jjson h4").html( data.result.replaceAll("\n", "<br>").replaceAll(" ", "&nbsp;") );
        return;
    }

    $("#jjson").JSONView(res);
}

function compute_data()
{
    var post =
    {
        command: $("input[name='command']").val(), 
        params: [ ],
        types: [ ],
        comment: $("#comment").val(),
        review: $("#review").is(":checked")
    };

    $("input[name='params[]'], textarea[multi_field_id]").having_text().serializeArray().forEach(function(val)
    {
        post.params.push( val.value );
    });
     $("select[name='types[]']").having_text().serializeArray().forEach(function(val)
    {
        post.types.push( val.value );
    });

    return post;
}

function save()
{
    var data = compute_data();
    data.id = $("input[name='id']").val();
    data.name = $("input[name='name']").val();
    data.additional = 'no-menu';

    $.post(window.base_path+"/terminal", data, function(data)
    {
        try
        {
            data = JSON.parse(data);
        } catch(e)
        {
            $.notify("Wrong server response", "error");
            return;
        }

        if(data.error)
        {
            if(typeof(data.error) != "string")
            {
                return $.notify("An internal error occured", "error");
            }

            $.notify(data.error, "error");
            return;
        }

        if(data.id)
        {
            $("input[name='id']").val(data.id);
        }

        $.notify("Command successfully saved", "success");
    })
    .fail(function(e)
    {
        $.notify("An error occured", "error");
    });
}

function open_action()
{
    $("#openmodal").modal("show");
}

$(document).ready(function() {
    //--------------------------------------------------------------
    //Send commands

    $("#jjson").JSONView('{  }');
    $("input[name='params[]']").multi_field();
    
    $("#frmterm").submit(function(e)
    {
        e.preventDefault();
        
        var post = compute_data();
        console.log(post);
        post.additional = 'no-menu';

        $.post(window.base_path+"/terminal", post, function(data)
        {
            console.log(data);
            
            try
            {
                set_result(data);
            }
            catch(e)
            {
                console.error(e);
                $("#jjson").JSONView(data);
            }
            
            var date = new Date();
            $("#lstupd").text( padLeft(date.getHours(), 2) + ":" + padLeft(date.getMinutes(), 2)+ ":" + padLeft(date.getSeconds(), 2) );
        });
        
        return false;
    });
    
    
    //--------------------------------------------------------------
    
    $(window).keypress(function(event) {
        if((event.which == 115 && event.ctrlKey) || event.which == 19)
        {
            save();
        }
        else if((event.which == 111 && event.ctrlKey) || event.which == 15)
        {
            open_action();
        }
        else
        {
            return true;
        }

        event.preventDefault();
        return false;
    });
});