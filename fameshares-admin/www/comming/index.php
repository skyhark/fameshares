<?php
    if(isset($_POST["user"]) && isset($_POST["pass"]))
    {
        if($_POST["user"] == "beta" && $_POST["pass"] == "beta2016famebroker")
        {
            session_start();
            $_SESSION["beta"] = "oke";
            header("location: ../");
            exit;
        }
    }

?><!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title>	FameShares Coming Soon</title>
	<meta name="description" content="Kite Coming Soon HTML Template by Jewel Theme" >
	<meta name="author" content="Jewel Theme">

	<!-- Mobile Specific Meta -->
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<!--[if IE]><meta http-equiv='X-UA-Compatible' content='IE=edge,chrome=1'><![endif]-->

	<!-- Bootstrap  -->
	<link href="assets/css/bootstrap.min.css" rel="stylesheet">

	<!-- icon fonts font Awesome -->
	<link href="assets/css/font-awesome.min.css" rel="stylesheet">

	<!-- Custom Styles -->
	<link href="assets/css/style.css" rel="stylesheet">

	<!--[if lt IE 9]>
	<script src="assets/js/html5shiv.js"></script>
	<![endif]-->

</head>
<body>


	<!-- Preloader -->
	<div id="preloader">
		<div id="loader">
			<div class="dot"></div>
			<div class="dot"></div>
			<div class="dot"></div>
			<div class="dot"></div>
			<div class="dot"></div>
			<div class="dot"></div>
			<div class="dot"></div>
			<div class="dot"></div>
			<div class="lading"></div>
		</div>
	</div><!-- /#preloader -->
	<!-- Preloader End-->


	<!-- Main Menu -->
	<div id="main-menu" class="navbar navbar-default navbar-fixed-top" role="navigation">

		<div class="navbar-header">
			<!-- responsive navigation -->
			<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
				<span class="sr-only">Toggle navigation</span>
				<i class="fa fa-bars"></i>
			</button> <!-- /.navbar-toggle -->

		</div> <!-- /.navbar-header -->

		<nav class="collapse navbar-collapse">
			<!-- Main navigation -->
			<ul id="headernavigation" class="nav navbar-nav">
				<li class="active"><a href="#page-top">Home</a></li>	
				<li><a href="#about">Login</a></li>	
			</ul> <!-- /.nav .navbar-nav -->
		</nav> <!-- /.navbar-collapse  -->
	</div><!-- /#main-menu -->
	<!-- Main Menu End -->
	

	<!-- Page Top Section -->
	<section id="page-top" class="section-style" data-background-image="../images/background.jpg">
		<div class="pattern height-resize">
			<div class="container">
				<h1 class="site-title">
					FameShares
				</h1><!-- /.site-title -->
				<h3 class="section-name">
					<span>
						We Are
					</span>
				</h3><!-- /.section-name -->
				<h2 class="section-title">
					Coming Soon
				</h2><!-- /.Section-title  -->
				<div id="time_countdown" class="time-count-container">

					<div class="col-sm-3">
						<div class="time-box">
							<div class="time-box-inner dash days_dash animated" data-animation="rollIn" data-animation-delay="300">
								<span class="time-number">
									<span class="digit">0</span>
									<span class="digit">0</span>
									<span class="digit">0</span>
								</span>
								<span class="time-name">Days</span>
							</div>
						</div>
					</div>

					<div class="col-sm-3">
						<div class="time-box">
							<div class="time-box-inner dash hours_dash animated" data-animation="rollIn" data-animation-delay="600">
								<span class="time-number">
									<span class="digit">0</span>
									<span class="digit">0</span>	
								</span>
								<span class="time-name">Hours</span>
							</div>
						</div>
					</div>

					<div class="col-sm-3">
						<div class="time-box">
							<div class="time-box-inner dash minutes_dash animated" data-animation="rollIn" data-animation-delay="900">
								<span class="time-number">
									<span class="digit">0</span>
									<span class="digit">0</span>
								</span>
								<span class="time-name">Minutes</span>
							</div>
						</div>
					</div>

					<div class="col-sm-3">
						<div class="time-box">
							<div class="time-box-inner dash seconds_dash animated" data-animation="rollIn" data-animation-delay="1200">
								<span class="time-number">
									<span class="digit">0</span>
									<span class="digit">0</span>
								</span>
								<span class="time-name">Seconds</span>
							</div>
						</div>
					</div>
					
				</div><!-- /.time-count-container -->

				<p class="time-until">
					<span>Time Until Launce</span>
				</p><!-- /.time-until -->



				<div class="next-section">
					<a class="go-to-about"><span></span></a>
				</div><!-- /.next-section -->
				
			</div><!-- /.container -->
		</div><!-- /.pattern -->		
	</section><!-- /#page-top -->
	<!-- Page Top Section  End -->


	<!-- About Us Section -->
	<section id="about" class="section-style" data-background-image="images/background.jpg">
		<div class="pattern height-resize"> 
			<div class="container">
				<h3 class="section-name">
					<span>
						FameShares
					</span>
				</h3><!-- /.section-name -->
				<h2 class="section-title">
					Beta testers login
				</h2><!-- /.Section-title  -->
				<p class="section-description" style="display: none">
					Proin gravida nibh vel velit auctor aliquet. Aenean sollicitudin, lorem quis bibendum auctor, nisi elit consequat ipsum, nec sagittis sem nibh id elit. Duis sed odio sit amet nibh vulputate cursus a sit amet mauris.
				</p><!-- /.section-description -->

				<div class="team-container">
					<div class="row">
						<div class="col-sm-6 col-sm-offset-3">

                          <div class="card card-container" style="margin: 5em 0 5em 0">
                                <!-- <img class="profile-img-card" src="//lh3.googleusercontent.com/-6V8xOA6M7BA/AAAAAAAAAAI/AAAAAAAAAAA/rzlHcD0KYwo/photo.jpg?sz=120" alt="" /> -->
                                <img id="profile-img" class="profile-img-card" src="//ssl.gstatic.com/accounts/ui/avatar_2x.png">
                                <p id="profile-name" class="profile-name-card"></p>
                                <form class="form" method="post" action=""> <!--#Login!-->
                                    <span id="reauth-email" class="reauth-email"></span>
                                    <div class="form-group">
                                        <input type="text" id="inputEmail" class="form-control input-lg" placeholder="Username" required="" autofocus="" name="user">
                                    </div>
                                    <div class="form-group">
                                        <input type="password" id="inputPassword" class="form-control input-lg" placeholder="Password" required="" name="pass">
                                    </div>
                                    
                                    <button class="btn btn-lg btn-primary btn-block btn-signin" type="submit" style="margin-top: 15px" onclick="window.location = 'home.php'">Sign in</button>
                                </form><!-- /form -->
                            </div><!-- /card-container -->

                    
						</div><!-- /.col-sm-4 -->
					</div><!-- /.row -->
					
				</div><!-- /.team-container -->
		<br><br><br><br>

			</div><!-- /.container -->
		</div><!-- /.pattern -->
		
	</section><!-- /#about -->
	<!-- About Us Section End -->



		<!-- Footer Section -->
		<footer id="footer-section">
			<p class="copyright">
				&copy; <a href="http://demos.jeweltheme.com/kite">Kite</a> 2014-2015, All Rights Reserved. Designed by & Developed by <a href="http://jeweltheme.com">Jewel Theme</a>
			</p>
		</footer>
		<!-- Footer Section End -->


		<!-- jQuery Library -->
		<script type="text/javascript" src="assets/js/jquery-2.1.0.min.js"></script>
		<!-- Modernizr js -->
		<script type="text/javascript" src="assets/js/modernizr-2.8.0.min.js"></script>
		<!-- Plugins -->
		<script type="text/javascript" src="assets/js/plugins.js"></script>
		<!-- Custom JavaScript Functions -->
		<script type="text/javascript" src="assets/js/functions.js"></script>
		<!-- Custom JavaScript Functions -->
		<script type="text/javascript" src="assets/js/jquery.ajaxchimp.min.js"></script>

	</body>
	</html>
	
<style>

#profile-img
{
	border-radius: 50% 50%;
	margin: 0 auto 4em;
	max-height: 130px;
	display: block;
}
    
    
    .card
    {
         background: #fff;   
        padding: 20px;
        border-radius: 5px;
    }
    
    .card btn-primary
    {
         background: blue;   
    }
    
    .card .form-group input
    {
        border: 1px solid #888;
        border-radius: 5px;
        color: #000;
    }
    
    ::-webkit-input-placeholder { /* WebKit browsers */
    color:    #444;
}
:-moz-placeholder { /* Mozilla Firefox 4 to 18 */
   color:    #444;
   opacity:  1;
}
::-moz-placeholder { /* Mozilla Firefox 19+ */
   color:    #909;
   opacity:  1;
}
:-ms-input-placeholder { /* Internet Explorer 10+ */
   color:    #909;
}
    
</style>
